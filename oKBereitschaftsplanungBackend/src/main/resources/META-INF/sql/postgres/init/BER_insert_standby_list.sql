/* {0}.standby_list */
INSERT INTO {0}.standby_list(id, modification_date, title) VALUES (1, '2018-08-06 02:00:00', 'Einsatzleiter Strom');
INSERT INTO {0}.standby_list(id, modification_date, title) VALUES (2, '2018-08-06 02:00:00', 'Einsatzleiter Gas');
INSERT INTO {0}.standby_list(id, modification_date, title) VALUES (3, '2018-08-06 02:00:00', 'Alle Gruppen');
/* Erweiterung der N-ERGIE */
INSERT INTO {0}.standby_list(id, modification_date, title)VALUES (4, '2018-11-11 11:11:11', 'Sulzbach_Rosenberg');
INSERT INTO {0}.standby_list(id, modification_date, title)VALUES (5, '2018-11-11 11:11:11', 'Nürnberg_NN');
INSERT INTO {0}.standby_list(id, modification_date, title)VALUES (6, '2018-11-11 11:11:11', 'Rothenburg_NR');
INSERT INTO {0}.standby_list(id, modification_date, title)VALUES (7, '2018-11-11 11:11:11', 'Nürnberg_NZ');
INSERT INTO {0}.standby_list(id, modification_date, title)VALUES (8, '2018-11-11 11:11:11', 'Weissenburg_NW');
INSERT INTO {0}.standby_list(id, modification_date, title)VALUES (9, '2018-11-11 11:11:11', 'Ing Bereitschaft MDN/NSG');
SELECT setval('{0}.standby_list_id_seq', 9, true);