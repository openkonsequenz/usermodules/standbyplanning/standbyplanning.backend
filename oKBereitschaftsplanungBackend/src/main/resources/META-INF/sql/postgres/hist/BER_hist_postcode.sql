/*postcode History*/
CREATE TABLE IF NOT EXISTS POSTCODE_HIST (HIST_ID serial, operation char(1), stamp timestamp, LIKE POSTCODE EXCLUDING ALL);
--
-- Create a row in {0}.postcode_hist_audit to reflect the operation performed on postcode,
-- making use of the special variable TG_OP to work out the operation.
--
CREATE OR REPLACE FUNCTION process_postcode_audit() RETURNS TRIGGER AS $object_audit$ BEGIN IF (TG_OP = 'DELETE') THEN INSERT INTO {0}.postcode_hist SELECT nextval('postcode_hist_hist_id_seq'), 'D', now(), OLD.*; ELSIF (TG_OP = 'UPDATE') THEN INSERT INTO {0}.postcode_hist SELECT nextval('postcode_hist_hist_id_seq'), 'U', now(), NEW.*; ELSIF (TG_OP = 'INSERT') THEN INSERT INTO {0}.postcode_hist SELECT nextval('postcode_hist_hist_id_seq'), 'I', now(), NEW.*; END IF; RETURN NULL; END; $object_audit$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS postcode_audit on {0}.postcode;
CREATE TRIGGER postcode_audit AFTER INSERT OR UPDATE OR DELETE ON {0}.postcode FOR EACH ROW EXECUTE PROCEDURE process_postcode_audit();