/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import static org.junit.Assert.assertNotNull;

import javax.ws.rs.core.Response;

import org.easymock.Mock;
import org.eclipse.openk.sp.controller.UserFunctionController;
import org.eclipse.openk.sp.dto.UserFunctionDto;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class UserFunctionRestServiceTest {

	@InjectMocks
	private UserFunctionRestService userFunctionRestService;

	@Mock
	private UserFunctionController userFunctionController;

	public UserFunctionRestServiceTest() {
	}

	@Test
	public void getUserFunctionsTest() {
		String jwt = "LET-ME-IN";
		Response result = userFunctionRestService.getUserFunctions(jwt);
		assertNotNull(result);
	}
	
	@Test
	public void getUserFunctionsSelectionTest() {
		String jwt = "LET-ME-IN";
		Response result = userFunctionRestService.getUserFunctionsSelection(jwt);
		assertNotNull(result);
	}

	@Test
	public void getUserFunctionTest() {
		String jwt = "LET-ME-IN";
		Response result = userFunctionRestService.getUserFunction(1l, jwt);
		assertNotNull(result);
	}

	@Test
	public void saveUserFunctionTest() {
		FileHelperTest fh = new FileHelperTest();

		// create test data
		String json = fh.loadStringFromResource("testUserFunction.json");
		UserFunctionDto userFunctionDto = (UserFunctionDto) new Gson().fromJson(json, UserFunctionDto.class);

		String jwt = "LET-ME-IN";
		Response result = userFunctionRestService.saveUserFunction(jwt, userFunctionDto);
		assertNotNull(result);
	}
	
//	@Test
//	public void checkAuthTest () {
//		userFunctionRestService.
//		
//	}
}
