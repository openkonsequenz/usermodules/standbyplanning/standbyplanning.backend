/*organisation History*/
CREATE TABLE IF NOT EXISTS ORGANISATION_HIST (HIST_ID serial, operation char(1), stamp timestamp, LIKE ORGANISATION EXCLUDING ALL);
--
-- Create a row in {0}.organisation_hist_audit to reflect the operation performed on organisation,
-- making use of the special variable TG_OP to work out the operation.
--
CREATE OR REPLACE FUNCTION process_organisation_audit() RETURNS TRIGGER AS $object_audit$ BEGIN IF (TG_OP = 'DELETE') THEN INSERT INTO {0}.organisation_hist SELECT nextval('organisation_hist_hist_id_seq'), 'D', now(), OLD.*; ELSIF (TG_OP = 'UPDATE') THEN INSERT INTO {0}.organisation_hist SELECT nextval('organisation_hist_hist_id_seq'), 'U', now(), NEW.*; ELSIF (TG_OP = 'INSERT') THEN INSERT INTO {0}.organisation_hist SELECT nextval('organisation_hist_hist_id_seq'), 'I', now(), NEW.*; END IF; RETURN NULL; END; $object_audit$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS organisation_audit on {0}.organisation;
CREATE TRIGGER organisation_audit AFTER INSERT OR UPDATE OR DELETE ON {0}.organisation FOR EACH ROW EXECUTE PROCEDURE process_organisation_audit();