/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class LocationDtoTest {

	@Test
	public void testGettersAndSetters() {

		LocationDto l = new LocationDto();

		l.setId(1l);
		assertEquals(1l, l.getId().longValue());

		l.setCommunity("community");
		assertEquals("community", l.getCommunity());

		l.setDistrict("district");
		assertEquals("district", l.getDistrict());

		l.setLatitudedistrict("latitudedistrict");
		assertEquals("latitudedistrict", l.getLatitudedistrict());

		l.setLongitudedistrict("longitudedistrict");
		assertEquals("longitudedistrict", l.getLongitudedistrict());

		List<PostcodeDto> lsPostcode = new ArrayList<PostcodeDto>();
		lsPostcode.add(new PostcodeDto("33100"));
		lsPostcode.add(new PostcodeDto("33102"));
		lsPostcode.add(new PostcodeDto("33105"));
		l.setLsPostcode(lsPostcode);
		assertEquals(lsPostcode.size(), l.getLsPostcode().size());

		List<RegionSmallDto> lsRegions = new ArrayList<>();
		RegionSmallDto region = new RegionSmallDto();
		lsRegions.add(region);
		l.setLsRegions(lsRegions);
		assertEquals(lsRegions.size(), l.getLsRegions().size());

		l.setShorttext("shorttext");
		assertEquals("shorttext", l.getShorttext());

		l.setTitle("title");
		assertEquals("title", l.getTitle());

		l.setWgs84zonedistrict("wgs84zonedistrict");
		assertEquals("wgs84zonedistrict", l.getWgs84zonedistrict());
		
		
		List<LocationForBranchDto> lsLocationForBranches = new ArrayList<LocationForBranchDto>();
		lsLocationForBranches.add(new LocationForBranchDto());
		lsLocationForBranches.add(new LocationForBranchDto());
		lsLocationForBranches.add(new LocationForBranchDto());
		l.setLsLocationForBranches(lsLocationForBranches);
		assertEquals(lsLocationForBranches.size(), l.getLsLocationForBranches().size());

	}
}
