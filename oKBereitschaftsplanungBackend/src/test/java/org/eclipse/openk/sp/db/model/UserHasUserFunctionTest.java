/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.util.Date;

import org.eclipse.openk.sp.db.converter.UserHasFunctionToStringConverter;
import org.eclipse.openk.sp.dto.UserHasUserFunctionDto;
import org.junit.Test;

public class UserHasUserFunctionTest {

	@Test
	public void testGettersAndSetters() {
		
		Long l  = new Long(30);
//		String t = "Text";
		Date d = new Date();
		User user = new User();
		UserFunction userFunction = new UserFunction();
		

		UserHasUserFunction uf = new UserHasUserFunction();

		uf.setId(l);
		assertEquals(l, uf.getId());
		
		uf.setUser(user);
		assertEquals(user, uf.getUser());

		uf.setUserFunction(userFunction);
		assertEquals(userFunction, uf.getUserFunction());

		UserHasUserFunction uf2 = new UserHasUserFunction();
		uf2.setId(5L);
		assertEquals(1, uf.compareTo(uf));
		assertEquals(0, uf.compareTo(uf2));
		
	}
}
