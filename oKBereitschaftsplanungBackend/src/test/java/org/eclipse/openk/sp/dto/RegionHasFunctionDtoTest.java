/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

public class RegionHasFunctionDtoTest {

	@Test
	public void testGettersAndSetters() {
		RegionHasFunctionDto rhf = new RegionHasFunctionDto();
		rhf.setRegionHasFunctionId(1l);
		assertEquals(1l, rhf.getRegionHasFunctionId().longValue());

		UserFunctionDto uf = new UserFunctionDto();
		rhf.setUserFunction(uf);
		assertEquals(uf, rhf.getUserFunction());

		RegionDto region = new RegionDto();
		rhf.setRegion(region);
		assertEquals(region, rhf.getRegion());
		
		Date date = new Date();
		rhf.setValidFrom(date);
		assertEquals(date, rhf.getValidFrom());

		rhf.setValidTo(date);
		assertEquals(date, rhf.getValidTo());
		
		rhf.setFunctionId(1l);
		assertEquals(1l, rhf.getFunctionId().longValue());
		
		rhf.setFunctionName("functionName");
		assertEquals("functionName", rhf.getFunctionName());
		
		rhf.setRegionId(1l);
		assertEquals(1l, rhf.getRegionId().longValue());
		
		rhf.setRegionName("RegionName");
		assertEquals("RegionName", rhf.getRegionName());
		
		
	}
}
