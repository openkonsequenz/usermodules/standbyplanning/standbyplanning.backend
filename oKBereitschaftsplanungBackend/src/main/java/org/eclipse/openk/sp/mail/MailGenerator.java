/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.mail;

import java.util.Properties;

import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Class to generate the default mail with standard configuration.
 * 
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MailGenerator {

	private static Logger logger = Logger.getLogger(MailGenerator.class);

	@Autowired
	FileHelper fileHelper;

	/**
	 * Function to generate a default mail without subject, main text and
	 * attachments.
	 * 
	 * @param sendTo   address of the receiver
	 * @param username name of the user
	 * @throws Exception if error occurs.
	 * @return {@link HtmlMail} with default settings
	 * @throws SpException
	 */
	public HtmlEmail generateMail(String sendTo, String username, String sendersName, String sendersAddress,
			String subject, String htmlBody, Properties properties) throws SpException {

		try {

			String smtpHost = properties.getProperty(Globals.SMTP_MAIL_HOST);
			String smtpPort = properties.getProperty(Globals.SMTP_MAIL_PORT);
			String strSsl = properties.getProperty(Globals.SMTP_MAIL_SSL);
			String strAuthUser = properties.getProperty(Globals.SMTP_MAIL_AUTH_USER);
			String strAuthPwd = properties.getProperty(Globals.SMTP_MAIL_AUTH_SECRET);

			HtmlEmail email = new HtmlEmail();
			// host
			email.setHostName(smtpHost);
			// port
			email.setSmtpPort(Integer.parseInt(smtpPort));
			// sender
			email.setFrom(sendersAddress, sendersName);
			// subject
			email.setSubject(subject);
			// html body message
			email.setHtmlMsg(htmlBody);
			// Alternativ - Text
			email.setTextMsg("Your email client does not support HTML messages");

			calcSecureParam(email, strSsl, smtpPort, strAuthUser, strAuthPwd);

			// receiver(s)
			if (sendTo != null) {

				if (sendTo.contains(";")) {
					String[] arrayMailAddresses = sendTo.split(";");
					for (String mailAddress : arrayMailAddresses) {
						email.addTo(mailAddress);
					}
				} else if (sendTo.contains(",")) {
					String[] arrayMailAddresses = sendTo.split(",");
					for (String mailAddress : arrayMailAddresses) {
						email.addTo(mailAddress);
					}
				} else {
					email.addTo(sendTo);
				}
			}
			if (username != null) {
				email.addTo(username);
			}

			return email;
		} catch (Exception e) {
			logger.error(e);
			throw new SpException();
		}
	}

	public boolean calcSecureParam(HtmlEmail email, String strSsl, String smtpPort, String strAuthUser,
			String strAuthPwd) {

		if (strSsl != null && Boolean.parseBoolean(strSsl)) {
			email.setSSLOnConnect(true);
			email.setSslSmtpPort(smtpPort);
		}

		if ((strAuthUser != null && !strAuthUser.equals("")) && (strAuthPwd != null && !strAuthPwd.equals(""))) {
			email.setAuthentication(strAuthUser, strAuthPwd);
		}
		return true;
	}

}
