/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.config;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.util.FileHelper;
import org.eclipse.persistence.config.SessionCustomizer;
import org.eclipse.persistence.sessions.Session;
import org.springframework.stereotype.Service;

@Service
public class DbSessionCustomizer implements SessionCustomizer {

	private String schema = "OPENK";

	public DbSessionCustomizer() {
		final Logger LOGGER = Logger.getLogger(DbInitializer.class);

		FileHelper fh = new FileHelper();
		Properties property;
		try {
			property = fh.loadPropertiesFromResource(DbInitializer.APP_PROPERTIES_NAME);
			schema = property.getProperty("db.schema");
		} catch (IOException e) {
			LOGGER.error(e, e);
		}
	}

	public void customize(Session session) {
		session.getLogin().setTableQualifier(schema);
		session.getLogin().setUsesStreamsForBinding(true);
	}
}
