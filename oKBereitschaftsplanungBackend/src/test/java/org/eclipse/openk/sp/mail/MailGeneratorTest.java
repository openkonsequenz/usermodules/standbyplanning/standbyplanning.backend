/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.mail;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.mail.HtmlEmail;
import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.FileHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Class to generate the default mail with standard configuration.
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class MailGeneratorTest {

	private static Logger logger = Logger.getLogger(MailGeneratorTest.class);

	@Mock
	FileHelper fileHelper;

	@InjectMocks
	MailGenerator mailGenerator;

	@Test
	public void generateMailTest() throws SpException, IOException {

		String sendTo = "test@test.de;test@test.de";
		String username = "test@test.de";
		String sendersName = "test";
		String sendersAddress = "test@test.de";
		String subject = "test";
		String htmlBody = "mail";

		Properties property = new Properties();
		property.setProperty(Globals.SMTP_MAIL_HOST, "test");
		property.setProperty(Globals.SMTP_MAIL_PORT, "25");
		property.setProperty(Globals.SMTP_MAIL_SSL, "false");
		property.setProperty(Globals.SMTP_MAIL_AUTH_USER, "");
		property.setProperty(Globals.SMTP_MAIL_AUTH_SECRET, "");

		Mockito.when(fileHelper.loadPropertiesFromResource(Mockito.anyString())).thenReturn(property);

		HtmlEmail result = mailGenerator.generateMail(sendTo, username, sendersName, sendersAddress, subject, htmlBody,
				property);
		assertNotNull(result);

		sendTo = "test@test.de,test@test.de";
		result = mailGenerator.generateMail(sendTo, username, sendersName, sendersAddress, subject, htmlBody, property);
		assertNotNull(result);

		sendTo = "test@test.de";
		result = mailGenerator.generateMail(sendTo, username, sendersName, sendersAddress, subject, htmlBody, property);
		assertNotNull(result);

		sendTo = null;
		username = null;
		result = mailGenerator.generateMail(sendTo, username, sendersName, sendersAddress, subject, htmlBody, property);
		assertNotNull(result);

	}

	@Test(expected = SpException.class)
	public void generateMailErrorTest() throws SpException, IOException {

		String sendTo = "test@test.de;test@test.de";
		String username = "test@test.de";
		String sendersName = "test";
		String sendersAddress = "test@test.de";
		String subject = "test";
		String htmlBody = "mail";

		Properties property = new Properties();
		property.setProperty(Globals.SMTP_MAIL_HOST, "test");
		property.setProperty(Globals.SMTP_MAIL_PORT, "25");
		property.setProperty(Globals.SMTP_MAIL_SSL, "false");
		property.setProperty(Globals.SMTP_MAIL_AUTH_USER, "");
		property.setProperty(Globals.SMTP_MAIL_AUTH_SECRET, "");

		Mockito.when(fileHelper.loadPropertiesFromResource(Mockito.anyString())).thenReturn(null);

		HtmlEmail result = mailGenerator.generateMail(sendTo, username, sendersName, sendersAddress, subject, htmlBody,
				property);
		assertNotNull(result);

		sendTo = "test@test.de,test@test.de";
		result = mailGenerator.generateMail(sendTo, username, sendersName, sendersAddress, subject, htmlBody, null);
		assertNotNull(result);

	}

	@Test
	public void calcSecureParam() {

		HtmlEmail email = new HtmlEmail();
		String strSsl = "true";
		String smtpPort = "test";
		String strAuthUser = "test";
		String strAuthPwd = "test";

		boolean result = mailGenerator.calcSecureParam(email, strSsl, smtpPort, strAuthUser, strAuthPwd);
		assertNotNull(result);

		strSsl = null;
		smtpPort = "test";
		strAuthUser = null;
		strAuthPwd = null;

		result = mailGenerator.calcSecureParam(email, strSsl, smtpPort, strAuthUser, strAuthPwd);
		assertNotNull(result);

		strSsl = "false";
		smtpPort = "test";
		strAuthUser = "";
		strAuthPwd = "";

		result = mailGenerator.calcSecureParam(email, strSsl, smtpPort, strAuthUser, strAuthPwd);
		assertNotNull(result);

		strAuthUser = "qwert";
		strAuthPwd = "";

		result = mailGenerator.calcSecureParam(email, strSsl, smtpPort, strAuthUser, strAuthPwd);
		assertNotNull(result);

		strAuthUser = "";
		strAuthPwd = "aasdf";

		result = mailGenerator.calcSecureParam(email, strSsl, smtpPort, strAuthUser, strAuthPwd);
		assertNotNull(result);

		strAuthUser = null;
		strAuthPwd = "aasdf";

		result = mailGenerator.calcSecureParam(email, strSsl, smtpPort, strAuthUser, strAuthPwd);
		assertNotNull(result);

		strAuthUser = "asdfg";
		strAuthPwd = null;

		result = mailGenerator.calcSecureParam(email, strSsl, smtpPort, strAuthUser, strAuthPwd);
		assertNotNull(result);
	}

}
