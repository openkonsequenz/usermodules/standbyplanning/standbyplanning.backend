/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.openk.sp.abstracts.AbstractEntity;

/**
 * The persistent class for the "BRANCH" database table.
 */
@Entity
@Table(name = "Branch")
public class Branch extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BRANCH_ID_SEQ")
	@SequenceGenerator(name = "BRANCH_ID_SEQ", sequenceName = "BRANCH_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "title", length = 256, nullable = true)
	private String title;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "int_branches_for_groups", joinColumns = @JoinColumn(name = "branch_id"), inverseJoinColumns = @JoinColumn(name = "standby_group_id"))
	private List<StandbyGroup> lsStandbyGroups = new ArrayList<>();

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "branch")
	private List<LocationForBranch> lsLocationForBranches = new ArrayList<>();

	public Branch() {
		/** default constructor. */
	}

	public Branch(Long id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the lsStandbyGroups
	 */
	public List<StandbyGroup> getLsStandbyGroups() {
		return lsStandbyGroups;
	}

	/**
	 * @param lsStandbyGroups
	 *            the lsStandbyGroups to set
	 */
	public void setLsStandbyGroups(List<StandbyGroup> lsStandbyGroups) {
		this.lsStandbyGroups = lsStandbyGroups;
	}

	/**
	 * @return the lsLocationForBranches
	 */
	public List<LocationForBranch> getLsLocationForBranches() {
		return lsLocationForBranches;
	}

	/**
	 * @param lsLocationForBranches
	 *            the lsLocationForBranches to set
	 */
	public void setLsLocationForBranches(List<LocationForBranch> lsLocationForBranches) {
		this.lsLocationForBranches = lsLocationForBranches;
	}

	@Override
	public int compareTo(AbstractEntity arg0) {
		return (getId() == arg0.getId()) ? 1 : 0;
	}

	@Override
	public Branch copy() {
		Branch newBranch = new Branch();
		newBranch.setId(null);
		newBranch.setLsLocationForBranches(this.getLsLocationForBranches());
		newBranch.setLsStandbyGroups(this.getLsStandbyGroups());
		newBranch.setTitle(this.getTitle());

		return newBranch;

	}

}