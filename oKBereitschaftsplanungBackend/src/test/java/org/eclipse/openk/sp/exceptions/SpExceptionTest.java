/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.apache.http.HttpStatus;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.junit.Test;

public class SpExceptionTest {


	
	@Test
	public void testinit() {
		SpException ex = new SpException();
		
		assertNotNull(ex);
		
	}
	
	@Test
	public void testinit2() {
		SpException ex = new SpException(new HttpStatusException(HttpStatus.SC_BAD_REQUEST));
		
		assertNotNull(ex);
		
	}
	
	@Test
	public void testinit3() {
		SpErrorEntry ex = new SpErrorEntry(23, "test");
		ex.setMessage("neuer Test");
		ex.setCode(25);
		assertEquals("neuer Test", ex.getMessage());
		assertEquals(25, ex.getCode());
		
	}
}