/* {0}.region */
INSERT INTO {0}.region( id, region_name) VALUES (1, 'Darmstadt');
INSERT INTO {0}.region( id, region_name) VALUES (2, 'Erbach');
INSERT INTO {0}.region( id, region_name) VALUES (3, 'Groß-umstadt');
INSERT INTO {0}.region( id, region_name) VALUES (4, 'Heppenheim');
/*Erweiterung N-ERGIE */
INSERT INTO {0}.region( id, region_name) VALUES (5, 'Nürnberg');
INSERT INTO {0}.region( id, region_name) VALUES (6, 'Rothenburg');
INSERT INTO {0}.region( id, region_name) VALUES (7, 'Sulzbach Rosenberg');
INSERT INTO {0}.region( id, region_name) VALUES (8, 'Weißenburg');
/*Erweiterung N-ERGIE Ende */
SELECT setval('{0}.region_id_seq', 8, true);