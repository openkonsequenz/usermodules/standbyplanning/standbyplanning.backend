/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.net.MalformedURLException;
import java.util.Date;

import org.eclipse.openk.sp.util.DateHelper;
import org.junit.Test;

public class StandbyDurationTest {

	@Test
	public void testGettersAndSetters() {
		Date d = DateHelper.getDate(2018, 11, 1);
		Boolean b = new Boolean(true);
		StandbyDuration dur = new StandbyDuration();
		dur.setId(1l);
		assertEquals(1l, dur.getId().longValue());

		StandbyDuration standbyDuration = new StandbyDuration(2L);
		assertEquals(2L, standbyDuration.getId().longValue());

		dur.setModificationDate(d);
		assertEquals(d, dur.getModificationDate());

		dur.setValidDayFrom(1);
		assertEquals(1, dur.getValidDayFrom().intValue());

		dur.setValidDayTo(1);
		assertEquals(1, dur.getValidDayTo().intValue());

		dur.setValidFrom(d);
		assertEquals(d, dur.getValidFrom());

		dur.setValidTo(d);
		assertEquals(d, dur.getValidTo());

		dur.setNextUserInNextDuration(b);
		assertEquals(b, dur.getNextUserInNextDuration());

		StandbyGroup group = new StandbyGroup();

		dur.setStandbyGroup(group);
		assertNotNull(dur.getStandbyGroup());
		assertEquals(group, dur.getStandbyGroup());

		StandbyDuration dur2 = dur.copy();

		assertEquals(null, dur2.getId());
		assertEquals(1, dur2.getValidDayFrom().intValue());
		assertEquals(1, dur2.getValidDayTo().intValue());
		assertEquals(d, dur2.getValidFrom());
		assertEquals(d, dur2.getValidTo());
		assertEquals(b, dur2.getNextUserInNextDuration());

		assertNotEquals(d.getTime(), dur2.getModificationDate().getTime());

	}

	@Test
	public void testCompare() throws MalformedURLException {
		StandbyDuration obj1 = new StandbyDuration();
		obj1.setId(5L);

		StandbyDuration obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		StandbyDuration obj3 = new StandbyDuration();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}

}
