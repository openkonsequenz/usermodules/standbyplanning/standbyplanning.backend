/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.core.controller;

import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.controller.AbstractController;
import org.eclipse.openk.sp.exceptions.SpException;
import org.springframework.transaction.TransactionSystemException;

public class TestController extends AbstractController {

	
	public String getSpException() throws SpException {
		throw new SpException();
	}
	
	public String getSpException1001() throws SpException {
		throw new SpException(new HttpStatusException(1001));
	}
	
	public String assertAndRefreshToken(String token, String[] secureType) throws HttpStatusException {
		throw new HttpStatusException(Integer.parseInt(token));
	}
	
	public String getHttpStatusException(String token) throws SpException {
		return "ok";
	}
	
	
	public String getTransactionSystemException1() throws SpException {
		throw new TransactionSystemException("test");
	}

}
