/*Standby_user History*/
CREATE TABLE IF NOT EXISTS STANDBY_USER_HIST (HIST_ID serial, operation char(1), stamp timestamp, LIKE STANDBY_USER EXCLUDING ALL);
--
-- Create a row in {0}.standby_user_hist_audit to reflect the operation performed on standby_user,
-- making use of the special variable TG_OP to work out the operation.
--
CREATE OR REPLACE FUNCTION process_standby_user_audit() RETURNS TRIGGER AS $object_audit$ BEGIN IF (TG_OP = 'DELETE') THEN INSERT INTO {0}.standby_user_hist SELECT nextval('standby_user_hist_hist_id_seq'), 'D', now(), OLD.*; ELSIF (TG_OP = 'UPDATE') THEN INSERT INTO {0}.standby_user_hist SELECT nextval('standby_user_hist_hist_id_seq'), 'U', now(), NEW.*; ELSIF (TG_OP = 'INSERT') THEN INSERT INTO {0}.standby_user_hist SELECT nextval('standby_user_hist_hist_id_seq'), 'I', now(), NEW.*; END IF; RETURN NULL; END; $object_audit$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS standby_user_audit on {0}.standby_user;
CREATE TRIGGER standby_user_audit AFTER INSERT OR UPDATE OR DELETE ON {0}.standby_user FOR EACH ROW EXECUTE PROCEDURE process_standby_user_audit();