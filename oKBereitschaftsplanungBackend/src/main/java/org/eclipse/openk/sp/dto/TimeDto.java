/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.Date;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class TimeDto extends AbstractDto {

	/***/
	private static final long serialVersionUID = 1L;

	private Integer hour;
	private Integer minute;
	private Integer second;
	/**
	 * @return the hour
	 */
	public Integer getHour() {
		return hour;
	}
	/**
	 * @param hour the hour to set
	 */
	public void setHour(Integer hour) {
		this.hour = hour;
	}
	/**
	 * @return the minute
	 */
	public Integer getMinute() {
		return minute;
	}
	/**
	 * @param minute the minute to set
	 */
	public void setMinute(Integer minute) {
		this.minute = minute;
	}
	/**
	 * @return the second
	 */
	public Integer getSecond() {
		return second;
	}
	/**
	 * @param second the second to set
	 */
	public void setSecond(Integer second) {
		this.second = second;
	}
	
	public Date getAsDate() {
		String timeString = ""+getHour()+":"+getMinute()+":"+getSecond();
		DateTimeFormatter formatter = DateTimeFormat.forPattern("HH:mm:ss");
		return formatter.parseDateTime(timeString).toDate();
	}
	
	public void storeDate(Date date) {
		DateTime dateTime = new DateTime(date);
		this.setHour(dateTime.getHourOfDay());
		this.setMinute(dateTime.getMinuteOfHour());
		this.setSecond(dateTime.getSecondOfMinute());
	}
	
}
