/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class LabelValueDtoTest {

	@Test
	public void testGettersAndSetters() {
		String testString = "TEST";

		LabelValueDto dto = new LabelValueDto();
		dto.setLabel(testString);
		assertEquals(testString, dto.getLabel());

		dto.setValue(1L);
		assertTrue(dto.getValue().longValue() == 1L);

		List<LocationForBranchDto> list = new ArrayList<LocationForBranchDto>();
		dto.setLsLocationForBranches(list);
		assertEquals(list, dto.getLsLocationForBranches());
	}
}
