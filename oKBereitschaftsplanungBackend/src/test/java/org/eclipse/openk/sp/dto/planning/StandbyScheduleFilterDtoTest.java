/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

public class StandbyScheduleFilterDtoTest {

	@Test
	public void testGettersAndSetters() {
		Date d = new Date();
		Long id = new Long(2);
		String text = "test";

		StandbyScheduleFilterDto dto = new StandbyScheduleFilterDto();

		dto.setComment(text);
		assertEquals(text, dto.getComment());

		dto.setStandbyListId(id);
		assertEquals(id, dto.getStandbyListId());

		dto.setTitle(text);
		assertEquals(text, dto.getTitle());

		dto.setValidFrom(d);
		assertEquals(d, dto.getValidFrom());

		dto.setValidTo(d);
		assertEquals(d, dto.getValidTo());

	}
}
