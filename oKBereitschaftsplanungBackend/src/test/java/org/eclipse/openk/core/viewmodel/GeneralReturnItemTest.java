/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.core.viewmodel;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.eclipse.openk.common.JsonGeneratorBase;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;

public class GeneralReturnItemTest {
	// IMPORTANT TEST!!!
	// Make sure, our Interface produces a DEFINED Json!
	// Changes in the interface will HOPEFULLY crash here!!!

	@Test
	public void TestStructureAgainstJson() {
		FileHelperTest fh = new FileHelperTest(); 
		
		String json = fh.loadStringFromResource("testGeneralReturnItem.json");
		GeneralReturnItem gRRet = JsonGeneratorBase.getGson().fromJson(json, GeneralReturnItem.class);
		assertEquals("It works!", gRRet.getRet());
	}

	@Test
	public void TestSetters() {
		GeneralReturnItem gri = new GeneralReturnItem("firstStrike");
		assertTrue("firstStrike".equals(gri.getRet()));
		gri.setRet("Retour");
		assertTrue("Retour".equals(gri.getRet()));

	}

}
