/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.report;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;

public class ReportInputDto extends AbstractDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private ReportDto reportDto;
	private StandbyScheduleBody standbyScheduleBody;
	private ReportGroupDto reportGroupDto;
	private int groupSort;

	public ReportInputDto() {

	}

	public ReportDto getReportDto() {
		return reportDto;
	}

	public void setReportDto(ReportDto reportDto) {
		this.reportDto = reportDto;
	}

	public StandbyScheduleBody getStandbyScheduleBody() {
		return this.standbyScheduleBody;
	}

	public void setStandbyScheduleBody(StandbyScheduleBody standbyScheduleBody) {
		this.standbyScheduleBody = standbyScheduleBody;
	}

	public ReportGroupDto getReportGroupDto() {
		return reportGroupDto;
	}

	public void setReportGroupDto(ReportGroupDto reportGroupDto) {
		this.reportGroupDto = reportGroupDto;
	}

	public int getGroupSort() {
		return groupSort;
	}

	public void setGroupSort(int groupSort) {
		this.groupSort = groupSort;
	}

}
