/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.resources.BaseResource;
import org.eclipse.openk.sp.annotations.Compress;
import org.eclipse.openk.sp.controller.SearchController;
import org.eclipse.openk.sp.controller.StandbyScheduleController;
import org.eclipse.openk.sp.controller.planning.ArchiveController;
import org.eclipse.openk.sp.controller.planning.PlanningController;
import org.eclipse.openk.sp.dto.StandbyScheduleBlueprintDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySearchDto;
import org.eclipse.openk.sp.dto.StandbyScheduleDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgResponseDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleActionDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleCopyDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleSearchDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.ApiParam;

@Path("standbyschedule")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class StandbyScheduleRestService extends BaseResource {

	@Autowired
	private StandbyScheduleController standbyScheduleController;

	@Autowired
	private PlanningController planningController;

	@Autowired
	private SearchController searchController;

	@Autowired
	private ArchiveController archiveController;

	public StandbyScheduleRestService() {
		super(Logger.getLogger(StandbyScheduleRestService.class.getName()), new FileHelper());
	}

	@GET
	@Compress
	@Path("/import/archive/{id}")
	public Response importArchiveHeader(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<PlanningMsgResponseDto> invokable = modusr -> archiveController.importPlan(id, modusr);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Compress
	@Path("/filter/bodies/{statusId}")
	public Response filterPlanningDataByStatusId(@PathParam("statusId") Long statusId,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			StandbyScheduleFilterDto standbyScheduleFilterDto) {

		ModifyingInvokable<StandbyScheduleDto> invokable = modusr -> standbyScheduleController
				.getFilteredPlanByStatusId(standbyScheduleFilterDto, statusId);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN,
				Globals.KEYCLOAK_ROLE_BP_GRUPPENLEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Compress
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/filter/bodies/statusId/{statusId}/zip")
	public Response filterPlanningDataByStatusIdAsZip(@PathParam("statusId") Long statusId,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			StandbyScheduleFilterDto standbyScheduleFilterDto) {

		ModifyingInvokable<StandbyScheduleDto> invokable = modusr -> standbyScheduleController
				.getFilteredPlanByStatusId(standbyScheduleFilterDto, statusId);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN,
				Globals.KEYCLOAK_ROLE_BP_GRUPPENLEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/planning/blueprint/calculate")
	public Response calculate(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			StandbyScheduleBlueprintDto standbyBlueprintDto) {

		standbyBlueprintDto.setStatusId(1L);
		ModifyingInvokable<PlanningMsgResponseDto> invokable = modusr -> standbyScheduleController
				.calculatePlan(standbyBlueprintDto, modusr);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/copy/bodies")
	public Response copyBodies(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			StandbyScheduleCopyDto standbyScheduleCopyDto) {

		ModifyingInvokable<PlanningMsgResponseDto> invokable = modusr -> standbyScheduleController
				.copyBodyData(standbyScheduleCopyDto, modusr);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		Response response = invokeRunnable(jwt, securityRoles, invokable);

		return response;
	}

	@PUT
	@Path("/delete/bodies/{statusId}")
	public Response deleteBodies(@PathParam("statusId") Long statusId,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			StandbyScheduleCopyDto standbyScheduleCopyDto) {

		standbyScheduleCopyDto.setStatusId(statusId);
		ModifyingInvokable<PlanningMsgResponseDto> invokable = modusr -> standbyScheduleController
				.deleteBodyData(standbyScheduleCopyDto, modusr);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		Response response = invokeRunnable(jwt, securityRoles, invokable);

		return response;
	}

	@PUT
	@Path("/planning/user/replace")
	public Response replaceUserInPlan(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			StandbyScheduleActionDto actionDto) throws SpException {

		actionDto.createDatesFromStrings();
		ModifyingInvokable<PlanningMsgResponseDto> invokable = modusr -> planningController.replaceUserInPlan(actionDto,
				modusr);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN,
				Globals.KEYCLOAK_ROLE_BP_GRUPPENLEITER };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/planning/user/move")
	public Response moveUserInPlan(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			StandbyScheduleActionDto actionDto) throws SpException {

		actionDto.createDatesFromStrings();
		ModifyingInvokable<PlanningMsgResponseDto> invokable = modusr -> planningController.moveUserInPlan(actionDto,
				modusr);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Compress
	@Path("/searchnow")
	public Response searchnow(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			StandbyScheduleSearchDto searchDto) {

		ModifyingInvokable<List<StandbyScheduleBodySearchDto>> invokable = modusr -> searchController
				.searchNow(searchDto, jwt);

		String[] securityRoles = Globals.getAllRolls();

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}
}
