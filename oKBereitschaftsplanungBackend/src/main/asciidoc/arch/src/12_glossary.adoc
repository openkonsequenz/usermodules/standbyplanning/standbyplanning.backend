[[section-glossary]]
== Glossary

.Abbreviations and glossary terms
[options="header"]
|========================================================
|Short|Long|German|Description
|AC|Architecture Committee|Architektur-Komittee|Gives Framework and Constraints according to architecture for oK projects.
|DAO|Data Access Objects||
|DTO|Data Transfer Object||
|EPL|Eclipse Public License||Underlying license model for Eclipse projects like elogbook@openK
|REST|Representational State Transfer||Representational State Transfer bezeichnet ein Programmierparadigma für verteilte Systeme, insbesondere für Webservices.
|oK|openKONSEQUENZ|openKONSEQUENZ|Name of the consortium of DSOs
|QC|Quality Committee|Qualitätskomitee|Gives framework and constraints according to quality for oK projects.
|========================================================

