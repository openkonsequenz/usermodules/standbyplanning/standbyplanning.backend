/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.validation.validator;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.planning.PlanningController;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.db.model.StandbyStatus;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.DateHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/** Class to validate if all time slots are set for a group. */
@RunWith(MockitoJUnitRunner.class)
public class DoublePlannedValidatorTest {
	protected static final Logger LOGGER = Logger.getLogger(DoublePlannedValidatorTest.class);

	@Mock
	private PlanningController planningController;

	@Mock
	private StandbyScheduleBodyRepository scheduleBodyRepository;

	@InjectMocks
	DoublePlannedValidator doublePlannedValidator;

	@Test
	public void executeTest() throws SpException {

		Date from = DateHelper.getDate(2018, 9, 9);
		Date till = DateHelper.getDate(2018, 9, 10);
		StandbyGroup group1 = new StandbyGroup();
		group1.setId(1L);
		group1.setTitle("title");

		StandbyGroup group2 = new StandbyGroup();
		group2.setId(2L);
		group2.setTitle("title2");

		StandbyStatus status = new StandbyStatus();
		status.setId(1L);
		status.setTitle("TestEbene");

		List<StandbyGroup> lsStandbyGroups = new ArrayList<>();
		lsStandbyGroups.add(group1);
		lsStandbyGroups.add(group1);
		Long statusId = new Long(1);

		User user1 = new User();
		user1.setId(1L);
		user1.setFirstname("First");
		user1.setLastname("LastName");

		User user2 = new User();
		user2.setId(2L);
		user2.setFirstname("First2");
		user2.setLastname("LastName2");

		StandbyScheduleBody body1 = new StandbyScheduleBody();
		body1.setId(1L);
		body1.setUser(user1);
		body1.setValidFrom(from);
		body1.setValidTo(till);
		body1.setStatus(status);
		body1.setStandbyGroup(group1);

		StandbyScheduleBody body2 = new StandbyScheduleBody();
		body2.setId(2L);
		body2.setUser(user2);
		body2.setValidFrom(from);
		body2.setValidTo(till);
		body2.setStatus(status);
		body2.setStandbyGroup(group2);

		StandbyScheduleBody body3 = new StandbyScheduleBody();
		body3.setId(3L);
		body3.setUser(user1);
		body3.setValidFrom(from);
		body3.setValidTo(till);
		body3.setStatus(status);
		body3.setStandbyGroup(group1);

		// add bodies to lists
		List<StandbyScheduleBody> lsScheduleBodies = new ArrayList<>();
		lsScheduleBodies.add(body1);
		lsScheduleBodies.add(body2);

		List<StandbyScheduleBody> lsAlternativeScheduleBodies = new ArrayList<>();
		lsAlternativeScheduleBodies.add(body1);
		lsAlternativeScheduleBodies.add(body3);

		// positive test
		Mockito.when(scheduleBodyRepository.findByGroupAndDateAndStatus(Mockito.any(), Mockito.any(), Mockito.any(),
				Mockito.any())).thenReturn(lsScheduleBodies);
		Mockito.when(scheduleBodyRepository.findByUserHittingDateInterval(Mockito.any(), Mockito.any(), Mockito.any()))
				.thenReturn(lsAlternativeScheduleBodies);
		List<PlanningMsgDto> planningMsgDtos = doublePlannedValidator.execute(from, till, group1, lsStandbyGroups,
				statusId, null);
		assertNotNull(planningMsgDtos);

		// positive test - without group
		Mockito.when(scheduleBodyRepository.findByUserHittingDateInterval(Mockito.any(), Mockito.any(), Mockito.any()))
				.thenReturn(null);
		List<PlanningMsgDto> planningMsgDtos2 = doublePlannedValidator.execute(from, till, group1, lsStandbyGroups,
				statusId, null);
		assertNotNull(planningMsgDtos2);
	}
}
