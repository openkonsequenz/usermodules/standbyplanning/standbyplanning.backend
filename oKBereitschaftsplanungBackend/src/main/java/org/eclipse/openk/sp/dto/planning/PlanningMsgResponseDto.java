/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.eclipse.openk.sp.abstracts.AbstractDto;

/**
 * The "PlanningMsgResponseDto" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "PlanningBodyResultDto")
@JsonInclude(Include.NON_NULL)
public class PlanningMsgResponseDto extends AbstractDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<PlanningMsgDto> lsMsg = new ArrayList<>();

	/**
	 * @return the lsMsg
	 */
	public List<PlanningMsgDto> getLsMsg() {
		return lsMsg;
	}

	/**
	 * @param lsMsg the lsMsg to set
	 */
	public void setLsMsg(List<PlanningMsgDto> lsMsg) {
		this.lsMsg = lsMsg;
	}
}
