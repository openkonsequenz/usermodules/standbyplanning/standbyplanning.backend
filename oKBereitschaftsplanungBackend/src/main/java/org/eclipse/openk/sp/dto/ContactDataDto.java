/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "USER" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "ContactDataDto")
@JsonInclude(Include.NON_NULL)
public class ContactDataDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	@NotNull(message = "Firma oder Privat setzen")
	private Boolean isPrivate;
	@NotNull(message = "Mobil ist nicht gesetzt")
	@Size(max = 256, message = "Mobil mit max. 256 Zeichen")
	private String phone;
	@Size(max = 256, message = "Telefon mit max. 256 Zeichen")
	private String cellphone;
	@Size(max = 256, message = "Funk mit max. 256 Zeichen")
	private String radiocomm;
	@Size(max = 256, message = "E-Mail mit max. 256 Zeichen")
	private String email;
	@Size(max = 256, message = "Pager mit max. 256 Zeichen")
	private String pager;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the isPrivate
	 */
	public Boolean getIsPrivate() {
		return isPrivate;
	}

	/**
	 * @param isPrivate the isPrivate to set
	 */
	public void setIsPrivate(Boolean isPrivate) {
		this.isPrivate = isPrivate;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the cellphone
	 */
	public String getCellphone() {
		return cellphone;
	}

	/**
	 * @param cellphone the cellphone to set
	 */
	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	/**
	 * @return the radiocomm
	 */
	public String getRadiocomm() {
		return radiocomm;
	}

	/**
	 * @param radiocomm the radiocomm to set
	 */
	public void setRadiocomm(String radiocomm) {
		this.radiocomm = radiocomm;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the pager
	 */
	public String getPager() {
		return pager;
	}

	/**
	 * @param pager the pager to set
	 */
	public void setPager(String pager) {
		this.pager = pager;
	}
}
