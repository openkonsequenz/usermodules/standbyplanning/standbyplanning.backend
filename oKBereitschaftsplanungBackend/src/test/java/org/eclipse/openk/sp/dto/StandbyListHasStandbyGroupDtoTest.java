/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class StandbyListHasStandbyGroupDtoTest {

	@Test
	public void testGettersAndSetters() {

		StandbyListHasStandbyGroupDto slhsg = new StandbyListHasStandbyGroupDto();
		Long id = 1L;
		String text = "text";

		slhsg.setId(id);
		assertEquals(id.longValue(), slhsg.getId().longValue());

		slhsg.setPosition(1);
		assertEquals(1, slhsg.getPosition().intValue());

		slhsg.setStandbyGroupId(id);
		assertEquals(id, slhsg.getStandbyGroupId());

		slhsg.setStandbyGroupName(text);
		assertEquals(text, slhsg.getStandbyGroupName());

		slhsg.setStandbyListId(id);
		assertEquals(id, slhsg.getStandbyListId());

	}
}
