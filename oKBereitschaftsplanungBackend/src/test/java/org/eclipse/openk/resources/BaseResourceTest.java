/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.resources;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.core.viewmodel.ErrorReturn;
import org.eclipse.openk.sp.controller.external.AuthController;
import org.eclipse.openk.sp.exceptions.SpNestedException;
import org.eclipse.openk.sp.util.FileHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BaseResourceTest {

	private String payloadUser_bp_admin = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJMc1FkUElQa2V1Y210UkczTmx3SndXbVV1MjlybVR2MGtZZl9Ja0ZuLU5FIn0.eyJqdGkiOiJmNGIzODJhNy00MDllLTRjYjUtOTI0NC1mNWRmN2Y4YTBlYjAiLCJleHAiOjE1Mjg5ODY1OTgsIm5iZiI6MCwiaWF0IjoxNTI4OTg2Mjk4LCJpc3MiOiJodHRwOi8vMTAuMS4xOC4xNzU6ODM4MC9hdXRoL3JlYWxtcy9PcGVuS1JlYWxtIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6IjY0MzIwODJiLTdkNTEtNGM3NC1iZGUwLTViM2VlNTg5MGE4YyIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJlNjYwNzAxYi0wYTExLTQ1NjAtYjBjMC0zN2UzZjFkNmFhMjgiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIkJQX0FkbWluIiwiQlBfU2FjaGJlYXJiZWl0ZXIiLCJCUF9HcnVwcGVubGVpdGVyIiwiQlBfTGVzZWJlcmVjaHRpZ3RlIiwidW1hX2F1dGhvcml6YXRpb24iLCJwbGFubmluZy1hY2Nlc3MiXX0sInJlc291cmNlX2FjY2VzcyI6eyJzdGFuZGJ5cGxhbm5pbmciOnsicm9sZXMiOlsiQlBfQWRtaW4iXX0sImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInJvbGVzIjoiW3VtYV9hdXRob3JpemF0aW9uLCBwbGFubmluZy1hY2Nlc3MsIEJQX0dydXBwZW5sZWl0ZXIsIEJQX1NhY2hiZWFyYmVpdGVyLCBvZmZsaW5lX2FjY2VzcywgQlBfQWRtaW4sIEJQX0xlc2ViZXJlY2h0aWd0ZV0iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9hZG1pbiJ9.ejcrQJWCyY9rWEnQ-hMW9MzVbzx5h7jCvv3SO8krm03shFHZOprkLAvCb6vKJjGDapX7H58ctS-p6i-v4kW34ocnV7gqMof0dq6m42uKUhsA61urqb8QkvWDd03p1cF6l73qNKhOXe9eJBSgyvYenvzWf9auFeBv2CpmsVojos1iwdhZP0D_vBPqCaoL4BnF974gdqG3Vp49JQDAMpKdxMB2RsXMO01Flxu0mXPp8ejqMnh8LMFyKrNshWdRuh4HlRLs7fUvi5CN-9-DcuhJ9IzIs7WupK1Ot6VZSF6l0VESfOxmHVDBGB7eCsN9sB0P00-elMO-irCGRrzAnTr7OQ";
	private String errorToken = "https://169.50.13.155/elogbookFE/?accessToken=eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJHN0MtQ1E4ckdYUGpzSlY0R0xEWHI2dTFiOUwzV1pYVkxEa1VJOWV1d1FnIn0.eyJqdGkiOiI3NDNlOGQ2MC03ZGQwLTRjMjktYjBmYi1jNTE0MTM5ZjQ1ZmEiLCJleHAiOjE1Mjk4MjIzMzAsIm5iZiI6MCwiaWF0IjoxNTI5ODIyMDMwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgzODAvYXV0aC9yZWFsbXMvT3BlbktSZWFsbSIsImF1ZCI6ImVsb2dib29rLWJhY2tlbmQiLCJzdWIiOiJmYjI5NzE2NS00YzE2LTRmYzItOTk3Mi1hZjNhYmY5MDk0ZDgiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJlbG9nYm9vay1iYWNrZW5kIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiYTRkOTg3M2EtMjBhNi00MWExLTk1NTItYWYyZjYwZjMyNDIwIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJCUF9BZG1pbiIsIkJQX1NhY2hiZWFyYmVpdGVyIiwiQlBfR3J1cHBlbmxlaXRlciIsIkJQX0xlc2ViZXJlY2h0aWd0ZSIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5pbmctYWNjZXNzIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwicm9sZXMiOiJbQlBfR3J1cHBlbmxlaXRlciwgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBCUF9BZG1pbiwgQlBfU2FjaGJlYXJiZWl0ZXIsIHBsYW5uaW5nLWFjY2VzcywgQlBfTGVzZWJlcmVjaHRpZ3RlXSIsIm5hbWUiOiJhZCBtaW4iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9hZG1pbiIsImdpdmVuX25hbWUiOiJhZCIsImZhbWlseV9uYW1lIjoibWluIn0.iPJ43248IHUDpUFj0OAZAdFvE9WnYa_EANIf9Ep14jviDxu89V_DynVN0pPA52Un7LGyUD4FzVYn033RtiahRkPjA3dd4U20hCZwI7GoW1xV5C9PuLjIYYZ43_vL0f6pSSMDHaP_ACU_bcviBcmRFO15IHOQ1c-CmDUtGVVY_xYzjvww2E_U1a_qEgYN1W4qrTxJkCLqdXOJ2TUVWM_DPxfT8At4yeBVz2ssZxOqyP2F5eRHQcvmwSIbveTNz9uV_uNHYH8eob2xRZMKYupYqo8pBIoquw_Djk337mCgamlhAf9J_75Y-BbQampZYw9I6OVBFlQYXV8_ZcCSrcSJHw";
	private String accessTokenURL = "http://169.50.13.155/spfe/?accessToken=eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJHN0MtQ1E4ckdYUGpzSlY0R0xEWHI2dTFiOUwzV1pYVkxEa1VJOWV1d1FnIn0.eyJqdGkiOiI3NDNlOGQ2MC03ZGQwLTRjMjktYjBmYi1jNTE0MTM5ZjQ1ZmEiLCJleHAiOjE1Mjk4MjIzMzAsIm5iZiI6MCwiaWF0IjoxNTI5ODIyMDMwLCJpc3MiOiJodHRwOi8vbG9jYWxob3N0OjgzODAvYXV0aC9yZWFsbXMvT3BlbktSZWFsbSIsImF1ZCI6ImVsb2dib29rLWJhY2tlbmQiLCJzdWIiOiJmYjI5NzE2NS00YzE2LTRmYzItOTk3Mi1hZjNhYmY5MDk0ZDgiLCJ0eXAiOiJCZWFyZXIiLCJhenAiOiJlbG9nYm9vay1iYWNrZW5kIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiYTRkOTg3M2EtMjBhNi00MWExLTk1NTItYWYyZjYwZjMyNDIwIiwiYWNyIjoiMSIsImFsbG93ZWQtb3JpZ2lucyI6WyIqIl0sInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJCUF9BZG1pbiIsIkJQX1NhY2hiZWFyYmVpdGVyIiwiQlBfR3J1cHBlbmxlaXRlciIsIkJQX0xlc2ViZXJlY2h0aWd0ZSIsInVtYV9hdXRob3JpemF0aW9uIiwicGxhbm5pbmctYWNjZXNzIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwicm9sZXMiOiJbQlBfR3J1cHBlbmxlaXRlciwgb2ZmbGluZV9hY2Nlc3MsIHVtYV9hdXRob3JpemF0aW9uLCBCUF9BZG1pbiwgQlBfU2FjaGJlYXJiZWl0ZXIsIHBsYW5uaW5nLWFjY2VzcywgQlBfTGVzZWJlcmVjaHRpZ3RlXSIsIm5hbWUiOiJhZCBtaW4iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9hZG1pbiIsImdpdmVuX25hbWUiOiJhZCIsImZhbWlseV9uYW1lIjoibWluIn0.iPJ43248IHUDpUFj0OAZAdFvE9WnYa_EANIf9Ep14jviDxu89V_DynVN0pPA52Un7LGyUD4FzVYn033RtiahRkPjA3dd4U20hCZwI7GoW1xV5C9PuLjIYYZ43_vL0f6pSSMDHaP_ACU_bcviBcmRFO15IHOQ1c-CmDUtGVVY_xYzjvww2E_U1a_qEgYN1W4qrTxJkCLqdXOJ2TUVWM_DPxfT8At4yeBVz2ssZxOqyP2F5eRHQcvmwSIbveTNz9uV_uNHYH8eob2xRZMKYupYqo8pBIoquw_Djk337mCgamlhAf9J_75Y-BbQampZYw9I6OVBFlQYXV8_ZcCSrcSJHw";

	private Logger LOGGER = Logger.getLogger(BaseResourceTest.class.getName());

	@Mock
	AuthController authController;

	@Mock
	FileHelper fileHelper;

	@InjectMocks
	BaseResource baseResource = new BaseResource(LOGGER, fileHelper);

	@Test
	public void isBackdoor() {
		// backdoor is only available when the version(POM) contains "DEVELOP" or
		// "SNAPSHOT"

		baseResource.isDevelopMode(true);

		String token = "LET_ME_IN";

		boolean backdoor = baseResource.isBackdoor(token);
		assertEquals(true, backdoor);

		baseResource.isDevelopMode(false);
		backdoor = baseResource.isBackdoor(token);
		assertEquals(false, backdoor);

	}

	@Test
	public void assertAndRefreshToken() {
		// backdoor is only available when the version(POM) contains "DEVELOP" or
		// "SNAPSHOT"

		String token = "LET_ME_IN";
		String[] secureType = { Globals.KEYCLOAK_ROLE_BP_ADMIN };

		try {
			baseResource.assertAndRefreshToken(token, secureType);
		} catch (HttpStatusException e) {
			assertNotNull(e);
		}

	}

	@Test(expected = HttpStatusException.class)
	public void assertAndRefreshTokenExceptionTest() throws IOException, HttpStatusException {
		// backdoor is only available when the version(POM) contains "DEVELOP" or
		// "SNAPSHOT"

		String[] secureType = { Globals.KEYCLOAK_ROLE_BP_ADMIN };

		Mockito.when(fileHelper.getProperty(Mockito.anyString(), Mockito.anyString()))
				.thenThrow(new IOException("error"));

		Mockito.when(authController.performGetRequest(Mockito.anyBoolean(), Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString())).thenThrow(new SpNestedException(new ErrorReturn(500, "Error")));

		Mockito.when(authController.validateResponse(Mockito.any())).thenReturn(true);

		baseResource.assertAndRefreshToken(payloadUser_bp_admin, secureType);

	}

	@Test
	public void assertAndRefreshToken2() {
		// backdoor is only available when the version(POM) contains "DEVELOP" or
		// "SNAPSHOT"

		String[] secureType = { Globals.KEYCLOAK_ROLE_BP_ADMIN };

		try {
			baseResource.assertAndRefreshToken(payloadUser_bp_admin, secureType);
		} catch (Exception e) {
			assertNotNull(e);
		}

	}

	@Test
	public void checkAuthTest() throws SpNestedException {

		String[] secureType = { Globals.KEYCLOAK_ROLE_BP_ADMIN };

		HttpResponse response = null;
		Mockito.when(authController.performGetRequest(Mockito.anyBoolean(), Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString())).thenReturn(response);

		Mockito.when(authController.validateResponse(Mockito.any())).thenReturn(true);

		Throwable th = null;
		try {
			baseResource.checkAuth("true", accessTokenURL, payloadUser_bp_admin, secureType);
		} catch (Exception e) {
			th = e;
		}
		assertNull(th);
	}

	@Test(expected = HttpStatusException.class)
	public void checkAuthExceptionTest() throws HttpStatusException {
		// backdoor is only available when the version(POM) contains "DEVELOP" or
		// "SNAPSHOT"

		String[] secureType = { Globals.KEYCLOAK_ROLE_BP_ADMIN };

		Mockito.when(authController.performGetRequest(Mockito.anyBoolean(), Mockito.anyString(), Mockito.anyString(),
				Mockito.anyString())).thenThrow(new SpNestedException(new ErrorReturn(500, "Error")));

		Mockito.when(authController.validateResponse(Mockito.any())).thenReturn(true);

		baseResource.checkAuth("true", accessTokenURL, payloadUser_bp_admin, secureType);

	}

	@Test
	public void initTest() throws HttpStatusException, IOException {
		// backdoor is only available when the version(POM) contains "DEVELOP" or
		// "SNAPSHOT"

		Mockito.when(fileHelper.getProperty(Mockito.anyString(), Mockito.anyString()))
				.thenThrow(new IOException("error"));

		String result = baseResource.init("test", "test");
		assertNull(result);

	}

	@Test
	public void assertAndRefreshToken3() {
		// backdoor is only available when the version(POM) contains "DEVELOP" or
		// "SNAPSHOT"

		String[] secureType = { Globals.KEYCLOAK_ROLE_BP_ADMIN };

		try {
			baseResource.assertAndRefreshToken(payloadUser_bp_admin, secureType);
		} catch (Exception e) {
			assertNotNull(e);
		}

	}

}
