/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;

public class LocationForBranchDtoTest {
	
	@Test
	public void getterAndSetterTest() {
		LocationForBranchDto dto = new LocationForBranchDto();
		
		dto.setBranchId(1l);
		assertNotNull(dto.getBranchId());
		assertTrue(dto.getBranchId()==1);
		
		dto.setId(1l);
		assertNotNull(dto.getId());
		assertTrue(dto.getId()==1);
		
		dto.setLocationId(1l);
		assertNotNull(dto.getLocationId());
		assertTrue(dto.getLocationId()==1);
		
		dto.setTitle("Title");
		assertNotNull(dto.getTitle());
		assertTrue(dto.getTitle().equals("Title"));
		
		dto.setValidFrom(new Date());
		assertNotNull(dto.getValidFrom());
		
		dto.setValidTo(new Date());
		assertNotNull(dto.getValidTo());
	}
}