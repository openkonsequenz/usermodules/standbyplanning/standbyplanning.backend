/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import org.eclipse.openk.sp.controller.external.dto.DistanceServiceDto;
import org.junit.Test;

public class DistanceServiceDtoTest {

	@Test
	public void testGettersAndSetters() {

		String text = "text";

		Long id = 1L;
		DistanceServiceDto body = new DistanceServiceDto();

		body.setBodyId(id);
		assertEquals(id.longValue(), body.getBodyId().longValue());

		body.setOrganisationDistance(text);
		assertEquals(text, body.getOrganisationDistance());

		body.setPrivateDisance(text);
		assertEquals(text, body.getPrivateDisance());

		body.setUserId(id);
		assertEquals(id.longValue(), body.getUserId().longValue());

		LocationDto location = new LocationDto();
		location.setId(id);
		body.setLocation(location);
		assertEquals(location, body.getLocation());

		OrganisationDto organisation = new OrganisationDto();
		organisation.setId(id);
		body.setOrganisation(organisation);
		assertEquals(organisation, body.getOrganisation());

		AddressDto privateAddress = new AddressDto();
		privateAddress.setId(id);
		body.setPrivateAddress(privateAddress);
		assertEquals(privateAddress, body.getPrivateAddress());

	}

}
