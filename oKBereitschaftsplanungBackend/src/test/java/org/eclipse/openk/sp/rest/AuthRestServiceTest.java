/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import javax.ws.rs.core.Response;

import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.controller.external.AuthController;
import org.eclipse.openk.sp.controller.external.ExternalInterfaceMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AuthRestServiceTest {

	private String payloadUser_bp_admin = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJMc1FkUElQa2V1Y210UkczTmx3SndXbVV1MjlybVR2MGtZZl9Ja0ZuLU5FIn0.eyJqdGkiOiJmNGIzODJhNy00MDllLTRjYjUtOTI0NC1mNWRmN2Y4YTBlYjAiLCJleHAiOjE1Mjg5ODY1OTgsIm5iZiI6MCwiaWF0IjoxNTI4OTg2Mjk4LCJpc3MiOiJodHRwOi8vMTAuMS4xOC4xNzU6ODM4MC9hdXRoL3JlYWxtcy9PcGVuS1JlYWxtIiwiYXVkIjoiZWxvZ2Jvb2stYmFja2VuZCIsInN1YiI6IjY0MzIwODJiLTdkNTEtNGM3NC1iZGUwLTViM2VlNTg5MGE4YyIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVsb2dib29rLWJhY2tlbmQiLCJhdXRoX3RpbWUiOjAsInNlc3Npb25fc3RhdGUiOiJlNjYwNzAxYi0wYTExLTQ1NjAtYjBjMC0zN2UzZjFkNmFhMjgiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbIioiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIkJQX0FkbWluIiwiQlBfU2FjaGJlYXJiZWl0ZXIiLCJCUF9HcnVwcGVubGVpdGVyIiwiQlBfTGVzZWJlcmVjaHRpZ3RlIiwidW1hX2F1dGhvcml6YXRpb24iLCJwbGFubmluZy1hY2Nlc3MiXX0sInJlc291cmNlX2FjY2VzcyI6eyJzdGFuZGJ5cGxhbm5pbmciOnsicm9sZXMiOlsiQlBfQWRtaW4iXX0sImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyIsInZpZXctcHJvZmlsZSJdfX0sInJvbGVzIjoiW3VtYV9hdXRob3JpemF0aW9uLCBwbGFubmluZy1hY2Nlc3MsIEJQX0dydXBwZW5sZWl0ZXIsIEJQX1NhY2hiZWFyYmVpdGVyLCBvZmZsaW5lX2FjY2VzcywgQlBfQWRtaW4sIEJQX0xlc2ViZXJlY2h0aWd0ZV0iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJicF9hZG1pbiJ9.ejcrQJWCyY9rWEnQ-hMW9MzVbzx5h7jCvv3SO8krm03shFHZOprkLAvCb6vKJjGDapX7H58ctS-p6i-v4kW34ocnV7gqMof0dq6m42uKUhsA61urqb8QkvWDd03p1cF6l73qNKhOXe9eJBSgyvYenvzWf9auFeBv2CpmsVojos1iwdhZP0D_vBPqCaoL4BnF974gdqG3Vp49JQDAMpKdxMB2RsXMO01Flxu0mXPp8ejqMnh8LMFyKrNshWdRuh4HlRLs7fUvi5CN-9-DcuhJ9IzIs7WupK1Ot6VZSF6l0VESfOxmHVDBGB7eCsN9sB0P00-elMO-irCGRrzAnTr7OQ";

	@InjectMocks
	private AuthRestService authRestService = new AuthRestService();

	@Mock
	AuthController authController;

	@Mock
	ExternalInterfaceMapper externalInterfaceMapper;

	@Test
	public void loginTest() throws IOException, HttpStatusException {

		Response response = Response.status(200).build();
		Mockito.when(externalInterfaceMapper.authLogin(Mockito.any())).thenReturn(response);

		Response result = authRestService.login(payloadUser_bp_admin);
		assertNotNull(result);

	}

	@Test
	public void loginExceptionTest() throws Exception {

		Response response = Response.status(500).build();

		Mockito.when(externalInterfaceMapper.authLogin(Mockito.any())).thenReturn(response);

		Response result = authRestService.login(payloadUser_bp_admin);
		assertNotNull(result);
		assertEquals(500, result.getStatus());

	}

	@Test
	public void logoutExceptionTest() throws Exception {

		Response response = Response.status(500).build();

		Mockito.when(externalInterfaceMapper.authLogout(Mockito.any())).thenReturn(response);

		Response result = authRestService.logout(payloadUser_bp_admin);
		assertNotNull(result);
		assertEquals(500, result.getStatus());

	}

	@Test
	public void loginException2Test() throws Exception {

		Response response = Response.status(500).build();

		Mockito.when(externalInterfaceMapper.authLogin(Mockito.any())).thenThrow(new HttpStatusException(500));
		Mockito.when(externalInterfaceMapper.responseFromException(Mockito.any())).thenReturn(response);

		Response result = authRestService.login(payloadUser_bp_admin);
		assertNotNull(result);
		assertEquals(500, result.getStatus());

	}

	@Test
	public void logoutException2Test() throws Exception {

		Response response = Response.status(500).build();

		Mockito.when(externalInterfaceMapper.authLogout(Mockito.any())).thenThrow(new HttpStatusException(500));
		Mockito.when(externalInterfaceMapper.responseFromException(Mockito.any())).thenReturn(response);

		Response result = authRestService.logout(payloadUser_bp_admin);
		assertNotNull(result);
		assertEquals(500, result.getStatus());

	}

	@Test
	public void logout() throws IOException, HttpStatusException {

		Response response = Response.status(200).build();
		Mockito.when(externalInterfaceMapper.authLogout(Mockito.any())).thenReturn(response);

		Response result = authRestService.logout(payloadUser_bp_admin);
		assertNotNull(result);

	}

}
