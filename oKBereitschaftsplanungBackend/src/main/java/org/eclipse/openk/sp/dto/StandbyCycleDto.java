/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The persistent class for the "STANDBY_CYCLE" database table.
 */
@XmlRootElement(name = "StandbyCycleDto")
@JsonInclude(Include.NON_NULL)
public class StandbyCycleDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String cycleName;
	private String handOverDay;
	private Date startTime;
	private Date endTime;
	private Date handOverOnHoliday;
	private Boolean isSpecialStandby;
	private String weekdayStart;
	private Date weekendStartTime;
	private String weekdayEnd;
	private Date weekendEndTime;

	public StandbyCycleDto() {
		/** default constructor. */
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the cycleName
	 */
	public String getCycleName() {
		return cycleName;
	}

	/**
	 * @param cycleName
	 *            the cycleName to set
	 */
	public void setCycleName(String cycleName) {
		this.cycleName = cycleName;
	}

	/**
	 * @return the handOverDay
	 */
	public String getHandOverDay() {
		return handOverDay;
	}

	/**
	 * @param handOverDay
	 *            the handOverDay to set
	 */
	public void setHandOverDay(String handOverDay) {
		this.handOverDay = handOverDay;
	}

	/**
	 * @return the startTime
	 */
	public Date getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public Date getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime
	 *            the endTime to set
	 */
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	/**
	 * @return the handOverOnHoliday
	 */
	public Date getHandOverOnHoliday() {
		return handOverOnHoliday;
	}

	/**
	 * @param handOverOnHoliday
	 *            the handOverOnHoliday to set
	 */
	public void setHandOverOnHoliday(Date handOverOnHoliday) {
		this.handOverOnHoliday = handOverOnHoliday;
	}

	/**
	 * @return the isSpecialStandby
	 */
	public Boolean getIsSpecialStandby() {
		return isSpecialStandby;
	}

	/**
	 * @param isSpecialStandby
	 *            the isSpecialStandby to set
	 */
	public void setIsSpecialStandby(Boolean isSpecialStandby) {
		this.isSpecialStandby = isSpecialStandby;
	}

	/**
	 * @return the weekdayStart
	 */
	public String getWeekdayStart() {
		return weekdayStart;
	}

	/**
	 * @param weekdayStart
	 *            the weekdayStart to set
	 */
	public void setWeekdayStart(String weekdayStart) {
		this.weekdayStart = weekdayStart;
	}

	/**
	 * @return the weekendStartTime
	 */
	public Date getWeekendStartTime() {
		return weekendStartTime;
	}

	/**
	 * @param weekendStartTime
	 *            the weekendStartTime to set
	 */
	public void setWeekendStartTime(Date weekendStartTime) {
		this.weekendStartTime = weekendStartTime;
	}

	/**
	 * @return the weekdayEnd
	 */
	public String getWeekdayEnd() {
		return weekdayEnd;
	}

	/**
	 * @param weekdayEnd
	 *            the weekdayEnd to set
	 */
	public void setWeekdayEnd(String weekdayEnd) {
		this.weekdayEnd = weekdayEnd;
	}

	/**
	 * @return the weekendEndTime
	 */
	public Date getWeekendEndTime() {
		return weekendEndTime;
	}

	/**
	 * @param weekendEndTime
	 *            the weekendEndTime to set
	 */
	public void setWeekendEndTime(Date weekendEndTime) {
		this.weekendEndTime = weekendEndTime;
	}

}
