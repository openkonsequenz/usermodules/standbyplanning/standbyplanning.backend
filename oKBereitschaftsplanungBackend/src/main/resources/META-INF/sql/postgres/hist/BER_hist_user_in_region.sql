/*user_in_region History*/
CREATE TABLE IF NOT EXISTS USER_IN_REGION_HIST (HIST_ID serial, operation char(1), stamp timestamp, LIKE USER_IN_REGION EXCLUDING ALL);
--
-- Create a row in {0}.user_in_region_hist_audit to reflect the operation performed on user_in_region,
-- making use of the special variable TG_OP to work out the operation.
--
CREATE OR REPLACE FUNCTION process_user_in_region_audit() RETURNS TRIGGER AS $object_audit$ BEGIN IF (TG_OP = 'DELETE') THEN INSERT INTO {0}.user_in_region_hist SELECT nextval('user_in_region_hist_hist_id_seq'), 'D', now(), OLD.*; ELSIF (TG_OP = 'UPDATE') THEN INSERT INTO {0}.user_in_region_hist SELECT nextval('user_in_region_hist_hist_id_seq'), 'U', now(), NEW.*; ELSIF (TG_OP = 'INSERT') THEN INSERT INTO {0}.user_in_region_hist SELECT nextval('user_in_region_hist_hist_id_seq'), 'I', now(), NEW.*; END IF; RETURN NULL; END; $object_audit$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS user_in_region_audit on {0}.user_in_region;
CREATE TRIGGER user_in_region_audit AFTER INSERT OR UPDATE OR DELETE ON {0}.user_in_region FOR EACH ROW EXECUTE PROCEDURE process_user_in_region_audit();