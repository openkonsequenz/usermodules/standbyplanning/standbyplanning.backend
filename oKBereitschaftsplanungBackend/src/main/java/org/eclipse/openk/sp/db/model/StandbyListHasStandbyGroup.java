/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.persistence.annotations.ConversionValue;
import org.eclipse.persistence.annotations.ObjectTypeConverter;

/**
 * The persistent class for the "STANDBYLIST_HAS_STANDBYGROUP" database table.
 */
@Entity
@Table(name = "STANDBY_LIST_HAS_STANDBY_GROUP")
@ObjectTypeConverter(name = "booleanConverter", dataType = java.lang.String.class, objectType = java.lang.Boolean.class, conversionValues = {
		@ConversionValue(dataValue = "Y", objectValue = "true"),
		@ConversionValue(dataValue = "N", objectValue = "false") })
public class StandbyListHasStandbyGroup extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "STANDBY_LIST_HAS_GROUP_ID_SEQ")
	@SequenceGenerator(name = "STANDBY_LIST_HAS_GROUP_ID_SEQ", sequenceName = "STANDBY_LIST_HAS_GROUP_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "standby_group_id", referencedColumnName = "id", nullable = false)
	private StandbyGroup standbyGroup = new StandbyGroup();

	@ManyToOne
	@JoinColumn(name = "standby_list_id", referencedColumnName = "id", nullable = false)
	private StandbyList standbyList;

	@Column(name = "position", nullable = false)
	private Integer position;

	public StandbyListHasStandbyGroup() {
		/** default constructor. */
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the standbyGroup
	 */
	public StandbyGroup getStandbyGroup() {
		return standbyGroup;
	}

	/**
	 * @param standbyGroup
	 *            the standbyGroup to set
	 */
	public void setStandbyGroup(StandbyGroup standbyGroup) {
		this.standbyGroup = standbyGroup;
	}

	/**
	 * @return the standbyList
	 */
	public StandbyList getStandbyList() {
		return standbyList;
	}

	/**
	 * @param standbyList
	 *            the standbyList to set
	 */
	public void setStandbyList(StandbyList standbyList) {
		this.standbyList = standbyList;
	}

	/**
	 * @return the position
	 */
	public Integer getPosition() {
		return position;
	}

	/**
	 * @param position
	 *            the position to set
	 */
	public void setPosition(Integer position) {
		this.position = position;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}

}
