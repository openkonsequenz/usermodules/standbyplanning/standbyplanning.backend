/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.msg;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.db.model.StandbyStatus;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ValidationMsgControllerTest {
	protected static final Logger LOGGER = Logger.getLogger(ValidationMsgControllerTest.class);

	@Test
	public void createMsgStartValidationTest() {

		Date startDate = new Date();
		Date endDate = new Date();
		StandbyGroup group = new StandbyGroup();
		group.setTitle("title");

		List<StandbyGroup> lsStandbyGroups = new ArrayList<>();
		lsStandbyGroups.add(group);
		lsStandbyGroups.add(group);

		List<PlanningMsgDto> listDto = ValidationMsgController.createMsgStartValidation(startDate, endDate,
				lsStandbyGroups);

		assertNotNull(listDto);

	}

	@Test
	public void createMsgEndValidationTest() {

		List<PlanningMsgDto> lsInputMsgDtos = new ArrayList<>();

		List<PlanningMsgDto> listDto = ValidationMsgController.createMsgEndValidation(lsInputMsgDtos);

		assertNotNull(listDto);

		PlanningMsgDto msgDto = new PlanningMsgDto("msg", PlanningMsgDto.WARN, PlanningMsgDto.CSS_WARN);
		lsInputMsgDtos.add(msgDto);
		PlanningMsgDto msgDto2 = new PlanningMsgDto("msgs", PlanningMsgDto.CSS_DANGER, PlanningMsgDto.CSS_DANGER);
		lsInputMsgDtos.add(msgDto2);

		listDto = ValidationMsgController.createMsgEndValidation(lsInputMsgDtos);

		assertNotNull(listDto);

	}

	@Test
	public void createMsgMissingTimeSlotTest() {

		Date startDate = new Date();
		Date endDate = new Date();
		StandbyGroup group = new StandbyGroup();
		group.setTitle("title");

		List<PlanningMsgDto> listDto = ValidationMsgController.createMsgMissingTimeSlot(startDate, endDate, group);

		assertNotNull(listDto);

	}

	@Test
	public void createMsgInvalidTimeForUserTest() {
		User usr = new User();
		usr.setFirstname("j");
		usr.setLastname("unit");
		List<PlanningMsgDto> listDto = ValidationMsgController.createMsgInvalidTimeForUser(usr);
		assertNotNull(listDto);
	}

	@Test
	public void createMsgInvalidGroupForUserTest() {
		User usr = new User();
		usr.setFirstname("j");
		usr.setLastname("unit");

		StandbyGroup group = new StandbyGroup();
		group.setTitle("title");

		List<PlanningMsgDto> listDto = ValidationMsgController.createMsgInvalidGroupForUser(usr, group);
		assertNotNull(listDto);
	}

	@Test
	public void createMsgInvalidGroupForUserTest2() {

		// user
		User usr = new User();
		usr.setFirstname("j");
		usr.setLastname("unit");

		// status
		StandbyStatus status = new StandbyStatus();
		status.setId(1L);
		status.setTitle("StatusTitle");

		// group
		StandbyGroup group = new StandbyGroup();
		group.setTitle("title");

		// body
		StandbyScheduleBody body = new StandbyScheduleBody();
		body.setUser(usr);
		body.setStandbyGroup(group);

		// alternative body
		StandbyScheduleBody alternativeBody = new StandbyScheduleBody();
		alternativeBody.setStandbyGroup(group);
		alternativeBody.setStatus(status);

		List<PlanningMsgDto> listDto = ValidationMsgController.createMsgInvalidGroupForUser(body, alternativeBody);
		assertNotNull(listDto);
	}

	@Test
	public void createMsgPlaceholderFoundTest() {

		// user
		User usr = new User();
		usr.setFirstname("j");
		usr.setLastname("unit");

		// status
		StandbyStatus status = new StandbyStatus();
		status.setId(1L);
		status.setTitle("StatusTitle");

		// group
		StandbyGroup group = new StandbyGroup();
		group.setTitle("title");

		// body
		StandbyScheduleBody body = new StandbyScheduleBody();
		body.setUser(usr);
		body.setStandbyGroup(group);
		body.setStatus(status);

		List<PlanningMsgDto> listDto = ValidationMsgController.createMsgPlaceholderFound(body);
		assertNotNull(listDto);
	}

	@Test
	public void createMsgUserWasPlannedTest() {
		// user
		User usr = new User();
		usr.setFirstname("j");
		usr.setLastname("unit");

		// status
		StandbyStatus status = new StandbyStatus();
		status.setId(1L);
		status.setTitle("StatusTitle");

		// group
		StandbyGroup group = new StandbyGroup();
		group.setTitle("title");

		// body
		StandbyScheduleBody body = new StandbyScheduleBody();
		body.setUser(usr);
		body.setStandbyGroup(group);
		body.setStatus(status);

		List<PlanningMsgDto> listDto = ValidationMsgController.createMsgUserWasPlanned(body);
		assertNotNull(listDto);
	}

}
