/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.map.HashedMap;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.controller.external.ExternalInterfaceMapper;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.dao.StandbyStatusRepository;
import org.eclipse.openk.sp.db.model.Branch;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.eclipse.openk.sp.db.model.StandbyListHasStandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.dto.BranchDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySearchDto;
import org.eclipse.openk.sp.dto.TimeDto;
import org.eclipse.openk.sp.dto.UserInStandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleSearchDto;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.eclipse.openk.sp.util.GsonTypeHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class SearchControllerTest {

	@Mock
	private StandbyScheduleBodyRepository standbyScheduleBodyRepository;

	@Mock
	private EntityConverter entityConverter;

	@Mock
	private StandbyStatusRepository standbyStatusRepository;

	@Mock
	private StandbyGroupController standbyGroupController;

	@Mock
	private StandbyListRepository standbyListRepository;

	@Mock
	private ExternalInterfaceMapper externalInterfaces;

	@InjectMocks
	private SearchController searchController;

	@Test
	public void searchNow() {

		StandbyScheduleSearchDto standbyScheduleSearchDto = new StandbyScheduleSearchDto();
		String username = "test";

		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyScheduleBody.json");
		StandbyScheduleBody body1 = (StandbyScheduleBody) new Gson().fromJson(json, StandbyScheduleBody.class);
		StandbyScheduleBody body2 = (StandbyScheduleBody) new Gson().fromJson(json, StandbyScheduleBody.class);

		json = fh.loadStringFromResource("testStandbyScheduleBodySearch.json");
		StandbyScheduleBodySearchDto standbyScheduleBodySearchDto = (StandbyScheduleBodySearchDto) new Gson()
				.fromJson(json, StandbyScheduleBodySearchDto.class);

		List<StandbyScheduleBody> listBodies = new ArrayList<>();
		listBodies.add(body1);
		listBodies.add(body2);

		List<StandbyScheduleBodySearchDto> listBodyDto = new ArrayList<>();
		List<UserInStandbyGroupSelectionDto> lsUserInSBG = new ArrayList<>();

		Mockito.when(standbyScheduleBodyRepository.findByDateAndStatus(Mockito.any(), Mockito.any(), Mockito.anyLong()))
				.thenReturn(listBodies);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(listBodyDto);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) standbyScheduleBodySearchDto);

		Mockito.when(standbyGroupController.getUserInGroupList(Mockito.anyLong())).thenReturn(lsUserInSBG);

		List<StandbyScheduleBodySearchDto> result = searchController.searchNow(standbyScheduleSearchDto, null);
		assertNotNull(result);

		standbyScheduleSearchDto.setDateIndex(null);
		result = searchController.searchNow(standbyScheduleSearchDto, null);
		assertNotNull(result);

		standbyScheduleSearchDto.setDateIndex(new Date());
		result = searchController.searchNow(standbyScheduleSearchDto, null);
		assertNotNull(result);

		TimeDto timeIndex = new TimeDto();
		timeIndex.setHour(17);
		timeIndex.setMinute(34);
		timeIndex.setSecond(27);
		standbyScheduleSearchDto.setTimeIndex(timeIndex);
		result = searchController.searchNow(standbyScheduleSearchDto, null);
		assertNotNull(result);

	}

	@Test
	public void searchNow2() {

		StandbyScheduleSearchDto standbyScheduleSearchDto = new StandbyScheduleSearchDto();
		standbyScheduleSearchDto.setLocationId(new Long(2));
		String username = "test";

		List<StandbyScheduleBody> listBodies = new ArrayList<>();
		List<StandbyScheduleBodySearchDto> listBodyDto = new ArrayList<>();

		Mockito.when(standbyScheduleBodyRepository.findByDateAndStatus(Mockito.any(), Mockito.any(), Mockito.anyLong()))
				.thenReturn(listBodies);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(listBodyDto);

		List<StandbyScheduleBodySearchDto> result = searchController.searchNow(standbyScheduleSearchDto, null);
		assertNotNull(result);

	}

	@Test
	public void filterBodiesByListTest() {
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyScheduleBodySearchList.json");
		List<StandbyScheduleBody> lsStandbyScheduleBody = (List<StandbyScheduleBody>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_STANDBYBODY_ARRAY_LIST);

		json = fh.loadStringFromResource("testStandbyList.json");
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);
		standbyList.setId(1l);

		json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup sbg = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);
		sbg.setTitle("TestGruppe");
		List<StandbyListHasStandbyGroup> lsStandbyListHasStandbyGroup = new ArrayList<>();

		StandbyListHasStandbyGroup slhsg = new StandbyListHasStandbyGroup();
		slhsg.setId(1L);
		slhsg.setPosition(1);
		slhsg.setStandbyGroup(sbg);
		slhsg.setStandbyList(standbyList);

		lsStandbyListHasStandbyGroup.add(slhsg);
		standbyList.setLsStandbyListHasStandbyGroup(lsStandbyListHasStandbyGroup);

		StandbyScheduleSearchDto standbyScheduleSearchDto = new StandbyScheduleSearchDto();
		standbyScheduleSearchDto.setListId(1L);

		List<StandbyScheduleBodySearchDto> listBodyDto = new ArrayList<>();

		Mockito.when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(standbyList);

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(listBodyDto);

		List<StandbyScheduleBody> result = searchController.filterBodiesByList(standbyScheduleSearchDto,
				lsStandbyScheduleBody);
		assertNotNull(result);
		assertEquals(0, result.size());

		standbyScheduleSearchDto.setListId(null);
		Mockito.when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(true);
		result = searchController.filterBodiesByList(standbyScheduleSearchDto, lsStandbyScheduleBody);
		assertNotNull(result);
		assertEquals(4, result.size());

		standbyScheduleSearchDto.setListId(null);
		Mockito.when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(false);
		result = searchController.filterBodiesByList(standbyScheduleSearchDto, lsStandbyScheduleBody);
		assertNotNull(result);
		assertEquals(4, result.size());

		standbyScheduleSearchDto.setListId(1L);
		Mockito.when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(false);
		result = searchController.filterBodiesByList(standbyScheduleSearchDto, lsStandbyScheduleBody);
		assertNotNull(result);
		assertEquals(4, result.size());

		Mockito.when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(true);
		sbg.setTitle("group2");
		result = searchController.filterBodiesByList(standbyScheduleSearchDto, lsStandbyScheduleBody);
		assertNotNull(result);
		assertEquals(1, result.size());

	}

	@Test
	public void matchEmptySearchTest() {

		StandbyScheduleSearchDto standbyScheduleSearchDto = new StandbyScheduleSearchDto();

		standbyScheduleSearchDto.setCity("");
		boolean result = searchController.matchEmptySearch(standbyScheduleSearchDto);
		assertTrue(result);

		standbyScheduleSearchDto.setCity(null);
		result = searchController.matchEmptySearch(standbyScheduleSearchDto);
		assertTrue(result);

		standbyScheduleSearchDto.setCity("Hallo");
		result = searchController.matchEmptySearch(standbyScheduleSearchDto);
		assertFalse(result);
	}

	@Test
	public void filterBodiesByLocationTest() {

		StandbyScheduleSearchDto standbyScheduleSearchDto = new StandbyScheduleSearchDto();

		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyScheduleBody.json");
		StandbyScheduleBody body1 = (StandbyScheduleBody) new Gson().fromJson(json, StandbyScheduleBody.class);
		StandbyScheduleBody body2 = (StandbyScheduleBody) new Gson().fromJson(json, StandbyScheduleBody.class);

		List<StandbyScheduleBody> lsMock = new ArrayList<>();
		lsMock.add(body1);
		lsMock.add(body2);

		standbyScheduleSearchDto.setLocationId(new Long(2));

		List<StandbyScheduleBody> result = searchController.filterBodiesByLocation(standbyScheduleSearchDto, lsMock);
		assertNotNull(result);

		standbyScheduleSearchDto.setLocationId(new Long(25));
		result = searchController.filterBodiesByLocation(standbyScheduleSearchDto, lsMock);
		assertNotNull(result);

	}

	@Test
	public void filterBodiesByBranches() {

		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyScheduleBody.json");
		StandbyScheduleBody body1 = (StandbyScheduleBody) new Gson().fromJson(json, StandbyScheduleBody.class);
		StandbyScheduleBody body2 = (StandbyScheduleBody) new Gson().fromJson(json, StandbyScheduleBody.class);
		Branch branch = body2.getStandbyGroup().getLsBranches().get(0);
		branch.setId(3L);

		StandbyScheduleSearchDto standbyScheduleSearchDto = new StandbyScheduleSearchDto();
		List<StandbyScheduleBody> listBodies = new ArrayList<>();
		listBodies.add(body1);
		listBodies.add(body2);

		List<BranchDto> lsBranch = new ArrayList<>();
		standbyScheduleSearchDto.setLsBranchSelected(lsBranch);
		BranchDto bDto = new BranchDto();
		bDto.setId(1L);
		lsBranch.add(bDto);

		List<StandbyScheduleBody> result = searchController.filterBodiesByBranches(standbyScheduleSearchDto,
				listBodies);
		assertNotNull(result);

		standbyScheduleSearchDto.setLsBranchSelected(new ArrayList<>());
		result = searchController.filterBodiesByBranches(standbyScheduleSearchDto, listBodies);
		assertNotNull(result);

		standbyScheduleSearchDto.setLsBranchSelected(null);
		result = searchController.filterBodiesByBranches(standbyScheduleSearchDto, listBodies);
		assertNotNull(result);

	}

	@Test
	public void hasBodyThisBranchTest() {

		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyScheduleBody.json");
		StandbyScheduleBody body1 = (StandbyScheduleBody) new Gson().fromJson(json, StandbyScheduleBody.class);

		Branch branch = body1.getStandbyGroup().getLsBranches().get(0);
		branch.setId(3L);

		boolean result = searchController.hasBodyThisBranch(body1, 1L);
		assertNotNull(result);

		result = searchController.hasBodyThisBranch(body1, 3L);
		assertNotNull(result);

		body1.getStandbyGroup().setLsBranches(new ArrayList<>());
		result = searchController.hasBodyThisBranch(body1, 3L);
		assertNotNull(result);

		body1.getStandbyGroup().setLsBranches(null);
		result = searchController.hasBodyThisBranch(body1, 3L);
		assertNotNull(result);

	}

	@Test
	public void calcBranchHit() {
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyScheduleBody.json");
		StandbyScheduleBody body1 = (StandbyScheduleBody) new Gson().fromJson(json, StandbyScheduleBody.class);

		List<StandbyScheduleBodySearchDto> result = null;
		Map<Long, List<StandbyScheduleBodySearchDto>> mapGroupBranch = new HashedMap();
		List<Long> searchBranchIdList = new ArrayList<>();

		List<StandbyScheduleBodySearchDto> listBodyDto1 = new ArrayList<>();
		List<StandbyScheduleBodySearchDto> listBodyDto2 = new ArrayList<>();

		result = searchController.calcBranchHit(mapGroupBranch, searchBranchIdList, body1, listBodyDto1, listBodyDto2);
		assertNotNull(result);
		assertEquals(listBodyDto1, result);

		searchBranchIdList.add(1L);
		mapGroupBranch.put(1L, listBodyDto1);
		result = searchController.calcBranchHit(mapGroupBranch, searchBranchIdList, body1, listBodyDto1, listBodyDto2);
		assertNotNull(result);
		assertEquals(listBodyDto1, result);

		body1.getStandbyGroup().setId(2L);
		result = searchController.calcBranchHit(mapGroupBranch, searchBranchIdList, body1, listBodyDto1, listBodyDto2);
		assertNotNull(result);
		assertEquals(listBodyDto2, result);

		body1.getStandbyGroup().setId(2L);
		searchBranchIdList.add(4L);
		result = searchController.calcBranchHit(mapGroupBranch, searchBranchIdList, body1, listBodyDto1, listBodyDto2);
		assertNotNull(result);
		assertEquals(listBodyDto2, result);

		body1.getStandbyGroup().setId(3L);
		body1.getStandbyGroup().getLsBranches().add(new Branch(2L));
		searchBranchIdList.add(5L);
		result = searchController.calcBranchHit(mapGroupBranch, searchBranchIdList, body1, listBodyDto1, listBodyDto2);
		assertNotNull(result);
		assertEquals(listBodyDto1, result);

	}

	@Test
	public void sortBodiesTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyScheduleBodySearchList.json");

		List<StandbyScheduleBody> lsStandbyScheduleBody = (List<StandbyScheduleBody>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_STANDBYBODY_ARRAY_LIST);

		json = fh.loadStringFromResource("testStandbyScheduleBodyDto.json");
		StandbyScheduleBodySearchDto bodyDto = (StandbyScheduleBodySearchDto) new Gson().fromJson(json,
				StandbyScheduleBodySearchDto.class);

		StandbyScheduleSearchDto standbyScheduleSearchDto = new StandbyScheduleSearchDto();

		List<StandbyScheduleBodySearchDto> result = null;

		Mockito.when(entityConverter.convertEntityToDto(Mockito.any(), (StandbyScheduleBodySearchDto) Mockito.any()))
				.thenReturn(bodyDto);

		result = searchController.sortBodies(standbyScheduleSearchDto, lsStandbyScheduleBody);
		assertNotNull(result);

		BranchDto bDto = new BranchDto();
		bDto.setId(1L);
		standbyScheduleSearchDto.setLsBranchSelected(new ArrayList<>());
		standbyScheduleSearchDto.getLsBranchSelected().add(bDto);
		result = searchController.sortBodies(standbyScheduleSearchDto, lsStandbyScheduleBody);
		assertNotNull(result);

//
//		body1.getStandbyGroup().setId(2L);
//		result = searchController.sortBodies(mapGroupBranch, searchBranchIdList, body1, listBodyDto1, listBodyDto2);
//		assertNotNull(result);
//		assertEquals(listBodyDto2, result);
//
//		body1.getStandbyGroup().setId(2L);
//		searchBranchIdList.add(4L);
//		result = searchController.sortBodies(mapGroupBranch, searchBranchIdList, body1, listBodyDto1, listBodyDto2);
//		assertNotNull(result);
//		assertEquals(listBodyDto2, result);
//
//		body1.getStandbyGroup().setId(3L);
//		body1.getStandbyGroup().getLsBranches().add(new Branch(2L));
//		searchBranchIdList.add(5L);
//		result = searchController.sortBodies(mapGroupBranch, searchBranchIdList, body1, listBodyDto1, listBodyDto2);
//		assertNotNull(result);
//		assertEquals(listBodyDto1, result);

	}
}
