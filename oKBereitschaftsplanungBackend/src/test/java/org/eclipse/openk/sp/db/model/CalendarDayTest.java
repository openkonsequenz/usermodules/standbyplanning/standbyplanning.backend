/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;

import org.junit.Test;

public class CalendarDayTest {

	@Test
	public void testGettersAndSetters() {

		Date d = new Date();
		Long l = new Long(3);
		String t = "Tag";
		Boolean b = new Boolean(true);

		CalendarDay cd = new CalendarDay();
		cd.setId(l);
		assertEquals(l, cd.getId());

		cd = new CalendarDay(l);
		assertEquals(l, cd.getId());

		cd.setName(t);
		assertEquals(t, cd.getName());

		cd.setDateIndex(d);
		assertEquals(d, cd.getDateIndex());

		cd.setModificationDate(d);
		assertEquals(d, cd.getModificationDate());

		cd.setRepeat(b);
		assertEquals(b, cd.getRepeat());

		cd.setShorttext(t);
		assertEquals(t, cd.getShorttext());

		cd.setValidFrom(d);
		assertEquals(d, cd.getValidFrom());

		cd.setValidTo(d);
		assertEquals(d, cd.getValidTo());

		cd.setLsStandbyGroups(new ArrayList<StandbyGroup>());
		assertNotNull(cd.getLsStandbyGroups());

	}

	@Test
	public void testCompare() throws MalformedURLException {
		CalendarDay obj1 = new CalendarDay();
		obj1.setId(5L);

		CalendarDay obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		CalendarDay obj3 = new CalendarDay();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}
}
