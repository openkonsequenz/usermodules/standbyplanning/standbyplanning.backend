/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;

import org.junit.Test;

public class StandbyStatusTest {

	@Test
	public void testGettersAndSetters() {
		StandbyStatus status = new StandbyStatus();
		status.setId(1l);
		assertEquals(1l, status.getId().longValue());

		status.setTitle("Title");
		assertEquals("Title", status.getTitle());
	}
	
	
	@Test
	public void testCompare() throws MalformedURLException {
		StandbyStatus obj1 = new StandbyStatus();
		obj1.setId(5L);

		StandbyStatus obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		StandbyStatus obj3 = new StandbyStatus();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}

}
