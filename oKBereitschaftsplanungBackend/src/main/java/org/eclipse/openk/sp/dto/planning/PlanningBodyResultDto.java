/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.db.model.StandbyDuration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "Address" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "PlanningBodyResultDto")
@JsonInclude(Include.NON_NULL)
public class PlanningBodyResultDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Integer newPosition = 0;

	private Long lastStartUserId = 0L;

	private Long lastUserId = 0L;

	private Date tempDate = null;

	private List<PlanningMsgDto> lsMsg = new ArrayList<>();

	private StandbyDuration currentDuration;

	/**
	 * @return the newPosition
	 */
	public Integer getNewPosition() {
		return newPosition;
	}

	/**
	 * @param newPosition
	 *            the newPosition to set
	 */
	public void setNewPosition(Integer newPosition) {
		this.newPosition = newPosition;
	}

	/**
	 * @return the tempDate
	 */
	public Date getTempDate() {
		return tempDate;
	}

	/**
	 * @param tempDate
	 *            the tempDate to set
	 */
	public void setTempDate(Date tempDate) {
		this.tempDate = tempDate;
	}

	/**
	 * @return the lsMsg
	 */
	public List<PlanningMsgDto> getLsMsg() {
		return lsMsg;
	}

	/**
	 * @param lsMsg
	 *            the lsMsg to set
	 */
	public void setLsMsg(List<PlanningMsgDto> lsMsg) {
		this.lsMsg = lsMsg;
	}

	/**
	 * @return the lastStartUserId
	 */
	public Long getLastStartUserId() {
		return lastStartUserId;
	}

	/**
	 * @param lastStartUserId
	 *            the lastStartUserId to set
	 */
	public void setLastStartUserId(Long lastStartUserId) {
		this.lastStartUserId = lastStartUserId;
	}

	/**
	 * @return the currentDuration
	 */
	public StandbyDuration getCurrentDuration() {
		return currentDuration;
	}

	/**
	 * @param currentDuration
	 *            the currentDuration to set
	 */
	public void setCurrentDuration(StandbyDuration currentDuration) {
		this.currentDuration = currentDuration;
	}

	/**
	 * @return the lastUserId
	 */
	public Long getLastUserId() {
		return lastUserId;
	}

	/**
	 * @param lastUserId
	 *            the lastUserId to set
	 */
	public void setLastUserId(Long lastUserId) {
		this.lastUserId = lastUserId;
	}
}