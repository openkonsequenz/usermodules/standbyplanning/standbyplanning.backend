/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@XmlRootElement(name = "LabelValueDto")
@JsonInclude(Include.NON_NULL)
public class CopyDto extends AbstractDto {

	/** default serial id. */
	private static final long serialVersionUID = 1L;

	private boolean copyRegion = false;
	private boolean copyFunction = false;
	private boolean copyUser = true;
	private boolean copyBranch = false;
	private boolean copyCalendar = false;
	private boolean copyDuration = true;
	private boolean userPlusOneYear = false;

	/**
	 * @return the copyRegion
	 */
	public boolean isCopyRegion() {
		return copyRegion;
	}

	/**
	 * @param copyRegion the copyRegion to set
	 */
	public void setCopyRegion(boolean copyRegion) {
		this.copyRegion = copyRegion;
	}

	/**
	 * @return the copyFunction
	 */
	public boolean isCopyFunction() {
		return copyFunction;
	}

	/**
	 * @param copyFunction the copyFunction to set
	 */
	public void setCopyFunction(boolean copyFunction) {
		this.copyFunction = copyFunction;
	}

	/**
	 * @return the copyUser
	 */
	public boolean isCopyUser() {
		return copyUser;
	}

	/**
	 * @param copyUser the copyUser to set
	 */
	public void setCopyUser(boolean copyUser) {
		this.copyUser = copyUser;
	}

	/**
	 * @return the copyBranch
	 */
	public boolean isCopyBranch() {
		return copyBranch;
	}

	/**
	 * @param copyBranch the copyBranch to set
	 */
	public void setCopyBranch(boolean copyBranch) {
		this.copyBranch = copyBranch;
	}

	/**
	 * @return the copyCalendar
	 */
	public boolean isCopyCalendar() {
		return copyCalendar;
	}

	/**
	 * @param copyCalendar the copyCalendar to set
	 */
	public void setCopyCalendar(boolean copyCalendar) {
		this.copyCalendar = copyCalendar;
	}

	/**
	 * @return the copyDuration
	 */
	public boolean isCopyDuration() {
		return copyDuration;
	}

	/**
	 * @param copyDuration the copyDuration to set
	 */
	public void setCopyDuration(boolean copyDuration) {
		this.copyDuration = copyDuration;
	}

	public boolean isUserPlusOneYear() {
		return userPlusOneYear;
	}

	public void setUserPlusOneYear(boolean userPlusOneYear) {
		this.userPlusOneYear = userPlusOneYear;
	}

}
