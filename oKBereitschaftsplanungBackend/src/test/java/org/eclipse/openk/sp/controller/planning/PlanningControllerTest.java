/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.planning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.UserController;
import org.eclipse.openk.sp.controller.validation.ValidationController;
import org.eclipse.openk.sp.db.dao.CalendarRepository;
import org.eclipse.openk.sp.db.dao.StandbyDurationRepository;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.dao.StandbyStatusRepository;
import org.eclipse.openk.sp.db.dao.UserInStandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.UserRepository;
import org.eclipse.openk.sp.db.model.CalendarDay;
import org.eclipse.openk.sp.db.model.StandbyDuration;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.db.model.StandbyStatus;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.db.model.UserInStandbyGroup;
import org.eclipse.openk.sp.dto.StandbyScheduleBlueprintDto;
import org.eclipse.openk.sp.dto.planning.PlanningBodyResultDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.dto.planning.PlanningPhaseDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleActionDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.mail.MailRequest;
import org.eclipse.openk.sp.util.DateHelper;
import org.eclipse.openk.sp.util.SpMsg;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PlanningControllerTest extends PlanningController {
	protected static final Logger LOGGER = Logger.getLogger(PlanningControllerTest.class);

	private static final String ERROR_MSG_MISSING_ENTITY = "Eines der folgenden Entities kann nicht gefunden werden:";
	private static final String ERROR_MSG_STANDBYLIST = "Es fehlt der folgendende Einagebeparameter : StandbyList";
	private static final String ERROR_MSG_STANDBYGROUP = "Es fehlt der folgendende Einagebeparameter : StandbyGroup";
	private static final String ERROR_MSG_VALID_FROM = "Es fehlt der folgendende Einagebeparameter : validFrom";
	private static final String ERROR_MSG_VALID_TO = "Es fehlt der folgendende Einagebeparameter : validTo";

	@InjectMocks
	@Spy
	PlanningController planningController;

	@Mock
	private StandbyGroupRepository standbyGroupRepository;

	@Mock
	private StandbyListRepository standbyListRepository;

	@Mock
	private StandbyDurationRepository standbyDurationRepository;

	@Mock
	private StandbyStatusRepository standbyStatusRepository;

	@Mock
	private StandbyScheduleBodyRepository standbyScheduleBodyRepository;

	@Mock
	private UserRepository userRepository;

	@Mock
	private CalendarRepository calendarRepository;

	@Mock
	private UserController userController;

	@Mock
	private ValidationController validationController;

	@Mock
	private ArchiveController archiveController;

	@Mock
	private MailRequest mailRequest;

	@Mock
	private UserInStandbyGroupRepository uisgRepository;

	@Test
	public void calculatePlanTest() throws SpException {
		assertNotNull(new Boolean(true));
	}

	@Test
	public void startPlanningTest() throws SpException {
		String username = "JUnit";
		StandbyScheduleBlueprintDto standbyBlueprintDto = new StandbyScheduleBlueprintDto();
		standbyBlueprintDto.setStandbyGroupId(1L);
		standbyBlueprintDto.setStandbyListId(1L);
		standbyBlueprintDto.setStartIdOfUser(1L);
		standbyBlueprintDto.setValidFrom(DateHelper.getDate(2018, 1, 1, 8, 0, 0));
		standbyBlueprintDto.setValidTo(DateHelper.getDate(2018, 1, 1, 16, 0, 0));

		StandbyGroup stbyGroup = new StandbyGroup();
		stbyGroup.setId(1l);

		List<UserInStandbyGroup> lsUserInStandbyGroup = new ArrayList<>();
		User user1 = new User();
		user1.setFirstname("J.");
		user1.setLastname("Unit");
		UserInStandbyGroup uisg = new UserInStandbyGroup();
		uisg.setUser(user1);

		Map<Integer, PlanningPhaseDto> planningPhaseMap = new HashMap<>();
		PlanningPhaseDto planningPhaseDto = new PlanningPhaseDto();
		planningPhaseDto.setStartDate(DateHelper.getDate(2018, 1, 1, 0, 0, 0));
		planningPhaseDto.setEndDate(DateHelper.getDate(2018, 1, 31, 0, 0, 0));
		planningPhaseDto.setLsUsers(lsUserInStandbyGroup);
		planningPhaseMap.put(1, planningPhaseDto);

		try {
			// success test 01 - validInput
			Mockito.doReturn(stbyGroup).when(standbyGroupRepository).findOne(Mockito.any());
			Mockito.doReturn(true).when(planningController).validateInputForPlanCalculation(standbyBlueprintDto);
			Mockito.doReturn(planningPhaseMap).when(planningController).calculatePlanningPhaseMap(standbyBlueprintDto);
			Mockito.doReturn(new PlanningBodyResultDto()).when(planningController).calculatePlanForPhase(Mockito.any(),
					Mockito.any(), Mockito.any(), Mockito.anyInt(), Mockito.any());
			planningController.startPlanning(standbyBlueprintDto, username);

			// success test 02 - invalidInput
			Mockito.doReturn(false).when(planningController).validateInputForPlanCalculation(standbyBlueprintDto);
			Mockito.doReturn(planningPhaseMap).when(planningController).calculatePlanningPhaseMap(standbyBlueprintDto);
			Mockito.doReturn(new PlanningBodyResultDto()).when(planningController).calculatePlanForPhase(Mockito.any(),
					Mockito.any(), Mockito.any(), Mockito.anyInt(), Mockito.any());
			planningController.startPlanning(standbyBlueprintDto, username);
		} catch (SpException e) {
			assertNull(e);
		}

		// negative test 01
		try {
			Mockito.doThrow(new SpException()).when(planningController)
					.validateInputForPlanCalculation(standbyBlueprintDto);
			planningController.startPlanning(standbyBlueprintDto, username);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// negative test 02
		try {
			Mockito.doThrow(new IllegalArgumentException()).when(standbyGroupRepository).findOne(Mockito.any());
			planningController.startPlanning(standbyBlueprintDto, username);
		} catch (SpException e) {
			assertNotNull(e);
		}
	}

	@Test
	public void calculatePlanForPhaseTest() {
		PlanningPhaseDto dto = new PlanningPhaseDto();
		List<UserInStandbyGroup> lsUserInStandbyGroup = this.getTestDataForUserInStandbyGroups();
		dto.setLsUsers(lsUserInStandbyGroup);

		dto.setStartDate(new Date());
		dto.setEndDate(DateHelper.addDaysToDate(new Date(), 7));
		Long lastStartUserId = 1L;
		String username = "JUnit";

		when(standbyScheduleBodyRepository.save(Mockito.any(StandbyScheduleBody.class)))
				.thenReturn(new StandbyScheduleBody());

		when(userRepository.findOne(Mockito.any())).thenReturn(lsUserInStandbyGroup.get(0).getUser());

		Mockito.doNothing().when(planningController).changeOrDeleteExistingBodies(Mockito.any(), Mockito.any(),
				Mockito.any(), Mockito.any(), Mockito.any());

		try {
			planningController.calculatePlanForPhase(dto, lastStartUserId, username, 1, dto.getEndDate());
		} catch (Exception e) {
			LOGGER.error(e, e);
			assertNull(e);
		}
	}

	@Ignore
	@Test
	public void calculatePlanningPhaseMapTest() {
		Map<Integer, PlanningPhaseDto> planningPhaseMap = new HashMap<>();
		StandbyScheduleBlueprintDto standbyBlueprintDto = new StandbyScheduleBlueprintDto();
		standbyBlueprintDto.setStandbyGroupId(1L);
		standbyBlueprintDto.setStandbyListId(1L);
		standbyBlueprintDto.setStartIdOfUser(1L);
		standbyBlueprintDto.setValidFrom(DateHelper.getDate(2018, 1, 1, 8, 0, 0));
		standbyBlueprintDto.setValidTo(DateHelper.getDate(2018, 1, 9, 16, 0, 0));

		List<UserInStandbyGroup> lsUserInStandbyGroupsEmpty = new ArrayList<>();
		List<UserInStandbyGroup> lsUserInStandbyGroups = new ArrayList<>();
		UserInStandbyGroup uisg = new UserInStandbyGroup();
		User user = new User();
		uisg.setUser(user);
		lsUserInStandbyGroups.add(uisg);
		user = new User();
		uisg.setUser(user);
		lsUserInStandbyGroups.add(uisg);

		try {
			// [SUCCESS] returning null list for lsUserInGroup
			when(uisgRepository.findUserForInterval(Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(null);
			planningPhaseMap = planningController.calculatePlanningPhaseMap(standbyBlueprintDto);
			assertNotNull(planningPhaseMap);
			assertTrue(planningPhaseMap.isEmpty());

			// [SUCCESS] returning empty list for lsUserInGroup
			when(uisgRepository.findUserForInterval(Mockito.any(), Mockito.any(), Mockito.any()))
					.thenReturn(lsUserInStandbyGroupsEmpty);
			planningPhaseMap = planningController.calculatePlanningPhaseMap(standbyBlueprintDto);
			assertNotNull(planningPhaseMap);
			assertTrue(planningPhaseMap.isEmpty());

			// [SUCCESS] returning list for lsUserInGroup
			when(uisgRepository.findUserForInterval(Mockito.any(), Mockito.any(), Mockito.any()))
					.thenReturn(lsUserInStandbyGroups);
			planningPhaseMap = planningController.calculatePlanningPhaseMap(standbyBlueprintDto);
			assertNotNull(planningPhaseMap);
			assertFalse(planningPhaseMap.isEmpty());

		} catch (Exception e) {
			LOGGER.error(e, e);
			assertNull(e);
		}
	}

	@Test
	public void createScheduleBodyEntryTest() {
		Date startDate = new Date();
		Date currentDate = new Date();
		Date lastDate = new Date();
		String username = "JUnit";
		Long lastUserId = 1L;
		List<UserInStandbyGroup> lsUserInStandbyGroup = this.getTestDataForUserInStandbyGroups();

		List<StandbyDuration> lsDuration = new ArrayList<>();
		StandbyDuration dur = new StandbyDuration();
		dur.setId(1L);
		dur.setNextUserInNextDuration(true);
		dur.setValidDayFrom(1);
		dur.setValidDayTo(1);
		dur.setValidFrom(DateHelper.getDate(2018, 10, 1));
		dur.setValidFrom(DateHelper.getDate(2018, 10, 31));
		lsDuration.add(dur);

		when(standbyDurationRepository.findDurationForValidFromDay(Mockito.any(), Mockito.any()))
				.thenReturn(lsDuration);
		when(standbyScheduleBodyRepository.save(Mockito.any(StandbyScheduleBody.class)))
				.thenReturn(new StandbyScheduleBody());

		// covered days = 1
		try {
			PlanningBodyResultDto dto = planningController.calculateScheduleBodyEntries(lsUserInStandbyGroup, startDate,
					currentDate, lastDate, lastUserId, username, lastUserId, 1);
			assertNotNull(dto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// covered days > 1
		lsDuration.get(0).setValidDayTo(3);
		lastDate = DateHelper.addDaysToDate(new Date(), 5);
		try {
			PlanningBodyResultDto dto = planningController.calculateScheduleBodyEntries(lsUserInStandbyGroup, startDate,
					currentDate, lastDate, lastUserId, username, lastUserId, 1);
			assertNotNull(dto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// date after current
		try {
			currentDate = DateHelper.addDaysToDate(new Date(), 1);
			lastDate = DateHelper.addDaysToDate(new Date(), 1);
			PlanningBodyResultDto dto = planningController.calculateScheduleBodyEntries(lsUserInStandbyGroup, startDate,
					currentDate, lastDate, lastUserId, username, lastUserId, 1);
			assertNotNull(dto);
		} catch (Exception e) {
			assertNotNull(e);
		}

		// fitting duration = false
		when(standbyDurationRepository.findDurationForValidFromDay(Mockito.any(), Mockito.any())).thenReturn(null);
		try {
			PlanningBodyResultDto dto = planningController.calculateScheduleBodyEntries(lsUserInStandbyGroup, startDate,
					currentDate, lastDate, lastUserId, username, lastUserId, 1);
			assertNotNull(dto);
			assertTrue(
					dto.getLsMsg().get(0).getMsg().contains("wurde kein aktiver Zeitraum für folgenden Tag gefunden"));
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	public PlanningBodyResultDto calculateScheduleBodyEntries(List<UserInStandbyGroup> lsUserInGroup, Date currentDate,
			Date lastDate, int currentPosition, String username) {
		StandbyGroup group = lsUserInGroup.get(0).getStandbyGroup();
		PlanningBodyResultDto resultObj = new PlanningBodyResultDto();
		StandbyDuration fittingDuration = this.getFittingDurationOfGroup(group, currentDate);
		if (fittingDuration != null) {
			int intStartDay = fittingDuration.getValidDayFrom();
			int intEndDay = fittingDuration.getValidDayTo();
			int coveredDays = DateHelper.calculateDifferenceOfDays(intStartDay, intEndDay, new Date(), new Date());
			Date nextDay = null;

			if (coveredDays == 0) {
				// single entry with no over night duration
				LOGGER.debug("A day was found that just cover a single day.");
				StandbyScheduleBody stbyBody = this.createScheduleBodyObject(group, username, currentDate,
						lsUserInGroup.get(currentPosition).getUser(), fittingDuration,
						PlanningController.TXT_AUTOMATIC_PLANNING);
				standbyScheduleBodyRepository.save(stbyBody);
				resultObj.getLsMsg().add(createMsgSavingBody(stbyBody));
			} else {
				// entries with over night or many days duration
				for (int i = 0; i < coveredDays; i++) {
					nextDay = DateHelper.addDaysToDate(currentDate, 1);
					if (DateHelper.isDateAfter(nextDay, lastDate)) {
						String warning = "Für das Datum (" + currentDate
								+ ") wird kein Eintrag erzeugt, da der Folgetag den definierten Zeitraum überschereitet.";
						resultObj.getLsMsg()
								.add(new PlanningMsgDto(warning, PlanningMsgDto.WARN, PlanningMsgDto.CSS_WARN));
						resultObj.setTempDate(nextDay);
						resultObj.setNewPosition(currentPosition);
						return resultObj;
					} else {
						List<StandbyScheduleBody> lsScheduleBodies = this.createOverlappingScheduleBodyObjects(group,
								username, currentDate, lsUserInGroup.get(currentPosition).getUser(), fittingDuration, i,
								coveredDays);
						for (StandbyScheduleBody stbyBody : lsScheduleBodies) {
							standbyScheduleBodyRepository.save(stbyBody);
							resultObj.getLsMsg().add(createMsgSavingBody(stbyBody));
							resultObj.setTempDate(nextDay);
						}
						resultObj.setNewPosition(currentPosition);
						resultObj.setTempDate(nextDay);
						currentDate = resultObj.getTempDate();
					}
				}
			}
			resultObj.setTempDate(nextDay);
			resultObj.setCurrentDuration(fittingDuration);
		} else {
			resultObj.setTempDate(DateHelper.addDaysToDate(currentDate, 1));
			resultObj.getLsMsg().add(this.createMsgNotAvailable(group, currentDate));
		}
		return resultObj;
	}

	@Test
	public void createMsgExtendHolidayTest() {
		StandbyScheduleBody stbyBody = new StandbyScheduleBody();
		User user = new User();
		user.setFirstname("J.");
		user.setLastname("Unit");
		stbyBody.setUser(user);
		Date date = new Date();
		Boolean isStandbyTimeExtension = true;

		// positive test - extend
		PlanningMsgDto dto = planningController.createMsgExtendHoliday(stbyBody, date, isStandbyTimeExtension);
		assertNotNull(dto);
		assertTrue(dto.getMsg().endsWith("verlängert werden."));

		// negative test - pre
		isStandbyTimeExtension = false;
		dto = planningController.createMsgExtendHoliday(stbyBody, date, isStandbyTimeExtension);
		assertNotNull(dto);
		assertTrue(dto.getMsg().endsWith("vorgezogen werden."));
	}

	@Test
	public void createMsgStartOfPhaseTest() {
		List<UserInStandbyGroup> lsUserInStandbyGroup = this.getTestDataForUserInStandbyGroups();
		Integer phaseId = 1;
		PlanningPhaseDto phase = new PlanningPhaseDto();
		phase.setEndDate(new Date());
		phase.setStartDate(new Date());
		phase.setLsUsers(lsUserInStandbyGroup);
		try {
			List<PlanningMsgDto> lsMsgDto = planningController.createMsgStartOfPhase(phaseId, phase);
			assertNotNull(lsMsgDto);
		} catch (Exception e) {
			assertNull(e);
		}
	}

	@Test
	public void createMsgEndOfPhaseTest() {
		Integer phaseId = 1;
		PlanningMsgDto msgDto = planningController.createMsgEndOfPhase(phaseId);
		assertNotNull(msgDto);
	}

	@Test
	public void createMsgSavingBodyTest() {
		StandbyScheduleBody stbyBody = new StandbyScheduleBody();
		User user = new User();
		user.setFirstname("J.");
		user.setLastname("Unit");
		stbyBody.setUser(user);
		stbyBody.setValidFrom(new Date());
		stbyBody.setValidTo(new Date());
		PlanningMsgDto msgDto = planningController.createMsgSavingBody(stbyBody);
		assertNotNull(msgDto);
	}

	@Test
	public void createMsgNotAvailableTest() {
		StandbyGroup group = new StandbyGroup();
		group.setTitle("Test");
		Date currentDate = new Date();
		PlanningMsgDto msgDto = planningController.createMsgNotAvailable(group, currentDate);
		assertNotNull(msgDto);
	}

	@Test
	public void createMsgNumberOfPhasesTest() {
		Map<Integer, PlanningPhaseDto> map = new HashMap<>();
		map.put(1, new PlanningPhaseDto());
		map.put(2, new PlanningPhaseDto());

		StandbyScheduleBlueprintDto dto = new StandbyScheduleBlueprintDto();
		dto.setValidFrom(new Date());
		dto.setValidTo(new Date());

		// success
		PlanningMsgDto msgDto = planningController.createMsgNumberOfPhases(map, dto);
		assertNotNull(msgDto);
		assertTrue(msgDto.getMsg().startsWith("Durch wechselnde Gruppenmitglieder wurde(n)"));

		// empty map
		map = new HashMap<>();
		msgDto = planningController.createMsgNumberOfPhases(map, dto);
		assertNotNull(msgDto);
		assertTrue(msgDto.getMsg().startsWith("Auf Grund fehlender Bereitschaften im gewählten Zeitraum,"));
	}

	@Test
	public void createMsgGroupLeaderChangedTest() {
		List<UserInStandbyGroup> lsUserInStandbyGroup = new ArrayList<UserInStandbyGroup>();
		UserInStandbyGroup uisg = new UserInStandbyGroup();
		User nextUser = new User();
		nextUser.setFirstname("Next");
		nextUser.setLastname("User");
		uisg.setUser(nextUser);
		lsUserInStandbyGroup.add(uisg);

		User user = new User();
		user.setFirstname("J.");
		user.setLastname("Unit");
		PlanningMsgDto msgDto = planningController.createMsgGroupLeaderChanged(user, nextUser);
		assertNotNull(msgDto);
	}

	@Test
	public void deleteScheduleBodiesByIdsTest() {

		List<Long> lsIds = new ArrayList<>();
		lsIds.add(1L);
		lsIds.add(2L);

		Mockito.doNothing().when(standbyScheduleBodyRepository).delete(1L);
		try {
			planningController.deleteScheduleBodiesByIds(lsIds);
		} catch (Exception e) {
			assertNull(e);
		}

		lsIds.add(3L);
		Mockito.doThrow(new IllegalArgumentException()).when(standbyScheduleBodyRepository).delete(3L);
		try {
			planningController.deleteScheduleBodiesByIds(lsIds);
		} catch (SpException e) {
			assertNotNull(e);
		}
	}

	@Test
	public void validateInputForPlanCalculationTest() {
		StandbyScheduleBlueprintDto standbyBlueprintDto = new StandbyScheduleBlueprintDto();
		// [FAIL] TEST INVALID LIST : EMPTY
		try {
			planningController.validateInputForPlanCalculation(standbyBlueprintDto);
		} catch (SpException e) {
			assertNotNull(e);
			assertEquals(ERROR_MSG_STANDBYLIST, e.getMessage());
		}

		// [FAIL] TEST INVALID LIST : UNKOWN ENTITY
		when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(false);
		standbyBlueprintDto.setStandbyListId(1l);
		try {
			planningController.validateInputForPlanCalculation(standbyBlueprintDto);
		} catch (SpException e) {
			assertNotNull(e);
			assertTrue(e.getMessage().startsWith(ERROR_MSG_MISSING_ENTITY));
		}

		// [FAIL] TEST INVALID GROUP : EMPTY
		when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(true);
		standbyBlueprintDto.setStandbyListId(1l);
		try {
			planningController.validateInputForPlanCalculation(standbyBlueprintDto);
		} catch (SpException e) {
			assertNotNull(e);
			assertEquals(ERROR_MSG_STANDBYGROUP, e.getMessage());
		}

		// [FAIL] TEST INVALID GROUP : UNKOWN ENTITY
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		standbyBlueprintDto.setStandbyGroupId(1l);
		try {
			planningController.validateInputForPlanCalculation(standbyBlueprintDto);
		} catch (SpException e) {
			assertNotNull(e);
			assertTrue(e.getMessage().startsWith(ERROR_MSG_MISSING_ENTITY));
		}

		// [FAIL] TEST INVALID VALID FROM : EMPTY
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		try {
			planningController.validateInputForPlanCalculation(standbyBlueprintDto);
		} catch (SpException e) {
			assertNotNull(e);
			assertEquals(ERROR_MSG_VALID_FROM, e.getMessage());
		}

		// [FAIL] TEST INVALID VALID TO : EMPTY
		standbyBlueprintDto.setValidFrom(new Date());
		try {
			planningController.validateInputForPlanCalculation(standbyBlueprintDto);
		} catch (SpException e) {
			assertNotNull(e);
			assertEquals(ERROR_MSG_VALID_TO, e.getMessage());
		}

		// [SUCCESS] TEST VALID
		standbyBlueprintDto.setValidTo(new Date());
		try {
			Boolean result = planningController.validateInputForPlanCalculation(standbyBlueprintDto);
			assertTrue(result);
		} catch (SpException e) {
			assertNull(e);
		}
	}

	@Test
	public void isHolidayTest() {
		StandbyGroup group = new StandbyGroup();

		List<CalendarDay> lsCalendarDays = null;

		// negative test - null value
		when(calendarRepository.findValidByDate(Mockito.any(), Mockito.any())).thenReturn(lsCalendarDays);
		Boolean result = planningController.isHoliday(new Date(), group);
		assertTrue(!result);

		// negative test - empty list
		lsCalendarDays = new ArrayList<>();
		when(calendarRepository.findValidByDate(Mockito.any(), Mockito.any())).thenReturn(lsCalendarDays);
		result = planningController.isHoliday(new Date(), group);
		assertTrue(!result);

		// negative test - object in Ignore List
		CalendarDay cal = new CalendarDay();
		lsCalendarDays.add(cal);
		when(calendarRepository.findValidByDate(Mockito.any(), Mockito.any())).thenReturn(lsCalendarDays);
		group.getLsIgnoredCalendarDays().add(cal);
		result = planningController.isHoliday(new Date(), group);
		assertTrue(!result);

		// positiv test
		when(calendarRepository.findValidByDate(Mockito.any(), Mockito.any())).thenReturn(lsCalendarDays);
		group.getLsIgnoredCalendarDays().remove(cal);
		result = planningController.isHoliday(new Date(), group);
		assertTrue(result);
	}

	@Test
	public void getFittingDurationOfGroupTest() {
		StandbyGroup group = new StandbyGroup();
		Date date = new Date();
		List<StandbyDuration> lsDuration = new ArrayList<>();
		StandbyDuration result = null;

		when(standbyDurationRepository.findDurationForValidFromDay(Mockito.any(), Mockito.any()))
				.thenReturn(lsDuration);

		// success
		lsDuration.add(new StandbyDuration());
		result = planningController.getFittingDurationOfGroup(group, date);
		assertNotNull(result);

		// failure
		lsDuration = new ArrayList<>();
		when(standbyDurationRepository.findDurationForValidFromDay(Mockito.any(), Mockito.any()))
				.thenReturn(lsDuration);
		result = planningController.getFittingDurationOfGroup(group, date);
		assertNull(result);

		when(standbyDurationRepository.findDurationForValidFromDay(Mockito.any(), Mockito.any())).thenReturn(null);
		result = planningController.getFittingDurationOfGroup(group, date);
		assertNull(result);

	}

	@Test
	public void getFittingDurationByValidToTest() {
		StandbyGroup group = new StandbyGroup();
		Date date = new Date();
		List<StandbyDuration> lsDuration = new ArrayList<>();
		StandbyDuration result = null;

		when(standbyDurationRepository.findDurationForValidToDay(Mockito.any(), Mockito.any())).thenReturn(lsDuration);

		// success
		lsDuration.add(new StandbyDuration());
		result = planningController.getFittingDurationByValidTo(group, date);
		assertNotNull(result);

		// failure
		lsDuration = new ArrayList<>();
		when(standbyDurationRepository.findDurationForValidToDay(Mockito.any(), Mockito.any())).thenReturn(lsDuration);
		result = planningController.getFittingDurationByValidTo(group, date);
		assertNull(result);

		when(standbyDurationRepository.findDurationForValidToDay(Mockito.any(), Mockito.any())).thenReturn(null);
		result = planningController.getFittingDurationByValidTo(group, date);
		assertNull(result);

	}

	@Test
	public void resetCounterToNewPositionTest() {
		int currentPosition = 0;
		Long lastStartUserId = 1L;
		List<UserInStandbyGroup> lsUserInGroup = new ArrayList<>();
		UserInStandbyGroup uisg = new UserInStandbyGroup();
		User user = new User();
		user.setId(1L);
		uisg.setUser(user);
		lsUserInGroup.add(uisg);

		uisg = new UserInStandbyGroup();
		user = new User();
		user.setId(2L);
		uisg.setUser(user);
		lsUserInGroup.add(uisg);

		uisg = new UserInStandbyGroup();
		user = new User();
		user.setId(3L);
		uisg.setUser(user);
		lsUserInGroup.add(uisg);

		Boolean isNextUserInNextCycle = false;
		Boolean isNextUserInDuration = true;

		PlanningBodyResultDto response = null;
		response = planningController.resetCounterToNewPosition(currentPosition, lastStartUserId, lsUserInGroup,
				isNextUserInNextCycle, isNextUserInDuration);
		currentPosition = response.getNewPosition();
		assertTrue(currentPosition == 1);

		// not start user
		response = planningController.resetCounterToNewPosition(1, 2L, lsUserInGroup, isNextUserInNextCycle,
				isNextUserInDuration);
		currentPosition = response.getNewPosition();
		assertTrue(currentPosition == 2);

		// test start from beginning because list ends
		response = planningController.resetCounterToNewPosition(2, 2L, lsUserInGroup, isNextUserInNextCycle,
				isNextUserInDuration);
		currentPosition = response.getNewPosition();
		assertTrue(currentPosition == 0);

		// user at position but isNextUserInNextCycle = false
		response = planningController.resetCounterToNewPosition(0, 2L, lsUserInGroup, isNextUserInNextCycle,
				isNextUserInDuration);
		currentPosition = response.getNewPosition();
		assertTrue(currentPosition == 1);

		// skip position because user at position and isNextUserInNextCycle = true
		isNextUserInNextCycle = true;
		response = planningController.resetCounterToNewPosition(0, 2L, lsUserInGroup, isNextUserInNextCycle,
				isNextUserInDuration);
		currentPosition = response.getNewPosition();
		assertTrue(currentPosition == 2);

		// next user in duration = false
		isNextUserInDuration = false;
		response = planningController.resetCounterToNewPosition(0, 2L, lsUserInGroup, isNextUserInNextCycle,
				isNextUserInDuration);
		currentPosition = response.getNewPosition();
		assertTrue(currentPosition == 0);

	}

	@Test
	public void getNextAvailablePositionTest() {
		int currentPosition = 0;
		List<UserInStandbyGroup> lsUserInGroup = new ArrayList<>();
		lsUserInGroup.add(new UserInStandbyGroup());
		lsUserInGroup.add(new UserInStandbyGroup());

		currentPosition = planningController.getNextAvailablePosition(currentPosition, lsUserInGroup);
		assertTrue(currentPosition == 1);

		// start at first position because of end of list.
		currentPosition = planningController.getNextAvailablePosition(currentPosition, lsUserInGroup);
		assertTrue(currentPosition == 0);
	}

	@Test
	public void getIndexOfLastPlannedUserTest() {
		Long lastStartUserId = 1L;
		List<UserInStandbyGroup> lsUserInGroup = new ArrayList<>();
		UserInStandbyGroup uisg = new UserInStandbyGroup();
		User user = new User();
		user.setId(1L);
		uisg.setUser(user);
		lsUserInGroup.add(uisg);

		uisg = new UserInStandbyGroup();
		user = new User();
		user.setId(2L);
		uisg.setUser(user);
		lsUserInGroup.add(uisg);

		uisg = new UserInStandbyGroup();
		user = new User();
		user.setId(3L);
		uisg.setUser(user);
		lsUserInGroup.add(uisg);

		int result = -1;
		result = planningController.getIndexOfLastPlannedUser(lastStartUserId, lsUserInGroup);
		assertTrue(result == 0);

		lastStartUserId = 3L;
		result = planningController.getIndexOfLastPlannedUser(lastStartUserId, lsUserInGroup);
		assertTrue(result == 2);

		lastStartUserId = 4L;
		result = planningController.getIndexOfLastPlannedUser(lastStartUserId, lsUserInGroup);
		assertTrue(result == -1);
	}

	@Test
	public void logPhasesTest() {
		try {
			Map<Integer, PlanningPhaseDto> resultMap = new HashMap<>();
			List<UserInStandbyGroup> lsUserInStandbyGroups = new ArrayList<>();
			UserInStandbyGroup uisg = new UserInStandbyGroup();
			uisg.setId(1l);
			uisg.setStandbyGroup(new StandbyGroup());
			User user = new User();
			user.setFirstname("J.");
			user.setLastname("Unit");
			uisg.setUser(user);
			lsUserInStandbyGroups.add(uisg);
			user = new User();
			user.setFirstname("Nr.");
			user.setLastname("Zwei");
			uisg.setUser(user);
			lsUserInStandbyGroups.add(uisg);
			PlanningPhaseDto dto = new PlanningPhaseDto(new Date(), new Date(), lsUserInStandbyGroups);
			resultMap.put(1, dto);

			planningController.logPhases(resultMap);
		} catch (Exception e) {
			assertNull(e);
		}
	}

	@Test
	public void replaceUserInPlanTest() {

		StandbyScheduleActionDto actionDto = new StandbyScheduleActionDto();
		actionDto.setCurrentUserId(1L);
		actionDto.setNewUserId(2L);
		actionDto.setScheduleBodyId(1L);
		actionDto.setStandbyGroupId(1L);
		actionDto.setStatusId(1L);
		actionDto.setValidFromDate(new Date());
		actionDto.setValidToDate(new Date());
		String username = "JUnit";

		List<StandbyScheduleBody> lsStandbyScheduleBodies = new ArrayList<>();
		lsStandbyScheduleBodies.add(new StandbyScheduleBody());

		when(standbyScheduleBodyRepository.findByUserAndGroupAndDateAndStatus(Mockito.anyLong(), Mockito.anyLong(),
				Mockito.any(), Mockito.any(), Mockito.anyLong())).thenReturn(lsStandbyScheduleBodies);
		when(standbyStatusRepository.findOne(Mockito.any())).thenReturn(new StandbyStatus());
		when(userRepository.findOne(Mockito.any())).thenReturn(new User());
		when(userRepository.findOne(Mockito.any())).thenReturn(new User());
		when(standbyScheduleBodyRepository.save(Mockito.any(StandbyScheduleBody.class)))
				.thenReturn(new StandbyScheduleBody());

		// success
		try {
			when(userController.isUserInGroup(Mockito.anyLong(), Mockito.anyLong(), Mockito.any(), Mockito.any()))
					.thenReturn(true);
			planningController.replaceUserInPlan(actionDto, username);
		} catch (Exception e) {
			assertNull(e);
		}

		// test error handling
		Mockito.doThrow(new IllegalArgumentException()).when(userRepository).findOne(Mockito.any());
		try {
			planningController.replaceUserInPlan(actionDto, username);
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	@Test
	public void splitScheduleBodyTest() {
		Date defaultStartDate = DateHelper.getDate(2018, 10, 1, 16, 0, 0);
		Date defaultEndDate = DateHelper.getDate(2018, 10, 1, 22, 0, 0);
		Date defaultEndDateOverlapping = DateHelper.getDate(2018, 10, 2, 8, 0, 0);

		StandbyScheduleBody body = new StandbyScheduleBody();
		body.setValidFrom(defaultStartDate);
		body.setValidTo(defaultStartDate);
		Date from = defaultStartDate;
		Date to = defaultStartDate;
		User newUser = new User();
		String modifiedBy = "JUnit";

		// negative test - null pointer
		try {
			StandbyScheduleBody bodyNull = null;
			planningController.splitScheduleBody(bodyNull, from, to, newUser, modifiedBy);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// positive tests
		when(standbyScheduleBodyRepository.save(Mockito.any(StandbyScheduleBody.class))).thenReturn(body);
		try {
			Boolean boolResult = false;

			// test success - no interval
			boolResult = planningController.splitScheduleBody(body, from, to, newUser, modifiedBy);
			assertTrue(boolResult);

			// test success - same start and same end
			body.setValidTo(defaultEndDate);
			to = defaultEndDate;
			boolResult = planningController.splitScheduleBody(body, from, to, newUser, modifiedBy);
			assertTrue(boolResult);

			// test success - same start and earlier end
			body.setValidTo(defaultEndDate);
			to = DateHelper.getDate(2018, 10, 1, 18, 0, 0);
			boolResult = planningController.splitScheduleBody(body, from, to, newUser, modifiedBy);
			assertTrue(boolResult);

			// test success - same start and later end
			body.setValidTo(defaultEndDate);
			to = DateHelper.getDate(2018, 10, 1, 23, 0, 0);
			boolResult = planningController.splitScheduleBody(body, from, to, newUser, modifiedBy);
			assertTrue(boolResult);

			// test success - same start and overlaping end
			body.setValidTo(defaultEndDate);
			to = defaultEndDateOverlapping;
			boolResult = planningController.splitScheduleBody(body, from, to, newUser, modifiedBy);
			assertTrue(boolResult);

			// test success - later start and same end
			body.setValidTo(defaultEndDate);
			to = defaultEndDate;
			from = DateHelper.getDate(2018, 10, 1, 19, 0, 0);
			boolResult = planningController.splitScheduleBody(body, from, to, newUser, modifiedBy);
			assertTrue(boolResult);

			// test success - later start and later end
			body.setValidTo(defaultEndDate);
			to = DateHelper.getDate(2018, 10, 1, 23, 0, 0);
			from = DateHelper.getDate(2018, 10, 1, 19, 0, 0);
			boolResult = planningController.splitScheduleBody(body, from, to, newUser, modifiedBy);
			assertTrue(boolResult);

			// test success - later start and earlier end
			body.setValidTo(defaultEndDate);
			to = DateHelper.getDate(2018, 10, 1, 21, 0, 0);
			from = DateHelper.getDate(2018, 10, 1, 19, 0, 0);
			boolResult = planningController.splitScheduleBody(body, from, to, newUser, modifiedBy);
			assertTrue(boolResult);

			// test success - overlaping start
			body.setValidTo(defaultEndDate);
			to = defaultEndDate;
			from = DateHelper.getDate(2018, 9, 30, 19, 0, 0);
			boolResult = planningController.splitScheduleBody(body, from, to, newUser, modifiedBy);
			assertTrue(boolResult);
		} catch (Exception e) {
			assertNull(e);
		}
	}

	@Test
	public void moveUserInPlanTest() {
		StandbyScheduleActionDto actionDto = new StandbyScheduleActionDto();
		actionDto.setScheduleBodyId(1L);
		actionDto.setValidFromDate(new Date());
		actionDto.setValidToDate(new Date());
		actionDto.setNewUserId(1L);
		actionDto.setStandbyGroupId(1L);
		String username = "JUnit";

		StandbyScheduleBody responseBody = new StandbyScheduleBody();
		responseBody.setId(1L);
		StandbyStatus status = new StandbyStatus();
		status.setId(SpMsg.STATUS_PLANNING);
		responseBody.setStatus(status);
		StandbyGroup responseGroup = new StandbyGroup();
		responseGroup.setId(1L);
		responseBody.setStandbyGroup(responseGroup);
		when(standbyScheduleBodyRepository.findOne(Mockito.any())).thenReturn(responseBody);
		when(standbyGroupRepository.findOne(actionDto.getStandbyGroupId())).thenReturn(responseGroup);
		when(standbyScheduleBodyRepository.save(Mockito.any(StandbyScheduleBody.class))).thenReturn(responseBody);

		// test success - equal date
		try {
			when(validationController.startValidation(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any(),
					Mockito.any(), Mockito.any())).thenReturn(new ArrayList<PlanningMsgDto>());
			when(userController.isUserValid(Mockito.anyLong(), Mockito.any(), Mockito.any())).thenReturn(true);
			when(userController.isUserInGroup(Mockito.anyLong(), Mockito.anyLong(), Mockito.any(), Mockito.any()))
					.thenReturn(true);
			when(archiveController.createArchiveOnMove(Mockito.any(), Mockito.any())).thenReturn(true);
			// when(mailRequestSendDate.sendMail(Mockito.any(), Mockito.any(),
			// Mockito.any(), Mockito.any()))
			// .thenReturn(null);

			planningController.moveUserInPlan(actionDto, username);
		} catch (Exception e) {
			assertNull(e);
		}

		// test success - different date
		actionDto.setValidToDate(DateHelper.addDaysToDate(new Date(), 3));
		try {
			when(userController.isUserValid(Mockito.anyLong(), Mockito.any(), Mockito.any())).thenReturn(true);
			when(userController.isUserInGroup(Mockito.anyLong(), Mockito.anyLong(), Mockito.any(), Mockito.any()))
					.thenReturn(true);
			planningController.moveUserInPlan(actionDto, username);
		} catch (Exception e) {
			assertNull(e);
		}

		// test success - bigger from date
		actionDto.setValidFromDate(DateHelper.addDaysToDate(new Date(), 3));
		actionDto.setValidToDate(new Date());
		try {
			when(userController.isUserValid(Mockito.anyLong(), Mockito.any(), Mockito.any())).thenReturn(true);
			when(userController.isUserInGroup(Mockito.anyLong(), Mockito.anyLong(), Mockito.any(), Mockito.any()))
					.thenReturn(true);
			planningController.moveUserInPlan(actionDto, username);
		} catch (Exception e) {
			assertNull(e);
		}

		// test error handling
		Mockito.doThrow(new IllegalArgumentException()).when(standbyScheduleBodyRepository).findOne(Mockito.any());
		try {
			planningController.moveUserInPlan(actionDto, username);
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	/**
	 * method to get a test data UserInStandbyGroup List.
	 * 
	 * @return
	 */
	private List<UserInStandbyGroup> getTestDataForUserInStandbyGroups() {
		List<UserInStandbyGroup> lsUserInStandbyGroups = new ArrayList<>();
		UserInStandbyGroup uisg = new UserInStandbyGroup();
		uisg.setId(1l);
		uisg.setStandbyGroup(new StandbyGroup());
		uisg.setPosition(3);
		User user = new User();
		user.setId(1L);
		user.setFirstname("J.");
		user.setLastname("Unit");
		uisg.setUser(user);
		lsUserInStandbyGroups.add(uisg);
		uisg = new UserInStandbyGroup();
		uisg.setId(2l);
		uisg.setStandbyGroup(new StandbyGroup());
		uisg.setPosition(1);
		user = new User();
		user.setId(2L);
		user.setFirstname("Nr.");
		user.setLastname("Zwei");
		uisg.setUser(user);
		lsUserInStandbyGroups.add(uisg);
		return lsUserInStandbyGroups;
	}

	@Test
	public void calculateHolidayTest() {
		StandbyGroup group = new StandbyGroup();
		Date date = DateHelper.getDate(2018, 1, 1);
		StandbyScheduleBody stbyBody = new StandbyScheduleBody();
		StandbyScheduleBody resultExtend = new StandbyScheduleBody();
		resultExtend.setId(1L);
		StandbyScheduleBody resultPreDraw = new StandbyScheduleBody();
		resultPreDraw.setId(2L);

		try {
			Mockito.doReturn(true).when(planningController).isHoliday(Mockito.any(Date.class),
					Mockito.any(StandbyGroup.class));
			Mockito.doReturn(resultExtend).when(planningController).calculateHolidayExtend(Mockito.any(), Mockito.any(),
					Mockito.any());
			Mockito.doReturn(resultPreDraw).when(planningController).calculateHolidayPreDraw(Mockito.any(),
					Mockito.any(), Mockito.any());

			// test success 01 - holiday extend
			group.setExtendStandbyTime(true);
			StandbyScheduleBody result = planningController.calculateHoliday(group, date, stbyBody, false);
			assertNotNull(result);
			assertTrue(result.getId() == 1);

			// test success 02 - holiday pre draw
			group.setExtendStandbyTime(false);
			result = planningController.calculateHoliday(group, date, stbyBody, false);
			assertTrue(result.getId() == 2);
			assertNotNull(result);

			// test success 03 - no holiday
			Mockito.doReturn(false).when(planningController).isHoliday(Mockito.any(Date.class),
					Mockito.any(StandbyGroup.class));
			result = planningController.calculateHoliday(group, date, stbyBody, false);
			assertNotNull(result);

		} catch (Exception e) {
			assertNull(e);
		}
	}

	@Test
	public void calculateHolidayExtendTest() {
		StandbyGroup group = new StandbyGroup();
		group.setExtendStandbyTime(false);
		Date date = DateHelper.getDate(2018, 1, 1, 10, 0, 0);
		StandbyScheduleBody stbyBody = new StandbyScheduleBody();

		StandbyDuration standbyDuration = new StandbyDuration();
		standbyDuration.setValidFrom(date);
		standbyDuration.setValidFrom(DateHelper.addDaysToDate(date, 1));

		List<StandbyDuration> lsStandbyDurationsNull = new ArrayList<>();
		lsStandbyDurationsNull.add(null);

		List<StandbyDuration> lsStandbyDurations = new ArrayList<>();
		StandbyDuration stbyDuration = new StandbyDuration();
		// after valid from of stbyBody
		stbyDuration.setValidFrom(DateHelper.getDate(2018, 1, 1, 20, 0, 0));
		stbyDuration.setValidTo(DateHelper.getDate(2018, 1, 1, 22, 0, 0));
		lsStandbyDurations.add(stbyDuration);

		stbyDuration = new StandbyDuration();
		// before valid from of stbyBody
		stbyDuration.setValidFrom(DateHelper.getDate(2018, 1, 1, 8, 0, 0));
		stbyDuration.setValidTo(DateHelper.getDate(2018, 1, 1, 9, 0, 0));
		lsStandbyDurations.add(stbyDuration);

		try {
			StandbyScheduleBody result = null;
			// success 01 - test null return date before valid from
			stbyBody = this.getStandbyScheduleBody();
			Mockito.doReturn(null).when(planningController).getFittingDurationsOfGroup(Mockito.any(StandbyGroup.class),
					Mockito.any(Date.class));
			result = planningController.calculateHolidayExtend(group, date, stbyBody);
			assertNotNull(result);

			// success 02 - test null return of list element
			stbyBody = this.getStandbyScheduleBody();
			Mockito.doReturn(lsStandbyDurationsNull).when(planningController)
					.getFittingDurationsOfGroup(Mockito.any(StandbyGroup.class), Mockito.any(Date.class));
			result = planningController.calculateHolidayExtend(group, date, stbyBody);
			assertNotNull(result);

			// success 03 - test null return of list element
			stbyBody = this.getStandbyScheduleBody();
			Mockito.doReturn(lsStandbyDurations).when(planningController)
					.getFittingDurationsOfGroup(Mockito.any(StandbyGroup.class), Mockito.any(Date.class));
			result = planningController.calculateHolidayExtend(group, date, stbyBody);
			assertNotNull(result);

		} catch (Exception e) {
			LOGGER.error(e, e);
			assertNull(e);
		}
	}

	@Test
	public void calculateHolidayPreDrawTest() {
		StandbyGroup group = new StandbyGroup();
		Date date = DateHelper.getDate(2018, 1, 1, 10, 0, 0);
		StandbyScheduleBody stbyBody = this.getStandbyScheduleBody();

		List<StandbyDuration> lsStandbyDurationsNull = new ArrayList<>();
		lsStandbyDurationsNull.add(null);

		List<StandbyDuration> lsStandbyDurations = new ArrayList<>();
		StandbyDuration stbyDuration = new StandbyDuration();
		// after valid from of stbyBody
		stbyDuration.setValidFrom(DateHelper.getDate(2018, 1, 1, 20, 0, 0));
		stbyDuration.setValidTo(DateHelper.getDate(2018, 1, 1, 22, 0, 0));
		lsStandbyDurations.add(stbyDuration);

		stbyDuration = new StandbyDuration();
		// before valid from of stbyBody
		stbyDuration.setValidFrom(DateHelper.getDate(2018, 1, 1, 8, 0, 0));
		stbyDuration.setValidTo(DateHelper.getDate(2018, 1, 1, 9, 0, 0));
		lsStandbyDurations.add(stbyDuration);

		try {
			StandbyScheduleBody result = null;

			// success 01 - test null return date before valid from
			stbyBody = this.getStandbyScheduleBody();
			Mockito.doReturn(null).when(planningController)
					.getFittingDurationsByValidTo(Mockito.any(StandbyGroup.class), Mockito.any(Date.class));
			result = planningController.calculateHolidayPreDraw(group, date, stbyBody);
			assertNotNull(result);

			// success 02 - dur = null
			stbyBody = this.getStandbyScheduleBody();
			Mockito.doReturn(lsStandbyDurationsNull).when(planningController)
					.getFittingDurationsByValidTo(Mockito.any(StandbyGroup.class), Mockito.any(Date.class));
			List<StandbyScheduleBody> lsBodies = new ArrayList<>();
			lsBodies.add(stbyBody);
			Mockito.doReturn(lsBodies).when(standbyScheduleBodyRepository).findByGroupAndStatusHittingDateInterval(
					Mockito.any(Long.class), Mockito.any(Date.class), Mockito.any(Date.class), Mockito.any(Long.class));
			result = planningController.calculateHolidayPreDraw(group, date, stbyBody);
			assertNotNull(result);

			// success 03 - duration before stbyBody
			stbyBody = this.getStandbyScheduleBody();
			Mockito.doReturn(lsStandbyDurations).when(planningController)
					.getFittingDurationsByValidTo(Mockito.any(StandbyGroup.class), Mockito.any(Date.class));
			result = planningController.calculateHolidayPreDraw(group, date, stbyBody);
			assertNotNull(result);

			// success 04 - duration after stbyBody
			stbyBody = this.getStandbyScheduleBody();
			Mockito.doReturn(lsStandbyDurations).when(planningController)
					.getFittingDurationsByValidTo(Mockito.any(StandbyGroup.class), Mockito.any(Date.class));
			result = planningController.calculateHolidayPreDraw(group, DateHelper.getDate(2018, 1, 1, 23, 0, 0),
					stbyBody);
			assertNotNull(result);

		} catch (Exception e) {
			LOGGER.error(e, e);
			assertNull(e);
		}
	}

	private StandbyScheduleBody getStandbyScheduleBody() {
		StandbyScheduleBody stbyBody = new StandbyScheduleBody();
		stbyBody.setValidFrom(DateHelper.getDate(2018, 1, 1, 12, 0, 0));
		stbyBody.setValidTo(DateHelper.getDate(2018, 1, 1, 18, 0, 0));
		User user = new User();
		user.setFirstname("J.");
		user.setLastname("Unit");
		stbyBody.setUser(user);
		StandbyStatus status = new StandbyStatus();
		status.setId(1L);
		status.setTitle("Test");
		stbyBody.setStatus(status);
		return stbyBody;
	}
}
