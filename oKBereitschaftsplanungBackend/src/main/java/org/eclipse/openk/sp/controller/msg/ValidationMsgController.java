/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.msg;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.AbstractController;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.util.DateHelper;
import org.eclipse.openk.sp.util.SpMsg;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle planning operations. */
public class ValidationMsgController extends AbstractController {
	protected static final Logger LOGGER = Logger.getLogger(ValidationMsgController.class);
	private static final String TEXT_VALIDATION = "[Validation] ";
	private static final String TEXT_STANDBY_USER = "Die verwendete Bereitschaft {0}";
	private static final String TEXT_STANDBY_USER_CHANGE = "Die geänderte / entfernte Bereitschaft {0}";

	public static List<PlanningMsgDto> createMsgStartValidation(Date startDate, Date endDate,
			List<StandbyGroup> lsStandbyGroups) {
		List<PlanningMsgDto> lsMsgDto = new ArrayList<>();
		String info = "Die Validierung wird jetzt für den Zeitraum vom (" + DateHelper.getStartOfDay(startDate)
				+ ") bis zum (" + DateHelper.getStartOfDay(endDate) + ") gestartet.";
		LOGGER.info(info);
		PlanningMsgDto dto = new PlanningMsgDto(info, PlanningMsgDto.INFO, PlanningMsgDto.CSS_INFO);
		lsMsgDto.add(dto);

		StringBuilder builder = new StringBuilder();
		for (StandbyGroup group : lsStandbyGroups) {
			String name = group.getTitle();
			if (builder.toString().isEmpty()) {
				builder.append(name);
			} else {
				builder.append(", " + name);
			}
		}
		info = "Folgende Gruppen unterliegen der Prüfung: " + builder.toString();
		LOGGER.info(info);
		dto = new PlanningMsgDto(info, PlanningMsgDto.INFO, PlanningMsgDto.CSS_INFO);
		lsMsgDto.add(dto);
		return lsMsgDto;
	}

	public static List<PlanningMsgDto> createMsgEndValidation(List<PlanningMsgDto> lsInputMsgDtos) {
		int warn = 0;
		int error = 0;

		for (PlanningMsgDto msgDto : lsInputMsgDtos) {
			if (msgDto.getCssType().equals(PlanningMsgDto.CSS_WARN)) {
				warn++;
			} else if (msgDto.getCssType().equals(PlanningMsgDto.CSS_DANGER)) {
				error++;
			}
		}

		List<PlanningMsgDto> lsMsgDto = new ArrayList<>();
		if (error == 0 && warn == 0) {
			String info = "Die Validierung wurde ohne Fehler abgeschlossen.";
			LOGGER.info(info);
			PlanningMsgDto dto = new PlanningMsgDto(info, PlanningMsgDto.INFO, PlanningMsgDto.CSS_INFO);
			lsMsgDto.add(dto);
		} else {
			String warning = "Die Validierung wurde mit " + warn + " Warnung(en) abgeschlossen";
			LOGGER.info(warning);
			PlanningMsgDto dto = new PlanningMsgDto(warning, PlanningMsgDto.WARN, PlanningMsgDto.CSS_WARN);
			lsMsgDto.add(dto);
		}
		return lsMsgDto;
	}

	public static List<PlanningMsgDto> createMsgMissingTimeSlot(Date startDate, Date endDate, StandbyGroup group) {
		List<PlanningMsgDto> lsMsgDto = new ArrayList<>();
		String info = TEXT_VALIDATION + "[" + group.getTitle() + "] "
				+ " Fehlende oder nur Teilbelegung in folgendem Fall: vom (" + startDate + ") bis zum (" + endDate
				+ ") für die Gruppe (" + group.getTitle() + ").";
		LOGGER.trace(info);
		PlanningMsgDto dto = new PlanningMsgDto(info, PlanningMsgDto.WARN, PlanningMsgDto.CSS_WARN);
		lsMsgDto.add(dto);
		return lsMsgDto;
	}

	public static List<PlanningMsgDto> createMsgInvalidTimeForUser(User usr) {
		List<PlanningMsgDto> lsMsgDto = new ArrayList<>();
		String warning = TEXT_VALIDATION + SpMsg.getLbl(TEXT_STANDBY_USER, usr.getFirstname() + " " + usr.getLastname())
				+ " wird verschoben obwohl er für diesen Zeitraum nicht valide ist.";
		PlanningMsgDto dto = new PlanningMsgDto(warning, PlanningMsgDto.WARN, PlanningMsgDto.CSS_WARN);
		lsMsgDto.add(dto);
		return lsMsgDto;
	}

	public static List<PlanningMsgDto> createMsgInvalidGroupForUser(User usr, StandbyGroup standbyGroup) {
		List<PlanningMsgDto> lsMsgDto = new ArrayList<>();
		String warning = TEXT_VALIDATION + "[" + standbyGroup.getTitle() + "] "
				+ SpMsg.getLbl(TEXT_STANDBY_USER, usr.getFirstname() + " " + usr.getLastname())
				+ " ist für die Gruppe (" + standbyGroup.getTitle() + ") nicht valide.";
		PlanningMsgDto dto = new PlanningMsgDto(warning, PlanningMsgDto.WARN, PlanningMsgDto.CSS_WARN);
		lsMsgDto.add(dto);
		return lsMsgDto;
	}

	public static List<PlanningMsgDto> createMsgInvalidGroupForUser(StandbyScheduleBody body,
			StandbyScheduleBody alternativeBody) {
		List<PlanningMsgDto> lsMsgDto = new ArrayList<>();
		User usr = body.getUser();
		String warning = TEXT_VALIDATION + "[" + body.getStandbyGroup().getTitle() + "] "
				+ SpMsg.getLbl(TEXT_STANDBY_USER, usr.getFirstname() + " " + usr.getLastname())
				+ " ist im Zeitraum vom (" + body.getValidFrom() + ") bis zum (" + body.getValidTo()
				+ ") auch für die Gruppe (" + alternativeBody.getStandbyGroup().getTitle() + ") in der "
				+ alternativeBody.getStatus().getTitle() + " verplant.";
		PlanningMsgDto dto = new PlanningMsgDto(warning, PlanningMsgDto.WARN, PlanningMsgDto.CSS_WARN);
		lsMsgDto.add(dto);
		return lsMsgDto;
	}

	public static List<PlanningMsgDto> createMsgPlaceholderFound(StandbyScheduleBody body) {
		List<PlanningMsgDto> lsMsgDto = new ArrayList<>();
		User usr = body.getUser();
		String warning = TEXT_VALIDATION + "[" + body.getStandbyGroup().getTitle() + "] "
				+ SpMsg.getLbl(TEXT_STANDBY_USER, usr.getFirstname() + " " + usr.getLastname())
				+ " ist als Platzhalter im Zeitraum vom (" + body.getValidFrom() + ") bis zum (" + body.getValidTo()
				+ ") für die Gruppe (" + body.getStandbyGroup().getTitle() + ") in der " + body.getStatus().getTitle()
				+ " verplant.";
		PlanningMsgDto dto = new PlanningMsgDto(warning, PlanningMsgDto.WARN, PlanningMsgDto.CSS_WARN);
		lsMsgDto.add(dto);
		return lsMsgDto;
	}

	public static List<PlanningMsgDto> createMsgUserWasPlanned(StandbyScheduleBody body) {
		List<PlanningMsgDto> lsMsgDto = new ArrayList<>();
		User usr = body.getUser();
		String warning = TEXT_VALIDATION + "[" + body.getStandbyGroup().getTitle() + "] "
				+ SpMsg.getLbl(TEXT_STANDBY_USER_CHANGE, usr.getFirstname() + " " + usr.getLastname())
				+ "  ist in der Gruppe (" + body.getStandbyGroup().getTitle() + ") im Zeitraum vom ("
				+ body.getValidFrom() + ") bis zum (" + body.getValidTo() + ") in der " + body.getStatus().getTitle()
				+ " verplant.";
		PlanningMsgDto dto = new PlanningMsgDto(warning, PlanningMsgDto.WARN, PlanningMsgDto.CSS_WARN);
		lsMsgDto.add(dto);
		return lsMsgDto;
	}

}
