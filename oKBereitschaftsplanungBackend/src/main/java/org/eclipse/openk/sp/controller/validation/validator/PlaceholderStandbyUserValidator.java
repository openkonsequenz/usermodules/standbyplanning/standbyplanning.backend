/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.validation.validator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.AbstractController;
import org.eclipse.openk.sp.controller.msg.ValidationMsgController;
import org.eclipse.openk.sp.controller.validation.IValidator;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to validate if placeholder standby users are in use */
public class PlaceholderStandbyUserValidator extends AbstractController implements IValidator {
	protected static final Logger LOGGER = Logger.getLogger(PlaceholderStandbyUserValidator.class);
	private static final String APP_PROPERTIES_PATH = "/application.properties";
	private static final String PROPERTY_KEY_PLACEHOLDER_IDS = "placeholder.standby.user.ids";

	@Autowired
	private StandbyScheduleBodyRepository scheduleBodyRepository;

	@Autowired
	private FileHelper fileHelper;

	@Override
	public List<PlanningMsgDto> execute(Date from, Date till, StandbyGroup currentGroup,
			List<StandbyGroup> lsStandbyGroups, Long statusId, Long userId) throws SpException {
		List<PlanningMsgDto> planningMsgDtos = new ArrayList<>();

		try {
			// fetch all active schedule bodies
			List<StandbyScheduleBody> lsBodies = scheduleBodyRepository
					.findByGroupAndDateAndStatus(currentGroup.getId(), from, till, statusId);

			// get list of placeholder user
			String placeholerIdString = fileHelper.getProperty(APP_PROPERTIES_PATH, PROPERTY_KEY_PLACEHOLDER_IDS);
			long[] placeholderIds = Arrays.stream(placeholerIdString.split(",")).mapToLong(Integer::parseInt).toArray();

			// loop over every single scheduleBody and check if the user is planned in other
			// groups at the same time
			for (StandbyScheduleBody body : lsBodies) {
				if (ArrayUtils.contains(placeholderIds, body.getUser().getId().longValue())) {
					planningMsgDtos.addAll(ValidationMsgController.createMsgPlaceholderFound(body));
				}
			}
			return planningMsgDtos;
		} catch (Exception e) {
			throw new SpException(SpExceptionEnum.DEFAULT_EXCEPTION.getEntry());
		}
	}
}
