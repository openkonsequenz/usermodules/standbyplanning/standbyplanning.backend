/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.report;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ReportInputDtoTest {

	@InjectMocks
	ReportInputDtoTest reportInputDto;

	@Mock
	private EntityConverter entityConverter;

	@Test
	public void testGettersAndSetters() {

		StandbyScheduleBody standbyScheduleBody = new StandbyScheduleBody();

		ReportDto reportDto = new ReportDto();
		reportDto.setReportName("Wochenübersicht");
		ReportInputDto reportInputDto = new ReportInputDto();

		reportInputDto.setReportDto(reportDto);
		assertNotNull(reportInputDto.getReportDto());

		reportInputDto.setGroupSort(1);
		assertEquals(1, reportInputDto.getGroupSort());

		reportInputDto.setStandbyScheduleBody(standbyScheduleBody);
		assertNotNull(reportInputDto.getStandbyScheduleBody());

		ReportGroupDto gDto = new ReportGroupDto();
		reportInputDto.setReportGroupDto(gDto);
		assertNotNull(reportInputDto.getReportGroupDto());

	}

}
