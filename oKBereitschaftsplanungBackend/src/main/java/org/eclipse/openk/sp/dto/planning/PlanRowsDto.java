/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySelectionDto;

public class PlanRowsDto extends AbstractDto {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String label;
	private String style;

	private List<List<StandbyScheduleBodySelectionDto>> listGroupBodies = new ArrayList<>();
	private HashMap<Long, List<StandbyScheduleBodySelectionDto>> dayGroupMap = new HashMap<>();

	public PlanRowsDto() {
	}

	/**
	 * Initialize the row structure with ArrayLists for every group
	 * 
	 * @param listStandbyGroupDto
	 */
	public PlanRowsDto(List<StandbyGroupSelectionDto> listStandbyGroupDto) {

		if (listStandbyGroupDto != null) {

			for (int i = 0; i < listStandbyGroupDto.size(); i++) {

				StandbyGroupSelectionDto standbyGroupSelectionDto = listStandbyGroupDto.get(i);

				List<StandbyScheduleBodySelectionDto> listBodyDto = new ArrayList<>();
				listGroupBodies.add(listBodyDto);
				dayGroupMap.put(standbyGroupSelectionDto.getId(), listBodyDto);
			}
		}

	}

	/**
	 * Add a StandbyBody into the list of the corresponding group
	 * 
	 * @param body
	 * @return
	 */
	public boolean addStandbyBody(StandbyScheduleBodySelectionDto body) {

		if (body == null) {
			return false;
		}

		List<StandbyScheduleBodySelectionDto> list = dayGroupMap.get(body.getStandbyGroup().getId());
		if (list == null) {
			return false;
		}

		list.add(body);

		// sort
		Collections.sort(list, (o1, o2) -> o1.getValidFrom().compareTo(o2.getValidFrom()));

		return true;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param lable the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}

	/**
	 * @return the listGroupBodys
	 */
	public List<List<StandbyScheduleBodySelectionDto>> getListGroupBodys() {
		return listGroupBodies;
	}

	/**
	 * @param listGroupBodys the listGroupBodys to set
	 */
	public void setListGroupBodys(List<List<StandbyScheduleBodySelectionDto>> listGroupBodys) {
		this.listGroupBodies = listGroupBodys;
	}

	/**
	 * @return the style
	 */
	public String getStyle() {
		return style;
	}

	/**
	 * @param style the style to set
	 */
	public void setStyle(String style) {
		this.style = style;
	}

	public List<StandbyScheduleBodySelectionDto> getGroupList(Long groupId) {
		return this.dayGroupMap.get(groupId);
	}

}
