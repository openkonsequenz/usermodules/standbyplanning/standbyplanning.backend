/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.List;

import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "LOCATION" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "LocationDto")
@JsonInclude(Include.NON_NULL)
public class LocationDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;
	@Size(max = 256, message = "Titel mit max. 256 Zeichen")
	private String title;
	@Size(max = 256, message = "Gemeinde mit max. 256 Zeichen")
	private String district;
	@Size(max = 8, message = "Kurztext mit max. 8 Zeichen")
	private String shorttext;
	@Size(max = 256, message = "Ort mit max. 256 Zeichen")
	private String community;
	private List<PostcodeDto> lsPostcode;
	@Size(max = 32, message = "Zone mit max. 32 Zeichen")
	private String wgs84zonedistrict;
	@Size(max = 32, message = "Länge mit max. 32 Zeichen")
	private String latitudedistrict;
	@Size(max = 32, message = "Breite mit max. 32 Zeichen")
	private String longitudedistrict;

	private List<RegionSmallDto> lsRegions;
	private List<LocationForBranchDto> lsLocationForBranches;

	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getShorttext() {
		return shorttext;
	}

	public void setShorttext(String shorttext) {
		this.shorttext = shorttext;
	}

	public String getCommunity() {
		return community;
	}

	public void setCommunity(String community) {
		this.community = community;
	}

	public String getWgs84zonedistrict() {
		return wgs84zonedistrict;
	}

	public void setWgs84zonedistrict(String wgs84zonedistrict) {
		this.wgs84zonedistrict = wgs84zonedistrict;
	}

	public String getLatitudedistrict() {
		return latitudedistrict;
	}

	public void setLatitudedistrict(String latitudedistrict) {
		this.latitudedistrict = latitudedistrict;
	}

	public String getLongitudedistrict() {
		return longitudedistrict;
	}

	public void setLongitudedistrict(String longitudedistrict) {
		this.longitudedistrict = longitudedistrict;
	}

	public List<RegionSmallDto> getLsRegions() {
		return lsRegions;
	}

	public void setLsRegions(List<RegionSmallDto> lsRegions) {
		this.lsRegions = lsRegions;
	}

	public List<PostcodeDto> getLsPostcode() {
		return lsPostcode;
	}

	public void setLsPostcode(List<PostcodeDto> lsPostcode) {
		this.lsPostcode = lsPostcode;
	}

	/**
	 * @return the lsLocationForBranches
	 */
	public List<LocationForBranchDto> getLsLocationForBranches() {
		return lsLocationForBranches;
	}

	/**
	 * @param lsLocationForBranches the lsLocationForBranches to set
	 */
	public void setLsLocationForBranches(List<LocationForBranchDto> lsLocationForBranches) {
		this.lsLocationForBranches = lsLocationForBranches;
	}
}
