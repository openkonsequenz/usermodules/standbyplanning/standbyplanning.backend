/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.model.Branch;
import org.eclipse.openk.sp.db.model.CalendarDay;
import org.eclipse.openk.sp.db.model.Region;
import org.eclipse.openk.sp.db.model.StandbyDuration;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.UserFunction;
import org.eclipse.openk.sp.db.model.UserInStandbyGroup;
import org.eclipse.openk.sp.dto.BranchDto;
import org.eclipse.openk.sp.dto.CalendarDayDto;
import org.eclipse.openk.sp.dto.CopyDto;
import org.eclipse.openk.sp.dto.RegionSelectionDto;
import org.eclipse.openk.sp.dto.StandbyDurationDto;
import org.eclipse.openk.sp.dto.StandbyGroupDto;
import org.eclipse.openk.sp.dto.UserFunctionSelectionDto;
import org.eclipse.openk.sp.dto.UserInStandbyGroupDto;
import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.eclipse.openk.sp.util.DateHelper;
import org.eclipse.openk.sp.util.SpMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/** Class to handle StandbyGroup operation copy. */
@Service
@Transactional(rollbackFor = Exception.class)
public class CopyController extends AbstractController {
	protected static final Logger LOGGER = Logger.getLogger(StandbyGroupController.class);

	@Autowired
	private StandbyGroupRepository standbyGroupRepository;

	@Autowired
	private EntityConverter entityConverter;

	@Autowired
	private StandbyGroupController standbyGroupController;

	public StandbyGroupDto copy(Long standbyGroupId, CopyDto copyDto) throws SpException {

		StandbyGroup standbyGroup = standbyGroupRepository.findOne(standbyGroupId);

		StandbyGroup newSBG = new StandbyGroup();
		newSBG.setExtendStandbyTime(standbyGroup.getExtendStandbyTime());
		newSBG.setModificationDate(new Date());
		newSBG.setNextUserInNextCycle(standbyGroup.getNextUserInNextCycle());
		newSBG.setNote(standbyGroup.getNote());
		newSBG.setTitle(standbyGroup.getTitle() + " Kopie");

		// first save for getting an id
		newSBG = standbyGroupRepository.save(newSBG);

		// Branch
		if (doCopy(copyDto.isCopyBranch(), standbyGroup.getLsBranches())) {
			copyDeepBranches(standbyGroup, newSBG.getId());
		}

		// Calendar
		if (doCopy(copyDto.isCopyCalendar(), standbyGroup.getLsIgnoredCalendarDays())) {
			copyDeepCalendar(standbyGroup, newSBG.getId());
		}

		// Duration
		if (doCopy(copyDto.isCopyDuration(), standbyGroup.getLsStandbyDurations())) {
			copyDeepDuration(standbyGroup, newSBG.getId());
		}

		// Function
		if (doCopy(copyDto.isCopyFunction(), standbyGroup.getLsUserFunction())) {
			copyDeepFunction(standbyGroup, newSBG.getId());
		}

		// Region
		if (doCopy(copyDto.isCopyRegion(), standbyGroup.getLsRegions())) {
			copyDeepRegion(standbyGroup, newSBG.getId());
		}

		// User
		if (doCopy(copyDto.isCopyUser(), standbyGroup.getLsUserInStandbyGroups())) {
			copyDeepUser(standbyGroup, newSBG.getId(), copyDto.isUserPlusOneYear());
		}

		newSBG = standbyGroupRepository.save(newSBG);
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) entityConverter.convertEntityToDto(newSBG,
				new StandbyGroupDto());

		return standbyGroupDto;
	}

	private boolean doCopy(boolean flag, List list) {
		boolean doCopy = false;
		if (flag) {
			if (list != null && !list.isEmpty()) {
				doCopy = true;
			}
		}
		return doCopy;
	}

	private void copyDeepCalendar(StandbyGroup standbyGroup, Long id) throws SpException {

		List<CalendarDay> list = standbyGroup.getLsIgnoredCalendarDays();

		List<CalendarDayDto> listDto = new ArrayList<>();
		for (CalendarDay entity : list) {
			CalendarDayDto dto = new CalendarDayDto();
			dto.setId(entity.getId());
			listDto.add(dto);
		}
		standbyGroupController.saveIgnoredCalendarDayList(id, listDto);

	}

	public void copyDeepDuration(StandbyGroup standbyGroup, Long id) throws SpException {

		List<StandbyDuration> list = standbyGroup.getLsStandbyDurations();
		StandbyDuration[] array = list.toArray(new StandbyDuration[list.size()]);
		AbstractEntity[] arrayAbstr = array;

		List abstList = null;

		try {
			abstList = calcCopyOfEntityList(arrayAbstr, new StandbyDurationDto());

		} catch (InstantiationException | IllegalAccessException argE) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_ENTITY_WITH_ID, "Bereitschaftsdauer für Gruppe", id);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;

		}
		standbyGroupController.saveDurationList(id, abstList);

	}

	private void copyDeepFunction(StandbyGroup standbyGroup, Long id) throws SpException {

		List<UserFunction> list = standbyGroup.getLsUserFunction();

		List<UserFunctionSelectionDto> listDto = new ArrayList<>();
		for (UserFunction entity : list) {
			UserFunctionSelectionDto dto = new UserFunctionSelectionDto();
			dto.setFunctionId(entity.getId());
			listDto.add(dto);
		}
		standbyGroupController.saveUserFunctionList(id, listDto);

	}

	private void copyDeepRegion(StandbyGroup standbyGroup, Long id) throws SpException {

		List<Region> list = standbyGroup.getLsRegions();

		List<RegionSelectionDto> listDto = new ArrayList<>();
		for (Region entity : list) {
			RegionSelectionDto dto = new RegionSelectionDto();
			dto.setId(entity.getId());
			listDto.add(dto);
		}
		standbyGroupController.saveRegionsForStandbyGroup(id, listDto);

	}

	public void copyDeepUser(StandbyGroup standbyGroup, Long id, boolean doPlusOneYear) throws SpException {

		List<UserInStandbyGroup> list = standbyGroup.getLsUserInStandbyGroups();
		UserInStandbyGroup[] array = list.toArray(new UserInStandbyGroup[list.size()]);
		AbstractEntity[] arrayAbstr = array;
		List abstList = null;

		try {
			abstList = calcCopyOfEntityList(arrayAbstr, new UserInStandbyGroupDto());

		} catch (InstantiationException | IllegalAccessException argE) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_ENTITY_WITH_ID, "Mitarbeiter für Gruppe ", id);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;

		}

		if (doPlusOneYear) {
			for (Object obj : abstList) {
				UserInStandbyGroupDto uISG = (UserInStandbyGroupDto) obj;
				uISG.setValidFrom(DateHelper.addYearsToDate(uISG.getValidFrom(), 1));
				uISG.setValidTo(DateHelper.addYearsToDate(uISG.getValidTo(), 1));
			}
		}

		standbyGroupController.saveUserList(id, abstList);

	}

	public void copyDeepBranches(StandbyGroup standbyGroup, Long newSbgId) {

		List<Branch> list = standbyGroup.getLsBranches();
		List<BranchDto> listDto = new ArrayList<>();
		for (Branch branch : list) {
			BranchDto dto = new BranchDto();
			dto.setId(branch.getId());
			listDto.add(dto);
		}

		standbyGroupController.saveBranchesForStandbyGroup(newSbgId, listDto);

	}

	public List<AbstractDto> calcCopyOfEntityList(AbstractEntity[] array, AbstractDto dto)
			throws InstantiationException, IllegalAccessException {

		List<AbstractDto> lsDtos = new ArrayList<>();

		for (AbstractEntity entity : array) {
			AbstractEntity newEntity = entity.copy();
			AbstractDto sdDto = entityConverter.convertEntityToDto(newEntity, dto.getClass().newInstance());
			lsDtos.add(sdDto);
		}

		return lsDtos;
	}

}
