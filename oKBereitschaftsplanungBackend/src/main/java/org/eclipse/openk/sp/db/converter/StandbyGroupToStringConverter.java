/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.converter;

import java.util.ArrayList;
import java.util.List;

import org.dozer.CustomConverter;
import org.eclipse.openk.sp.db.model.StandbyGroup;

public class StandbyGroupToStringConverter implements CustomConverter {

	@SuppressWarnings("rawtypes")
	@Override
	public Object convert(Object destination, Object source, Class destClass, Class sourceClass) {
		if (source instanceof List) {
			return listStandbyGroupToString((List) source);
		} else if (source instanceof String) {

			return stringToStandbyGroup((String) source);
		}
		return null;
	}

	public String listStandbyGroupToString(List source) {
		StringBuilder resultBuf = new StringBuilder("");
		for (Object object : source) {
			StandbyGroup standbyGroup = (StandbyGroup) object;
			if (!resultBuf.toString().equals("")) {
				resultBuf.append(",");
			}
			resultBuf.append(standbyGroup.getTitle());
		}

		return resultBuf.toString();
	}

	public List<StandbyGroup> stringToStandbyGroup(String source) {
		List<StandbyGroup> lsStandbyGroup = new ArrayList<>();

		if (source.contains(",")) {
			String[] arr = source.split(",");
			for (String str : arr) {
				addStandbyGroup(lsStandbyGroup, str);
			}
		} else {
			addStandbyGroup(lsStandbyGroup, source);
		}
		return lsStandbyGroup;
	}

	public List<StandbyGroup> addStandbyGroup(List<StandbyGroup> lsStandbyGroup, String str) {
		StandbyGroup standbyGroup = new StandbyGroup();
		standbyGroup.setTitle(str);
		lsStandbyGroup.add(standbyGroup);

		return lsStandbyGroup;
	}
}
