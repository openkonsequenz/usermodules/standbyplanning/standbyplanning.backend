/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp;

import java.io.IOException;
import java.sql.SQLException;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ws.rs.ApplicationPath;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.sp.db.config.DbInitializer;
import org.eclipse.openk.sp.util.FileHelper;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@ApplicationPath("/")
@EnableJpaRepositories(basePackages = { "org.eclipse.openk.sp.db.dao" })
@EnableTransactionManagement
public class StandbyPlanningApplication extends ResourceConfig {

	protected static final Logger LOGGER = Logger.getLogger(StandbyPlanningApplication.class.getName());

	private String baseURLPortal;

	@Autowired
	private DbInitializer dbInitSqlData;

	public StandbyPlanningApplication() {

		if (Logger.getLogger(this.getClass().getName()).isLoggable(Level.INFO)) {
			register(LoggingFeature.class);
		}

		LOGGER.info("\nSP-Application is starting...");

	}

	protected String configureApp()
			throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		LOGGER.info("configureApp - start");
		FileHelper fileHelper = new FileHelper();
		baseURLPortal = fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_BASE_URL);

		packages("org.eclipse.openk", "io.swagger.jaxrs.listing");
		// EncodingFilter.enableFor(this, GZipEncoder.class);
		property(ServerProperties.BV_SEND_ERROR_IN_RESPONSE, true);

		LOGGER.info("start creating shadow tables and inserting data");
		// start creating shadow tables and inserting data
		dbInitSqlData.configureDatabase();

		String baseTimeZone = fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_BASE_TIMEZONE);

		TimeZone.setDefault(TimeZone.getTimeZone(baseTimeZone));

		LOGGER.info("configureApp - end");
		return "configureApp - OK";
	}

	public String getBaseURLPortal() {
		return baseURLPortal;
	}

	public void setBaseURLPortal(String baseURLPortal) {
		this.baseURLPortal = baseURLPortal;
	}

	@PostConstruct
	public String postConstruct()
			throws IOException, InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
		LOGGER.info("PostConstruct start");

		try {
			this.configureApp();
			LOGGER.info("\n|||>>>>>>>>>>>>>>>>>>>>>>|||\n|||SP-Application started|||\n|||<<<<<<<<<<<<<<<<<<<<<<|||");

		} catch (Exception e) {
			LOGGER.info(
					"\n|||>>>>>>>>>>>>>>>>>>>>>>>>>>|||\\n|||Exception in PostConstruct|||\\n|||<<<<<<<<<<<<<<<<<<<<<<<<<<|||");
			LOGGER.warning(ExceptionUtils.getFullStackTrace(e));
		}
		LOGGER.info("PostConstruct finnished");
		return "postConstruct - OK";
	}

}
