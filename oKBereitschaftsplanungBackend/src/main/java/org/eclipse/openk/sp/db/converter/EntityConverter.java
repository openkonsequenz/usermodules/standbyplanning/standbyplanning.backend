/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.converter;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Class to convert objects that extends of {@link AbstractEntity} to objects
 * that extends of {@link AbstractDto} and back.
 * 
 * @author weber
 *
 */
@Service
public class EntityConverter {

	/** Logger. */
	private static final Logger LOGGER = Logger.getLogger(EntityConverter.class);

	/** dozer mapping API. */
	@Autowired
	private DozerBeanMapper mapper;

	/**
	 * Method to convert any entity that extends {@link AbstractEntity} to a given
	 * DTO class that extends {@link AbtractDto}.
	 * 
	 * @param entity
	 *            extends {@link AbstractEntity}
	 * @param dto
	 *            extends {@link AbtractDto}
	 * @return {@link AbstractDto}
	 */
	public AbstractDto convertEntityToDto(AbstractEntity entity, AbstractDto dto) {
		return mapper.map(entity, dto.getClass());
	}

	/**
	 * Method to convert any DTO class that extends {@link AbtractDto} to a given
	 * entity that extends {@link AbstractEntity}.
	 * 
	 * @param dto
	 *            extends {@link AbtractDto}
	 * @param entity
	 *            extends {@link AbstractEntity}
	 * @return {@link AbstractEntity}
	 */
	public AbstractEntity convertDtoToEntity(AbstractDto dto, AbstractEntity entity) {
		return mapper.map(dto, entity.getClass());
	}

	/**
	 * Method to convert a list of any entity class that extends
	 * {@link AbstractEntity} to a given dto list that extends {@link AbtractDto}.
	 * 
	 * @param listDto
	 *            extends list of {@link AbtractDto}
	 * @param listEntity
	 *            extends {@link AbstractEntity}
	 * @param dtoClass
	 *            defines the class that is converted
	 * @return {@link AbstractEntity}
	 */
	public List convertEntityToDtoList(List listEntity, List listDto, Class dtoClass) {
		try {
			if (listDto == null) {
				listDto = new ArrayList<>();
			}
			listDto.clear();
			for (Object obj : listEntity) {
				AbstractEntity abstractEntity = (AbstractEntity) obj;
				AbstractDto dto = (AbstractDto) dtoClass.newInstance();
				dto = convertEntityToDto(abstractEntity, dto);
				listDto.add(dto);
			}
		} catch (Exception e) {
			LOGGER.error(e, e);
		}
		return listDto;
	}

	/**
	 * Method to convert a list of any DTO class that extends {@link AbtractDto} to
	 * a given entity list that extends {@link AbstractEntity}.
	 * 
	 * @param listDto
	 *            extends list of {@link AbtractDto}
	 * @param listEntity
	 *            extends {@link AbstractEntity}
	 * @param defines
	 *            the class that is converted
	 * @return {@link AbstractEntity}
	 */
	public List convertDtoToEntityList(List<?> listDto, List listEntity, Class entityClass) {
		try {
			listEntity.clear();
			for (Object obj : listDto) {
				AbstractDto abstractDto = (AbstractDto) obj;
				Object entity = entityClass.newInstance();
				convertDtoToEntity(abstractDto, (AbstractEntity) entity);
				listEntity.add(entity);
			}
		} catch (Exception e) {
			LOGGER.error(e, e);
		}
		return listEntity;
	}
}