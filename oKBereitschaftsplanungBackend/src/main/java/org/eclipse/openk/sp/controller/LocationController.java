/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.BranchRepository;
import org.eclipse.openk.sp.db.dao.LocationForBranchRepository;
import org.eclipse.openk.sp.db.dao.LocationRepository;
import org.eclipse.openk.sp.db.dao.PostcodeRepository;
import org.eclipse.openk.sp.db.dao.RegionRepository;
import org.eclipse.openk.sp.db.model.Branch;
import org.eclipse.openk.sp.db.model.Location;
import org.eclipse.openk.sp.db.model.LocationForBranch;
import org.eclipse.openk.sp.db.model.Postcode;
import org.eclipse.openk.sp.db.model.Region;
import org.eclipse.openk.sp.dto.LabelValueDto;
import org.eclipse.openk.sp.dto.LocationDto;
import org.eclipse.openk.sp.dto.LocationForBranchDto;
import org.eclipse.openk.sp.dto.LocationSelectionDto;
import org.eclipse.openk.sp.dto.PostcodeDto;
import org.eclipse.openk.sp.dto.RegionSmallDto;
import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.eclipse.openk.sp.util.SpMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle LOCATION operations. */
public class LocationController extends AbstractController {
	protected static final Logger LOGGER = Logger.getLogger(LocationController.class);

	@Autowired
	private BranchRepository branchRepository;

	@Autowired
	private LocationRepository locationRepository;

	@Autowired
	private LocationForBranchRepository locForBranchRepository;

	@Autowired
	private PostcodeRepository postcodeRepository;

	@Autowired
	private RegionRepository regionRepository;

	@Autowired
	private EntityConverter entityConverter;

	/**
	 * Method to query all locations.
	 * 
	 * @return
	 */
	public List<LocationDto> getLocations() throws SpException {
		try {
			ArrayList<LocationDto> listLocation = new ArrayList<>();
			Iterable<Location> itLocationList = locationRepository.findAllByOrderByTitleAsc();
			for (Location location : itLocationList) {
				listLocation.add((LocationDto) entityConverter.convertEntityToDto(location, new LocationDto()));
			}
			return listLocation;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.LOADING_LIST_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_LOCATION);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * 
	 * @return
	 */
	public List<LocationSelectionDto> getLocationsSelection() throws SpException {
		try {
			ArrayList<LocationSelectionDto> listLocation = new ArrayList<>();
			Iterable<Location> itLocationList = locationRepository.findAllByOrderByTitleAsc();
			for (Location location : itLocationList) {
				listLocation.add((LocationSelectionDto) entityConverter.convertEntityToDto(location,
						new LocationSelectionDto()));
			}
			return listLocation;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.LOADING_LIST_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_LOCATION);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to select a location by its id.
	 * 
	 * @param locationid
	 * @return
	 */
	public LocationDto getLocation(Long locationid) throws SpException {
		try {
			Location location = locationRepository.findOne(locationid);
			return (LocationDto) entityConverter.convertEntityToDto(location, new LocationDto());
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_LOCATION_WITH_ID, locationid);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to persist a location.
	 * 
	 * @param locationDto
	 * @return
	 * @throws HttpStatusException
	 */
	public LocationDto saveLocation(LocationDto locationDto) throws SpException {
		Location location = new Location();
		List<LocationForBranch> listLocationForBranches = new ArrayList<>();
		List<Postcode> listPostcode = new ArrayList<>();
		List<Region> listRegion = new ArrayList<>();

		try {
			if (locationDto.getId() != null && locationRepository.exists(locationDto.getId())) {
				location = locationRepository.findOne(locationDto.getId());

				listLocationForBranches = location.getLsLocationForBranches();
				listPostcode = location.getLsPostcode();
				listRegion = location.getLsRegions();
			}

			location = (Location) entityConverter.convertDtoToEntity(locationDto, location);
			location.setLsLocationForBranches(listLocationForBranches);
			location.setLsPostcode(listPostcode);
			location.setLsRegions(listRegion);

			location = locationRepository.save(location);

			return (LocationDto) entityConverter.convertEntityToDto(location, new LocationDto());

		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.COULD_NOT_SAVE_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_LOCATION_WITH_ID, new Gson().toJson(locationDto));
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to persist a list of {@link LocationForBranch}
	 * 
	 * @param locationId
	 * @param lsLocationForBranchDtos
	 * @return
	 * @throws HttpStatusException
	 */
	@SuppressWarnings("unchecked")
	public List<LocationForBranchDto> saveBranchesForLocation(Long locationId,
			List<LocationForBranchDto> lsLocationForBranchDtos) throws SpException {
		Long tmpBranchId = null;
		try {
			Location location = locationRepository.findOne(locationId);

			for (LocationForBranchDto dto : lsLocationForBranchDtos) {

				if (locationRepository.exists(locationId) && branchRepository.exists(dto.getBranchId())) {
					tmpBranchId = dto.getBranchId();
					Branch branch = branchRepository.findOne(tmpBranchId);

					LocationForBranch lfb = new LocationForBranch();
					if (dto.getId() != null && locForBranchRepository.exists(dto.getId())) {
						lfb = locForBranchRepository.findOne(dto.getId());
					}

					lfb.setLocation(location);
					lfb.setBranch(branch);
					lfb.setValidFrom(dto.getValidFrom());
					lfb.setValidTo(dto.getValidTo());
					locForBranchRepository.save(lfb);

					if (!branch.getLsLocationForBranches().contains(lfb)) {
						branch.getLsLocationForBranches().add(lfb);
						branchRepository.save(branch);
					}

					if (!location.getLsLocationForBranches().contains(lfb)) {
						location.getLsLocationForBranches().add(lfb);
						locationRepository.save(location);
					}

				} else {
					throw new IllegalArgumentException();
				}
			}

			lsLocationForBranchDtos = new ArrayList<>();
			lsLocationForBranchDtos = entityConverter.convertEntityToDtoList(location.getLsLocationForBranches(),
					lsLocationForBranchDtos, LocationForBranchDto.class);

			return lsLocationForBranchDtos;

		} catch (InvalidDataAccessApiUsageException | IllegalArgumentException argE) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_LOCATION_WITH_ID, locationId) + ", "
					+ SpMsg.getLbl(SpMsg.TXT_BRANCHES_WITH_ID, tmpBranchId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;

		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			SpException spE = new SpException(ee.getCode(), e, ee.getMessage());
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to persist a standbyGroup.
	 *
	 * @param locationId
	 * @param lsLocationForBranchDtos
	 * @return
	 * @throws HttpStatusException
	 */
	@SuppressWarnings("unchecked")
	public List<LocationForBranchDto> deleteBranchesForLocation(Long locationId,
			List<LocationForBranchDto> lsLocationForBranchDtos) throws SpException {
		Long tmpBranchId = null;
		try {
			Location location = locationRepository.findOne(locationId);

			for (LocationForBranchDto dto : lsLocationForBranchDtos) {

				if (locationRepository.exists(locationId) && locForBranchRepository.exists(dto.getId())) {

					tmpBranchId = dto.getBranchId();
					LocationForBranch lfb = locForBranchRepository.findOne(dto.getId());

					if (location.getLsLocationForBranches().contains(lfb)) {
						location.getLsLocationForBranches().remove(lfb);
						locationRepository.save(location);
					}

					Branch branch = branchRepository.findOne(dto.getBranchId());
					if (branch.getLsLocationForBranches().contains(lfb)) {
						branch.getLsLocationForBranches().remove(lfb);
						branchRepository.save(branch);
					}
					locForBranchRepository.delete(lfb);

				} else {
					throw new IllegalArgumentException();
				}
			}

			lsLocationForBranchDtos = new ArrayList<>();
			lsLocationForBranchDtos = entityConverter.convertEntityToDtoList(location.getLsLocationForBranches(),
					lsLocationForBranchDtos, LocationForBranchDto.class);

			return lsLocationForBranchDtos;

		} catch (InvalidDataAccessApiUsageException | IllegalArgumentException argE) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_LOCATION_WITH_ID, locationId) + ", "
					+ SpMsg.getLbl(SpMsg.TXT_BRANCHES_WITH_ID, tmpBranchId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			SpException spE = new SpException(ee.getCode(), e, ee.getMessage());
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to persist a list of {@link LocationForBranch}
	 * 
	 * @param locationId
	 * @param lsRegionDtos
	 * @return
	 * @throws HttpStatusException
	 */
	@SuppressWarnings("unchecked")
	public List<RegionSmallDto> saveRegionsForLocation(Long locationId, List<RegionSmallDto> lsRegionDtos)
			throws SpException {
		Long tmpRegionId = null;
		try {
			Location location = locationRepository.findOne(locationId);

			for (RegionSmallDto dto : lsRegionDtos) {

				tmpRegionId = dto.getId();
				if (locationRepository.exists(locationId) && regionRepository.exists(tmpRegionId)) {

					Region region = regionRepository.findOne(tmpRegionId);
					List<Location> listLocation = region.getLsLocations();

					if (!listLocation.contains(location)) {
						listLocation.add(location);
						regionRepository.save(region);
					}

					if (!location.getLsRegions().contains(region)) {
						location.getLsRegions().add(region);
					}

				} else {
					throw new IllegalArgumentException();
				}
			}

			locationRepository.save(location);

			lsRegionDtos = new ArrayList<>();
			lsRegionDtos = entityConverter.convertEntityToDtoList(location.getLsRegions(), lsRegionDtos,
					RegionSmallDto.class);

			return lsRegionDtos;
		} catch (InvalidDataAccessApiUsageException | IllegalArgumentException argE) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_LOCATION_WITH_ID, locationId) + ", "
					+ SpMsg.getLbl(SpMsg.TXT_REGION_WITH_ID, tmpRegionId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			SpException spE = new SpException(ee.getCode(), e, ee.getMessage());
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to persist a standbyGroup.
	 *
	 * @param locationId
	 * @param lsRegionDtos
	 * @return
	 * @throws HttpStatusException
	 */
	@SuppressWarnings("unchecked")
	public List<RegionSmallDto> deleteRegionsForLocation(Long locationId, List<RegionSmallDto> lsRegionDtos)
			throws SpException {
		Long tmpRegionId = null;

		try {
			Location location = locationRepository.findOne(locationId);

			for (RegionSmallDto dto : lsRegionDtos) {

				tmpRegionId = dto.getId();
				if (locationRepository.exists(locationId) && regionRepository.exists(tmpRegionId)) {

					Region region = regionRepository.findOne(tmpRegionId);
					List<Location> listLocation = region.getLsLocations();

					if (listLocation.contains(location)) {
						region.getLsLocations().remove(location);
						regionRepository.save(region);
					}

					List<Region> listRegions = location.getLsRegions();
					if (listRegions.contains(region)) {
						location.getLsRegions().remove(region);
						locationRepository.save(location);
					}
				} else {
					throw new IllegalArgumentException();
				}

			}

			lsRegionDtos = new ArrayList<>();
			lsRegionDtos = entityConverter.convertEntityToDtoList(location.getLsRegions(), lsRegionDtos,
					RegionSmallDto.class);

			return lsRegionDtos;

		} catch (InvalidDataAccessApiUsageException | IllegalArgumentException argE) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_LOCATION_WITH_ID, locationId) + ", "
					+ SpMsg.getLbl(SpMsg.TXT_REGION_WITH_ID, tmpRegionId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			SpException spE = new SpException(ee.getCode(), e, ee.getMessage());
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to persist a list of {@link LocationForBranch}
	 * 
	 * @param locationId
	 * @param lsPostcodeDtos
	 * @return
	 * @throws HttpStatusException
	 */
	@SuppressWarnings("unchecked")
	public List<PostcodeDto> savePostcodeForLocation(Long locationId, List<PostcodeDto> lsPostcodeDtos)
			throws SpException {
		Long tmpPostcodeId = null;
		try {

			Location location = locationRepository.findOne(locationId);

			for (PostcodeDto dto : lsPostcodeDtos) {
				tmpPostcodeId = dto.getId();
				if (locationRepository.exists(locationId) && postcodeRepository.exists(tmpPostcodeId)) {

					Postcode postcode = postcodeRepository.findOne(tmpPostcodeId);

					if (!location.getLsPostcode().contains(postcode)) {
						location.getLsPostcode().add(postcode);
						locationRepository.save(location);
					}

					if (!postcode.getLsLocation().contains(location)) {
						postcode.getLsLocation().add(location);
						postcodeRepository.save(postcode);
					}

				} else {
					throw new IllegalArgumentException();
				}
			}

			lsPostcodeDtos = new ArrayList<>();
			lsPostcodeDtos = entityConverter.convertEntityToDtoList(location.getLsPostcode(), lsPostcodeDtos,
					PostcodeDto.class);

			return lsPostcodeDtos;

		} catch (InvalidDataAccessApiUsageException | IllegalArgumentException argE) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_LOCATION_WITH_ID, locationId) + ", "
					+ SpMsg.getLbl(SpMsg.TXT_POSTCODE_WITH_ID, tmpPostcodeId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			SpException spE = new SpException(ee.getCode(), e, ee.getMessage());
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	/**
	 * Method to persist a list of {@link LocationForBranch}
	 * 
	 * @param locationId
	 * @param lsPostcodeDtos
	 * @return
	 * @throws HttpStatusException
	 */
	@SuppressWarnings("unchecked")
	public List<PostcodeDto> deletePostcodeForLocation(Long locationId, List<PostcodeDto> lsPostcodeDtos)
			throws SpException {

		Long tmpPostcodeId = null;
		try {
			Location location = locationRepository.findOne(locationId);

			for (PostcodeDto dto : lsPostcodeDtos) {
				tmpPostcodeId = dto.getId();

				if (locationRepository.exists(locationId) && postcodeRepository.exists(tmpPostcodeId)) {

					Postcode postcode = postcodeRepository.findOne(tmpPostcodeId);

					if (location.getLsPostcode().contains(postcode)) {
						location.getLsPostcode().remove(postcode);
						locationRepository.save(location);
					}

					if (postcode.getLsLocation().contains(location)) {
						postcode.getLsLocation().remove(location);
						postcodeRepository.save(postcode);
					}

				} else {
					throw new IllegalArgumentException();
				}
			}

			lsPostcodeDtos = new ArrayList<>();
			lsPostcodeDtos = entityConverter.convertEntityToDtoList(location.getLsPostcode(), lsPostcodeDtos,
					PostcodeDto.class);

			return lsPostcodeDtos;
		} catch (InvalidDataAccessApiUsageException | IllegalArgumentException argE) {
			SpErrorEntry ee = SpExceptionEnum.UNKNOWN_ENTITY_EXCEPTION.getEntry();
			String msgParam = SpMsg.getLbl(SpMsg.TXT_LOCATION_WITH_ID, locationId) + ", "
					+ SpMsg.getLbl(SpMsg.TXT_POSTCODE_WITH_ID, tmpPostcodeId);
			SpException spE = new SpException(ee.getCode(), null, ee.getMessage(), msgParam);
			LOGGER.error(spE, spE);
			throw spE;
		} catch (Exception e) {
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			SpException spE = new SpException(ee.getCode(), e, ee.getMessage());
			LOGGER.error(spE, spE);
			throw spE;
		}
	}

	public List<LabelValueDto> getLocationsSearchList() throws SpException {
		List<LabelValueDto> list = new ArrayList<>();

		List<LocationDto> locationList = getLocations();

		for (LocationDto locationDto : locationList) {

			Long value = locationDto.getId();
			String label = getLocationString(locationDto);
			LabelValueDto dto = new LabelValueDto();

			dto.setLabel(label);
			dto.setValue(value);
			dto.setLsLocationForBranches(locationDto.getLsLocationForBranches());
			list.add(dto);
		}

		Collections.sort(list, (o1, o2) -> o1.getLabel().compareTo(o2.getLabel()));
		return list;
	}

	public String getLocationString(LocationDto locationDto) {

		StringBuffer sb = new StringBuffer();

		String label = locationDto.getTitle();
		if (label != null && !label.equals("")) {
			sb.append(label);
			sb.append(", ");
		}

		label = locationDto.getCommunity();
		if (label != null && !label.equals("")) {
			sb.append(label);
			sb.append(", ");
		}

		label = locationDto.getDistrict();
		if (label != null && !label.equals("")) {
			sb.append(label);
			sb.append(", ");
		}

		List<PostcodeDto> lsPostcode = locationDto.getLsPostcode();
		for (int i = 0; i < lsPostcode.size(); i++) {
			PostcodeDto postcode = lsPostcode.get(i);
			sb.append(postcode.getPcode());
			if (i < lsPostcode.size() - 1) {
				sb.append(", ");
			}
		}

		label = locationDto.getShorttext();
		if (label != null && !label.equals("")) {
			sb.append(", ");
			sb.append(label);
		}
		return sb.toString();
	}
}
