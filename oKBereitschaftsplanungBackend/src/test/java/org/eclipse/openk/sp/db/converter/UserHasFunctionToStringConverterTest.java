/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.converter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.openk.sp.db.model.UserFunction;
import org.eclipse.openk.sp.db.model.UserHasUserFunction;
import org.junit.Test;

public class UserHasFunctionToStringConverterTest {

	@Test
	public void convertTest() {
		Object destination = null;
		Class destClass = null;
		Class sourceClass = null;
		
		UserHasUserFunction uhuf1 = new UserHasUserFunction();
		UserHasUserFunction uhuf2 = new UserHasUserFunction();
		UserFunction uf1 = new UserFunction();
		uf1.setFunctionName("fkt1");
		UserFunction uf2 = new UserFunction();
		uf2.setFunctionName("fkt2");
		
		uhuf1.setUserFunction(uf1);
		uhuf2.setUserFunction(uf2);

		List<UserHasUserFunction> source = new ArrayList<UserHasUserFunction>();
		source.add(uhuf1);		
		source.add(uhuf2);

		UserHasFunctionToStringConverter uhfConv = new UserHasFunctionToStringConverter();
		Object obj = uhfConv.convert(destination, source, destClass, sourceClass);
		
		assertNotNull(obj);
		
		String source2 = "Einsatzleiter, Kranwagen, Polizei";
		obj = uhfConv.convert(destination, source2, destClass, sourceClass);
		
		assertNotNull(obj);
		
		Long source3 = new Long(2);
		obj = uhfConv.convert(destination, source3, destClass, sourceClass);
		
		assertNull(obj);
	}
	
	@Test
	public void UhfListToString() {

		UserHasFunctionToStringConverter uhfConv = new UserHasFunctionToStringConverter();
		
		UserHasUserFunction uhuf1 = new UserHasUserFunction();
		UserHasUserFunction uhuf2 = new UserHasUserFunction();
		UserFunction uf1 = new UserFunction();
		uf1.setFunctionName("fkt1");
		UserFunction uf2 = new UserFunction();
		uf2.setFunctionName("fkt2");
		
		uhuf1.setUserFunction(uf1);
		uhuf2.setUserFunction(uf2);

		List<UserHasUserFunction> source = new ArrayList<UserHasUserFunction>();
		source.add(uhuf1);		
		source.add(uhuf2);

		String str = uhfConv.uhfListToString(source);
		assertNotNull(str);

	}

	@Test
	public void StringToUHF() {

		UserHasFunctionToStringConverter uhfConv = new UserHasFunctionToStringConverter();

		String source = "Einsatzleiter, Kranwagen, Polizei";

		List<UserHasUserFunction> lsUserHasUserFunctions = uhfConv.stringToUHF(source);

		assertNotNull(lsUserHasUserFunctions);
		assertEquals(3, lsUserHasUserFunctions.size());
		
		String source2 = "Einsatzleiter";
		lsUserHasUserFunctions = uhfConv.stringToUHF(source2);

		assertNotNull(lsUserHasUserFunctions);
		assertEquals(1, lsUserHasUserFunctions.size());
	}
}
