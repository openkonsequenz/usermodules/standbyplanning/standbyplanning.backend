/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.util.Date;

import org.junit.Test;

public class UserInRegionTest {

	@Test
	public void testGettersAndSetters() {
		Date d = new Date();
		Region region = new Region();
		User user = new User();
		UserInRegion uir = new UserInRegion();
		
		uir.setId(1l);
		assertEquals(1l, uir.getId().longValue());
		
		uir.setRegion(region);
		assertEquals(region, uir.getRegion());
		
		uir.setUser(user);
		assertEquals(user, uir.getUser());

	}
	
	
	
	@Test
	public void testCompare() throws MalformedURLException {
		UserInRegion obj1 = new UserInRegion();
		obj1.setId(5L);

		UserInRegion obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		UserInRegion obj3 = new UserInRegion();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}
}
