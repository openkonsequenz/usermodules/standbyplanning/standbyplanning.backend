/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.exceptions;

import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.core.viewmodel.ErrorReturn;

public class SpNestedException extends HttpStatusException {
	private static final long serialVersionUID = 6771249960550163561L;
	private final ErrorReturn errorReturn;

	public SpNestedException(ErrorReturn er) {
		super(er.getErrorCode());
		errorReturn = er;
	}

	public SpNestedException(ErrorReturn er, Throwable t) {
		super(er.getErrorCode(), "Error message", t);
		errorReturn = er;
	}

	@Override
	public int getHttpStatus() {
		return errorReturn.getErrorCode();
	}

	@Override
	public String getMessage() {
		return errorReturn.getErrorText();
	}
}