/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "USER" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "UserDto")
@JsonInclude(Include.NON_NULL)
public class UserSelectionDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private String firstname;
	private String lastname;
	private String hrNumber;
	private String userKey;
	private Boolean isCompany;
	private String notes;
	private Date validFrom;
	private Date validTo;
	private Date modificationDate;
	private String userRole;
	private String userRegionStr;
	private String userHasUserFunctionStr;
	private String organisationName;
	private AddressDto privateAddress;
	private ContactDataDto businessContactData;
	private ContactDataDto privateContactData;
	private OrganisationDto organisation;

	public UserSelectionDto() {
		/** default constructor. */
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @return the isCompany
	 */
	public Boolean getIsCompany() {
		return isCompany;
	}

	/**
	 * @param isCompany the isCompany to set
	 */
	public void setIsCompany(Boolean isCompany) {
		this.isCompany = isCompany;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the userRole
	 */
	public String getUserRole() {
		return userRole;
	}

	/**
	 * @param userRole the userRole to set
	 */
	public void setUserRole(String userRole) {
		this.userRole = userRole;
	}

	/**
	 * @return the userRegionStr
	 */
	public String getUserRegionStr() {
		return userRegionStr;
	}

	/**
	 * @param userRegionStr the userRegionStr to set
	 */
	public void setUserRegionStr(String userRegionStr) {
		this.userRegionStr = userRegionStr;
	}

	/**
	 * @return the userHasUserFunctionStr
	 */
	public String getUserHasUserFunctionStr() {
		return userHasUserFunctionStr;
	}

	/**
	 * @param userHasUserFunctionStr the userHasUserFunctionStr to set
	 */
	public void setUserHasUserFunctionStr(String userHasUserFunctionStr) {
		this.userHasUserFunctionStr = userHasUserFunctionStr;
	}

	/**
	 * @return the organisationName
	 */
	public String getOrganisationName() {
		return organisationName;
	}

	/**
	 * @param organisationName the organisationName to set
	 */
	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	public String getHrNumber() {
		return hrNumber;
	}

	public void setHrNumber(String hrNumber) {
		this.hrNumber = hrNumber;
	}

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}

	public AddressDto getPrivateAddress() {
		return privateAddress;
	}

	public void setPrivateAddress(AddressDto privateAddress) {
		this.privateAddress = privateAddress;
	}

	public ContactDataDto getBusinessContactData() {
		return businessContactData;
	}

	public void setBusinessContactData(ContactDataDto businessContactData) {
		this.businessContactData = businessContactData;
	}

	public ContactDataDto getPrivateContactData() {
		return privateContactData;
	}

	public void setPrivateContactData(ContactDataDto privateContactData) {
		this.privateContactData = privateContactData;
	}

	/**
	 * @return the organisation
	 */
	public OrganisationDto getOrganisation() {
		return organisation;
	}

	/**
	 * @param organisation the organisation to set
	 */
	public void setOrganisation(OrganisationDto organisation) {
		this.organisation = organisation;
	}
}
