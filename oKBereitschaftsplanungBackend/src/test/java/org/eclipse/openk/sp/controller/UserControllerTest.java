/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.OrganisationRepository;
import org.eclipse.openk.sp.db.dao.RegionRepository;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.UserFunctionRepository;
import org.eclipse.openk.sp.db.dao.UserHasUserFunctionRepository;
import org.eclipse.openk.sp.db.dao.UserInRegionRepository;
import org.eclipse.openk.sp.db.dao.UserInStandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.UserRepository;
import org.eclipse.openk.sp.db.model.Organisation;
import org.eclipse.openk.sp.db.model.Region;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.db.model.UserFunction;
import org.eclipse.openk.sp.db.model.UserHasUserFunction;
import org.eclipse.openk.sp.db.model.UserInRegion;
import org.eclipse.openk.sp.db.model.UserInStandbyGroup;
import org.eclipse.openk.sp.dto.LabelValueDto;
import org.eclipse.openk.sp.dto.OrganisationDto;
import org.eclipse.openk.sp.dto.StandbyGroupDto;
import org.eclipse.openk.sp.dto.UserDto;
import org.eclipse.openk.sp.dto.UserHasUserFunctionDto;
import org.eclipse.openk.sp.dto.UserInRegionDto;
import org.eclipse.openk.sp.dto.UserSelectionDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.DateHelper;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.eclipse.openk.sp.util.GsonTypeHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.google.gson.Gson;

/** Class to handle user operations. */
@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {
	protected static final Logger logger = Logger.getLogger(UserController.class);

	@Mock
	UserRepository userRepository;

	@Mock
	UserInRegionRepository userInRegionRepository;
	@Mock
	UserInStandbyGroupRepository userInStandbyGroupRepository;

	@Mock
	UserFunctionRepository userFunctionRepository;

	@Mock
	UserHasUserFunctionRepository uhufRepository;

	@Mock
	StandbyGroupRepository standbyGroupRepository;

	@Mock
	RegionRepository regionRepository;

	@Mock
	EntityConverter entityConverter;

	@Mock
	OrganisationRepository organisationRepository;

	@Mock
	StandbyGroupController standbyGroupController;

	@InjectMocks
	UserController userController;

	@SuppressWarnings("unchecked")
	@Test
	public void getUsersSelectionTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUserList.json");
		List<User> lsUserMock = (List<User>) new Gson().fromJson(json, GsonTypeHelper.JSON_TYPE_USER_ARRAY_LIST);

		json = fh.loadStringFromResource("testUser.json");
		UserSelectionDto userDtoMock = (UserSelectionDto) new Gson().fromJson(json, UserSelectionDto.class);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) userDtoMock);
		when(userRepository.findAllByOrderByLastnameAsc()).thenReturn(lsUserMock);

		// run test
		List<UserSelectionDto> lsOutputUserDto = null;
		try {
			lsOutputUserDto = userController.getUsersSelection();
		} catch (Exception e) {
			assertNull(e);
		}
		assertNotNull(lsOutputUserDto);
		assertTrue(lsOutputUserDto.size() > 0);
		assertTrue(lsOutputUserDto.get(0) instanceof UserSelectionDto);

		Mockito.doReturn(null).when(userRepository).findAllByOrderByLastnameAsc();
		try {
			lsOutputUserDto = userController.getUsersSelection();
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getUsersTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUserList.json");
		List<User> lsUserMock = (List<User>) new Gson().fromJson(json, GsonTypeHelper.JSON_TYPE_USER_ARRAY_LIST);

		json = fh.loadStringFromResource("testUser.json");
		UserDto userDtoMock = (UserDto) new Gson().fromJson(json, UserDto.class);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) userDtoMock);
		when(userRepository.findAllByOrderByLastnameAsc()).thenReturn(lsUserMock);

		// run test
		List<UserDto> lsOutputUserDto = null;
		try {
			lsOutputUserDto = userController.getUsers();
		} catch (SpException e) {
			assertNull(e);
		}
		assertNotNull(lsOutputUserDto);
		assertTrue(lsOutputUserDto.size() > 0);
		assertTrue(lsOutputUserDto.get(0) instanceof UserDto);

		// negative test
		Mockito.doReturn(null).when(userRepository).findAllByOrderByLastnameAsc();
		try {
			lsOutputUserDto = userController.getUsers();
		} catch (SpException e) {
			assertNotNull(e);
		}

	}

	@Test
	public void getUserTest() {
		// create test data
		long id = 5;
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testUser.json");
		User userMock = (User) new Gson().fromJson(json, User.class);
		userMock.setId(id);
		UserDto userDtoMock = (UserDto) new Gson().fromJson(json, UserDto.class);
		userDtoMock.setId(id);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) userDtoMock);
		when(userRepository.findOne(id)).thenReturn(userMock);
		when(userRepository.exists(id)).thenReturn(true);

		// run test
		try {
			UserDto userDto = userController.getUser(id);
			assertNotNull(userDto);
			assertTrue(userDto.getId() == id);
		} catch (Exception e) {
			assertNull(e);
		}

		// negativ test
		when(userRepository.exists(id)).thenReturn(false);
		try {
			UserDto userDto = userController.getUser(id);
			assertNull(userDto);
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	@Test
	public void saveUserErrorTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUser.json");
		UserDto userDto = (UserDto) new Gson().fromJson(json, UserDto.class);
		userDto.setId(1l);
		User user = (User) new Gson().fromJson(json, User.class);
		user.setId(1l);

		Throwable throwableClasses = new Exception("Error on save!");

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) user);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) userDto);

		// run test
		UserDto userResult = null;

		try {
			userResult = userController.saveUser(userDto);
			assertNotNull(userResult);
			assertEquals(1l, userResult.getId().longValue());
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	@Test
	public void saveUserTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUser.json");
		UserDto userDto = (UserDto) new Gson().fromJson(json, UserDto.class);
		userDto.setId(1l);
		User user = (User) new Gson().fromJson(json, User.class);
		user.setId(1l);

		Organisation organisation = user.getOrganisation();

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) user);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) userDto);
		when(userRepository.findOne(any())).thenReturn(user);
		when(userRepository.save(user)).thenReturn(user);

		when(organisationRepository.exists(any())).thenReturn(true);
		when(organisationRepository.findOne(any())).thenReturn(organisation);

		// run test
		UserDto userResult = null;

		try {
			userResult = userController.saveUser(userDto);
			assertNotNull(userResult);
			assertEquals(1l, userResult.getId().longValue());
		} catch (Exception e) {
			assertNull(e);
		}
		// run test2
		try {
			userDto.setId(null);
			userResult = userController.saveUser(userDto);
			assertNotNull(userResult);
		} catch (Exception e) {
			assertNull(e);
		}
	}

	@Test
	public void saveRegionsForUserTest() {
		// prepare data
		Long userId = 1l;

		UserInRegionDto userInRegionDto = new UserInRegionDto();
		userInRegionDto.setUserId(1l);
		userInRegionDto.setRegionId(1l);
		List<UserInRegionDto> lsUserInRegionDtos = new ArrayList<>();
		lsUserInRegionDtos.add(userInRegionDto);

		User user = new User();
		Region region = new Region();

		// define rules
		// when(repoHelper.exists(any(), any(RegionRepository.class))).thenReturn(true);
		// when(repoHelper.exists(any(), any(UserRepository.class))).thenReturn(true);
		when(regionRepository.exists(any())).thenReturn(true);
		when(userRepository.exists(any())).thenReturn(true);
		when(regionRepository.findOne(any())).thenReturn(region);
		when(userInRegionRepository.save(any(UserInRegion.class))).thenReturn(new UserInRegion());
		when(userRepository.findOne(any())).thenReturn(user);
		when(entityConverter.convertDtoToEntity(any(UserInRegionDto.class), any(UserInRegion.class)))
				.thenReturn(new UserInRegion());
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsUserInRegionDtos);

		// call method
		try {
			userController.saveRegionsForUser(userId, lsUserInRegionDtos);
		} catch (SpException e) {
			assertNull(e);
		}

		// prepare data for negative test
		// when(repoHelper.exists(any(),
		// any(RegionRepository.class))).thenReturn(false);
		// when(repoHelper.exists(any(), any(UserRepository.class))).thenReturn(true);
		when(regionRepository.exists(any())).thenReturn(false);
		when(userRepository.exists(any())).thenReturn(true);

		// call method
		try {
			userController.saveRegionsForUser(userId, lsUserInRegionDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// prepare data for negative test 2
		// when(repoHelper.exists(any(), any(RegionRepository.class))).thenReturn(true);
		// when(repoHelper.exists(any(), any(UserRepository.class))).thenReturn(false);
		when(regionRepository.exists(any())).thenReturn(true);
		when(userRepository.exists(any())).thenReturn(false);

		// call method
		try {
			userController.saveRegionsForUser(userId, lsUserInRegionDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// prepare data for negative test 3 - null user id
		when(userRepository.exists(any())).thenReturn(true);

		// call method
		try {
			userController.saveRegionsForUser(null, lsUserInRegionDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// prepare data for negative test 4 - null region id
		lsUserInRegionDtos.get(0).setId(null);
		try {
			userController.saveRegionsForUser(null, lsUserInRegionDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// prepare data for negative test 5 - null region id
		lsUserInRegionDtos.get(0).setId(null);
		try {
			userController.saveRegionsForUser(userId, null);
		} catch (SpException e) {
			assertNotNull(e);
		}

	}

	@Test
	public void deleteRegionsForUserTest() {
		// prepare data
		Long userId = 1l;
		User user = new User();
		Region region = new Region(1l);

		List<UserInRegionDto> lsUserInRegionDtos = new ArrayList<>();
		UserInRegionDto dto = new UserInRegionDto();
		dto.setId(1l);
		dto.setRegionId(1l);
		lsUserInRegionDtos.add(dto);

		UserInRegion userInRegion = new UserInRegion();
		userInRegion.setRegion(region);

		user.getLsUserInRegions().add(userInRegion);

		// define rules
		when(regionRepository.exists(any())).thenReturn(true);
		when(userRepository.exists(any())).thenReturn(true);
		when(userInRegionRepository.exists(any())).thenReturn(true);
		when(userRepository.findOne(any())).thenReturn(user);
		when(userRepository.save(any(User.class))).thenReturn(user);
		when(userInRegionRepository.findOne(any())).thenReturn(userInRegion);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsUserInRegionDtos);
		Mockito.doAnswer(new Answer() {
			public Object answer(InvocationOnMock invocation) {
				Object[] args = invocation.getArguments();
				return null;
			}
		}).when(userInRegionRepository).delete(userInRegion);

		// call method
		try {
			userController.deleteRegionsForUser(userId, lsUserInRegionDtos);
		} catch (SpException e) {
			assertNull(e);
		}

		// define rules for else
		when(userRepository.exists(any())).thenReturn(false);

		// call method
		try {
			userController.deleteRegionsForUser(userId, lsUserInRegionDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// define rules for else
		when(userRepository.exists(any())).thenReturn(true);
		when(regionRepository.exists(any())).thenReturn(false);

		// call method
		try {
			userController.deleteRegionsForUser(userId, lsUserInRegionDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// call method
		try {
			userController.deleteRegionsForUser(userId, null);
		} catch (SpException e) {
			assertNotNull(e);
		}
	}

	@Test
	public void saveFunctionsForUserTest() {
		// prepare data
		Long userId = 1l;
		UserHasUserFunction uhuf = new UserHasUserFunction();
		UserHasUserFunctionDto uhufDto = new UserHasUserFunctionDto();
		List<UserHasUserFunctionDto> lsFunctionDtos = new ArrayList<>();
		lsFunctionDtos.add(uhufDto);

		User user = new User();
		user.setLsUserHasUserFunction(new ArrayList<>());

		UserFunction uf = new UserFunction();
		uf.setLsUserHasUserFunction(new ArrayList<>());

		// define rules
		when(userRepository.findOne(any())).thenReturn(user);
		when(userRepository.exists(any())).thenReturn(true);
		when(userRepository.save((User) any())).thenReturn(user);
		when(userFunctionRepository.exists(any())).thenReturn(true);
		when(userFunctionRepository.findOne(any())).thenReturn(uf);
		when(userFunctionRepository.save((UserFunction) any())).thenReturn(uf);
		when(uhufRepository.save((UserHasUserFunction) any())).thenReturn(uhuf);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsFunctionDtos);

		// call method
		try {
			userController.saveFunctionsForUser(userId, lsFunctionDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// define rules for else
		when(userRepository.exists(any())).thenReturn(false);

		// call method
		try {
			userController.saveFunctionsForUser(userId, lsFunctionDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// define rules for else
		when(userRepository.exists(any())).thenReturn(true);
		when(userFunctionRepository.exists(any())).thenReturn(false);

		// call method
		try {
			userController.saveFunctionsForUser(userId, lsFunctionDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// call method
		try {
			userController.saveFunctionsForUser(userId, null);
		} catch (SpException e) {
			assertNotNull(e);
		}

	}

	@Test
	public void deleteFunctionsForUserTest() {
		// prepare data
		Long userId = 1l;
		UserHasUserFunction uhuf = new UserHasUserFunction();
		UserHasUserFunctionDto uhufDto = new UserHasUserFunctionDto();
		List<UserHasUserFunctionDto> lsFunctionDtos = new ArrayList<>();
		lsFunctionDtos.add(uhufDto);

		User user = new User();
		user.setLsUserHasUserFunction(new ArrayList<>());

		UserFunction uf = new UserFunction();
		uf.setLsUserHasUserFunction(new ArrayList<>());

		// define rules
		when(userRepository.findOne(any())).thenReturn(user);
		when(userRepository.exists(any())).thenReturn(true);
		when(userRepository.save((User) any())).thenReturn(user);
		when(userFunctionRepository.exists(any())).thenReturn(true);
		when(userFunctionRepository.findOne(any())).thenReturn(uf);
		when(userFunctionRepository.save((UserFunction) any())).thenReturn(uf);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsFunctionDtos);
		when(uhufRepository.exists(any())).thenReturn(true);
		Mockito.doNothing().when(uhufRepository).delete(any(UserHasUserFunction.class));

		// call method
		try {
			userController.deleteFunctionsForUser(userId, lsFunctionDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// define rules for else
		when(userRepository.exists(any())).thenReturn(false);

		// call method
		try {
			userController.deleteFunctionsForUser(userId, lsFunctionDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// define rules for else
		when(userRepository.exists(any())).thenReturn(true);
		when(userFunctionRepository.exists(any())).thenReturn(false);

		// call method
		try {
			userController.deleteFunctionsForUser(userId, lsFunctionDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// define rules for else

		when(userFunctionRepository.exists(any())).thenReturn(true);
		when(uhufRepository.exists(any())).thenReturn(false);

		// call method
		try {
			userController.deleteFunctionsForUser(userId, lsFunctionDtos);
		} catch (SpException e) {
			assertNotNull(e);
		}

		// call method
		try {
			userController.deleteFunctionsForUser(userId, null);
		} catch (SpException e) {
			assertNotNull(e);
		}
	}

	@Test
	public void getFilteredUsersSelectionTest() {
		// prepare data
		Long standbyGroupId = 1l;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUserList.json");
		List<User> listUser = (List<User>) new Gson().fromJson(json, GsonTypeHelper.JSON_TYPE_USER_ARRAY_LIST);

		json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupDto standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);
		standbyGroupDto.setId(1l);

		// define rules
		when(standbyGroupController.getStandbyGroup(any())).thenReturn(standbyGroupDto);
		when(userRepository.findAllValid(any())).thenReturn(listUser);

		List<UserSelectionDto> listResult = userController.getFilteredUsersSelection(standbyGroupId);
		assertNotNull(listResult);

		standbyGroupDto.setLsRegions(null);

		listResult = userController.getFilteredUsersSelection(standbyGroupId);
		assertNotNull(listResult);

		standbyGroupDto.setLsUserFunction(null);

		listResult = userController.getFilteredUsersSelection(standbyGroupId);
		assertNotNull(listResult);
		assertEquals(2, listResult.size());

		standbyGroupDto = (StandbyGroupDto) new Gson().fromJson(json, StandbyGroupDto.class);
		standbyGroupDto.setId(1l);
		standbyGroupDto.setLsUserFunction(null);

		// define rules
		when(standbyGroupController.getStandbyGroup(any())).thenReturn(standbyGroupDto);

		listResult = userController.getFilteredUsersSelection(standbyGroupId);
		assertNotNull(listResult);

	}

	@Test(expected = SpException.class)
	public void isUserValidTest() throws SpException {
		// prepare data
		Long userId = 1l;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUser.json");
		User user = (User) new Gson().fromJson(json, User.class);

		// define rules
		when(userRepository.findOne(any())).thenReturn(user);

		Date validForm = DateHelper.getDate(2018, 10, 10);
		Date validTo = DateHelper.getDate(2018, 10, 12);
		boolean result = userController.isUserValid(userId, validForm, validTo);
		assertTrue(result);

		validForm = DateHelper.getDate(2018, 6, 10);
		validTo = DateHelper.getDate(2018, 6, 12);
		result = userController.isUserValid(userId, validForm, validTo);
		assertFalse(result);

		when(userRepository.findOne(any())).thenReturn(null);
		result = userController.isUserValid(userId, validForm, validTo);

	}

	@Test(expected = SpException.class)
	public void isUserValidErrorTest() throws SpException {
		// prepare data
		Long userId = 1l;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUser.json");
		User user = (User) new Gson().fromJson(json, User.class);

		// define rules
		when(userRepository.findOne(any())).thenThrow(new IllegalArgumentException("test"));

		Date validForm = DateHelper.getDate(2018, 10, 10);
		Date validTo = DateHelper.getDate(2018, 10, 12);

		userController.isUserValid(userId, validForm, validTo);

	}

	@Test
	public void isUserInGroupTest() {
		// prepare data
		Long userId = 1l;
		Long groupId = 1l;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUser.json");
		User user = (User) new Gson().fromJson(json, User.class);

		// define rules
		when(userRepository.findOne(any())).thenReturn(user);
		List<UserInStandbyGroup> list = new ArrayList<>();
		UserInStandbyGroup uisg = new UserInStandbyGroup();
		list.add(uisg);

		when(userInStandbyGroupRepository.findUserForInterval(Mockito.anyLong(), Mockito.anyLong(), Mockito.any(),
				Mockito.any())).thenReturn(list);

		Date validForm = DateHelper.getDate(2018, 10, 10);
		Date validTo = DateHelper.getDate(2018, 10, 12);
		boolean result = userController.isUserInGroup(userId, groupId, validForm, validTo);
		assertTrue(result);

		list = new ArrayList<>();
		when(userInStandbyGroupRepository.findUserForInterval(Mockito.anyLong(), Mockito.anyLong(), Mockito.any(),
				Mockito.any())).thenReturn(list);

		validForm = DateHelper.getDate(2019, 10, 10);
		validTo = DateHelper.getDate(2019, 10, 12);
		result = userController.isUserInGroup(userId, groupId, validForm, validTo);
		assertFalse(result);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void getUsersDropDownTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUserList.json");
		List<User> lsUserMock = (List<User>) new Gson().fromJson(json, GsonTypeHelper.JSON_TYPE_USER_ARRAY_LIST);

		json = fh.loadStringFromResource("testUser.json");
		UserDto userDtoMock = (UserDto) new Gson().fromJson(json, UserDto.class);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) userDtoMock);
		when(userRepository.findAllByOrderByLastnameAsc()).thenReturn(lsUserMock);

		// run test

		List<LabelValueDto> resultList = userController.getUsersDropDown();

		assertNotNull(resultList);

	}

	@Test
	public void getUserString() {
		// prepare data

		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testUser.json");
		UserDto userDto = (UserDto) new Gson().fromJson(json, UserDto.class);

		String result = userController.getUserString(userDto);
		assertNotNull(result);

		OrganisationDto organisation = userDto.getOrganisation();
		organisation.setOrgaName("");
		userDto.setLastname("");
		userDto.setFirstname("");
		result = userController.getUserString(userDto);
		assertNotNull(result);

		organisation.setOrgaName(null);
		userDto.setLastname(null);
		userDto.setFirstname(null);
		result = userController.getUserString(userDto);
		assertNotNull(result);

		userDto.setOrganisation(null);
		result = userController.getUserString(userDto);
		assertNotNull(result);
	}
}
