/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "STANDBY_SCHEDULE_FILTER" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "StandbyScheduleFilterDto")
@JsonInclude(Include.NON_NULL)
public class StandbyScheduleFilterDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Date validFrom;
	private Date validTo;
	private Long standbyListId;
	private String title;
	private String comment;
	private List<TransferGroupDto> lsTransferGroup = new ArrayList<>();

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	public StandbyScheduleFilterDto() {
		/** default constructor. */
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the standbyListId
	 */
	public Long getStandbyListId() {
		return standbyListId;
	}

	/**
	 * @param standbyListId the standbyListId to set
	 */
	public void setStandbyListId(Long standbyListId) {
		this.standbyListId = standbyListId;
	}

	public List<TransferGroupDto> getLsTransferGroup() {
		return lsTransferGroup;
	}

	public void setLsTransferGroup(List<TransferGroupDto> lsTransferGroup) {
		this.lsTransferGroup = lsTransferGroup;
	}

}
