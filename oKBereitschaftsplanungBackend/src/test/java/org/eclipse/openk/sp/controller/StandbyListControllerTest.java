/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.RegionRepository;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListHasStandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.eclipse.openk.sp.db.model.StandbyListHasStandbyGroup;
import org.eclipse.openk.sp.dto.StandbyListDto;
import org.eclipse.openk.sp.dto.StandbyListHasStandbyGroupDto;
import org.eclipse.openk.sp.dto.StandbyListSelectionDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.eclipse.openk.sp.util.GsonTypeHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

/** Class to handle standbyList operations. */
@RunWith(MockitoJUnitRunner.class)
public class StandbyListControllerTest {
	protected static final Logger logger = Logger.getLogger(StandbyListController.class);

	@Mock
	StandbyListRepository standbyListRepository;

	@Mock
	StandbyGroupRepository standbyGroupRepository;

	@Mock
	StandbyListHasStandbyGroupRepository standbyListHasStandbyGroupRepository;

	@Mock
	RegionRepository regionRepository;

	@Mock
	EntityConverter entityConverter;

	@InjectMocks
	StandbyListController standbyListController = new StandbyListController();

	@Test
	public void saveStandbyListErrorTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyList.json");
		StandbyListDto standbyListDto = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);
		standbyListDto.setId(1l);
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);
		standbyList.setId(1l);

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) standbyList);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) standbyListDto);

		// run test
		StandbyListDto standbyListResult = null;

		try {
			standbyListResult = standbyListController.saveStandbyList(standbyListDto);
			assertNotNull(standbyListResult);
			assertEquals(1l, standbyListResult.getId().longValue());
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	@Test
	public void saveStandbyListTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyList.json");
		StandbyListDto standbyListDto = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);
		standbyListDto.setId(1l);
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);
		standbyList.setId(1l);

		StandbyGroup standbyGroup = new StandbyGroup(1l);

		// List list = standbyList.getLsPostcode();
		// List listRegion = standbyList.getLsRegions();

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) standbyList);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) standbyListDto);

		when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(standbyList);
		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);

		when(standbyListRepository.save((StandbyList) any())).thenReturn(standbyList);

		// run test
		StandbyListDto standbyListResult = null;

		try {
			standbyListResult = standbyListController.saveStandbyList(standbyListDto);
			assertNotNull(standbyListResult);
			assertEquals(1l, standbyListResult.getId().longValue());
		} catch (Exception e) {
			assertNull(e);
		}

		// run test positive else
		when(standbyListRepository.exists(any())).thenReturn(true);
		when(standbyListRepository.findOne(any())).thenReturn(standbyList);
		try {
			standbyListResult = standbyListController.saveStandbyList(standbyListDto);
			assertNotNull(standbyListResult);
			assertEquals(1l, standbyListResult.getId().longValue());
		} catch (Exception e) {
			assertNull(e);
		}

		// run negative test
		when(standbyListRepository.save((StandbyList) any())).thenThrow(new IllegalArgumentException());
		try {
			standbyListResult = standbyListController.saveStandbyList(standbyListDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

	}

	@SuppressWarnings("unchecked")
	@Test
	public void getStandbyListsTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyListList.json");
		List<StandbyList> lsStandbyListMock = (List<StandbyList>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_STANDBYLIST_ARRAY_LIST);

		List<StandbyListDto> lsStandbyListDtoMock = (List<StandbyListDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_STANDBYLIST_ARRAY_LIST);

		json = fh.loadStringFromResource("testStandbyList.json");
		StandbyListDto standbyListDtoMock = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);

		// create when
		when(entityConverter.convertEntityToDtoList(any(), any(), any())).thenReturn(lsStandbyListDtoMock);
		when(standbyListRepository.findAllByOrderByTitleAsc()).thenReturn(lsStandbyListMock);

		// run test
		List<StandbyListDto> lsOutputUserDto = standbyListController.getStandbyLists();

		assertNotNull(lsOutputUserDto);
		assertTrue(lsOutputUserDto.size() > 0);

	}

	@SuppressWarnings("unchecked")
	@Test
	public void getStandbyListSelectionTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyListList.json");
		List<StandbyList> lsStandbyListMock = (List<StandbyList>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_STANDBYLIST_ARRAY_LIST);

		List<StandbyListSelectionDto> lsStandbyListDtoMock = (List<StandbyListSelectionDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_STANDBYLIST_DTO_ARRAY_LIST);

		json = fh.loadStringFromResource("testStandbyList.json");
		StandbyListSelectionDto standbyListDtoMock = (StandbyListSelectionDto) new Gson().fromJson(json,
				StandbyListSelectionDto.class);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) standbyListDtoMock);

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsStandbyListDtoMock);

		when(standbyListRepository.findAllByOrderByTitleAsc()).thenReturn(lsStandbyListMock);
		when(standbyListRepository.findAll()).thenReturn(lsStandbyListMock);

		// run test
		List<StandbyListSelectionDto> lsOutputUserDto = standbyListController.getStandbyListsSelection();

		assertNotNull(lsOutputUserDto);
		assertTrue(lsOutputUserDto.size() > 0);
	}

	@Test
	public void getStandbyList() {
		// create test data
		long id = 5;
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testStandbyList.json");
		StandbyList standbyListMock = (StandbyList) new Gson().fromJson(json, StandbyList.class);
		standbyListMock.setId(id);
		StandbyListDto standbyListDtoMock = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);
		standbyListDtoMock.setId(id);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) standbyListDtoMock);
		when(standbyListRepository.findOne(id)).thenReturn(standbyListMock);

		// run test
		StandbyListDto standbyListDto = standbyListController.getStandbyList(id);
		assertNotNull(standbyListDto);
		assertTrue(standbyListDto.getId() == id);
	}

	@Test
	public void getSavedStandbyListTest() throws SpException {
		Long id = 30L;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyList.json");
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);

		// create when

		when(standbyListRepository.findOne(standbyList.getId())).thenReturn(standbyList);

		// run test

		StandbyList result = standbyListController.getSavedStandbyList(6L);
		assertNull(result);
	}

	@Test
	public void getSavedStandbyListNullTest() throws SpException {

		// run test

		StandbyList result = standbyListController.getSavedStandbyList(null);
		assertNull(result);
	}

	@Test
	public void validateEntitysTest() {
		Long id = 30L;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyList.json");
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);
		List list = new ArrayList();
		list.add(standbyList);

		when(standbyListRepository.findOne(standbyList.getId())).thenReturn(standbyList);

		List resultllist = standbyListController.validateEntitys(list, standbyListRepository, logger);

		assertNotNull(resultllist);

	}

	@Test
	public void validateEntitysLogTest() {
		Long id = 30L;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyList.json");
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);
		List list = new ArrayList();
		list.add(standbyList);

		when(standbyListRepository.findOne(standbyList.getId())).thenReturn(standbyList);

		List resultllist = standbyListController.validateEntitys(list, standbyListRepository, logger);

		assertNotNull(resultllist);

	}

	@Test
	public void validateEntitysLog2Test() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyList.json");
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);

		standbyList.setId(null);

		List list = new ArrayList();
		list.add(standbyList);

		when(standbyListRepository.findOne(standbyList.getId())).thenReturn(standbyList);

		List resultllist = standbyListController.validateEntitys(list, standbyListRepository, logger);

		assertNotNull(resultllist);

	}

	@Test
	public void saveStandbyGroupsForStandbyListTest() throws SpException {
		Long id = 30L;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyList.json");

		StandbyListDto standbyListDto = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);

		List<StandbyListHasStandbyGroupDto> lsStandbyListHasStandbyGroupDto = standbyListDto
				.getLsStandbyListHasStandbyGroup();
		StandbyGroup standbyGroup = new StandbyGroup(1L);

		Long standbyListId = 30L;

		StandbyListHasStandbyGroup listHasGroup = new StandbyListHasStandbyGroup();
		listHasGroup.setId(1L);
		listHasGroup.setPosition(1);
		listHasGroup.setStandbyGroup(standbyGroup);
		listHasGroup.setStandbyList(standbyList);

		when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(standbyList);
		when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);

		when(standbyListHasStandbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyListHasStandbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(listHasGroup);

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsStandbyListHasStandbyGroupDto);

		when(entityConverter.convertDtoToEntity(any(), (StandbyListHasStandbyGroup) any())).thenReturn(listHasGroup);

		// when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity)
		// any()))
		// .thenReturn(standbyGroup.getLsUserInStandbyGroups().get(0));

		List resultllist = standbyListController.saveStandbyGroupsForStandbyList(standbyListId,
				lsStandbyListHasStandbyGroupDto);

		assertEquals(5, resultllist.size());

	}

	@Test
	public void saveStandbyGroupsForStandbyList2Test() throws SpException {
		Long id = 30L;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyList.json");

		StandbyListDto standbyListDto = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);

		List<StandbyListHasStandbyGroupDto> lsStandbyListHasStandbyGroupDto = standbyListDto
				.getLsStandbyListHasStandbyGroup();
		StandbyGroup standbyGroup = new StandbyGroup(1L);

		Long standbyListId = 30L;

		StandbyListHasStandbyGroup listHasGroup = new StandbyListHasStandbyGroup();
		listHasGroup.setId(1L);
		listHasGroup.setPosition(1);
		listHasGroup.setStandbyGroup(standbyGroup);
		listHasGroup.setStandbyList(standbyList);

		when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(standbyList);
		when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);

		when(standbyListHasStandbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		when(standbyListHasStandbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(listHasGroup);

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsStandbyListHasStandbyGroupDto);

		when(entityConverter.convertDtoToEntity(any(), (StandbyListHasStandbyGroup) any())).thenReturn(listHasGroup);

		// when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity)
		// any()))
		// .thenReturn(standbyGroup.getLsUserInStandbyGroups().get(0));

		List resultllist = standbyListController.saveStandbyGroupsForStandbyList(standbyListId,
				lsStandbyListHasStandbyGroupDto);

		assertEquals(5, resultllist.size());

	}

	@Test
	public void pushPositionHigherTest() throws SpException {
		Long id = 30L;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyList.json");

		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);

		List<StandbyListHasStandbyGroup> lsStandbyListHasStandbyGroup = standbyList.getLsStandbyListHasStandbyGroup();

		when(standbyListHasStandbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		when(standbyListHasStandbyGroupRepository.findListMemberWithMinPosition(Mockito.anyLong(), Mockito.anyInt()))
				.thenReturn(lsStandbyListHasStandbyGroup);

		Throwable th = null;
		try {
			standbyListController.pushPositionHigher(id, 0);
		} catch (Exception e) {
			th = e;
		}

		assertEquals(null, th);

	}

	@Test(expected = SpException.class)
	public void saveStandbyGroupsForStandbyListErrorTest() throws SpException {
		Long id = 30L;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyList.json");
		StandbyListDto standbyListDto = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);

		List<StandbyListHasStandbyGroupDto> lsStandbyListHasStandbyGroupDto = standbyListDto
				.getLsStandbyListHasStandbyGroup();

		StandbyGroup standbyGroup = standbyList.getLsStandbyGroups().get(0);

		Long standbyListId = 30L;

		when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(standbyList);
		when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(false);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsStandbyListHasStandbyGroupDto);
		// when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity)
		// any()))
		// .thenReturn(standbyGroup.getLsUserInStandbyGroups().get(0));

		List resultllist = standbyListController.saveStandbyGroupsForStandbyList(standbyListId,
				lsStandbyListHasStandbyGroupDto);

		assertEquals(1, resultllist.size());

	}

	@Test
	public void deleteStandbyGroupsForStandbyListTest() throws SpException {
		Long id = 30L;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyList.json");
		StandbyListDto standbyListDto = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);

		List<StandbyListHasStandbyGroupDto> lsStandbyListHasStandbyGroupDto = standbyListDto
				.getLsStandbyListHasStandbyGroup();

		StandbyGroup standbyGroup = new StandbyGroup(1L);

		StandbyListHasStandbyGroup listHasGroup = new StandbyListHasStandbyGroup();
		listHasGroup.setId(1L);
		listHasGroup.setPosition(1);
		listHasGroup.setStandbyGroup(standbyGroup);
		listHasGroup.setStandbyList(standbyList);

		Long standbyListId = 30L;

		when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(standbyList);
		when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);

		when(standbyListHasStandbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		when(standbyListHasStandbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(listHasGroup);

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsStandbyListHasStandbyGroupDto);

//		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
//				.thenReturn(lsStandbyListHasStandbyGroupDto);
		// when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity)
		// any()))
		// .thenReturn(standbyGroup.getLsUserInStandbyGroups().get(0));

		List resultllist = standbyListController.deleteStandbyGroupsForStandbyList(standbyListId,
				lsStandbyListHasStandbyGroupDto);

		assertEquals(5, resultllist.size());

	}

	@Test(expected = SpException.class)
	public void deleteStandbyGroupsForStandbyListErrorTest() throws SpException {
		Long id = 30L;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyList.json");
		StandbyListDto standbyListDto = (StandbyListDto) new Gson().fromJson(json, StandbyListDto.class);
		StandbyList standbyList = (StandbyList) new Gson().fromJson(json, StandbyList.class);

		List<StandbyListHasStandbyGroupDto> lsStandbyListHasStandbyGroupDto = standbyListDto
				.getLsStandbyListHasStandbyGroup();

		StandbyGroup standbyGroup = standbyList.getLsStandbyGroups().get(0);

		Long standbyListId = 30L;

		when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(standbyList);
		when(standbyListRepository.exists(Mockito.anyLong())).thenReturn(false);
		when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);
		when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);

		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(lsStandbyListHasStandbyGroupDto);
		// when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity)
		// any()))
		// .thenReturn(standbyGroup.getLsUserInStandbyGroups().get(0));

		List resultllist = standbyListController.deleteStandbyGroupsForStandbyList(standbyListId,
				lsStandbyListHasStandbyGroupDto);

		assertEquals(1, resultllist.size());

	}
}
