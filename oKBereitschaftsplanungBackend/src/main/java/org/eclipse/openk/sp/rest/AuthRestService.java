/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.resources.BaseResource;
import org.eclipse.openk.sp.controller.external.ExternalInterfaceMapper;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("auth")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AuthRestService extends BaseResource {

	@Autowired
	ExternalInterfaceMapper externalInterfaceMapper;

	public AuthRestService() {
		super(Logger.getLogger(AuthRestService.class.getName()), new FileHelper());
	}

	@ApiOperation(value = "Login", notes = "This services validates the given token over the portal app.")
	@GET
	@Path("/login")
	@Produces("application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
	public Response login(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {
		logger.info("login - start");
		try { // NOSONAR

			return externalInterfaceMapper.authLogin(token);

		} catch (Exception e) {
			logger.error("Exception on Login!!");
			return externalInterfaceMapper.responseFromException(e);
		}
	}

	@ApiOperation(value = "Logout", notes = "This services kills the server session belonging to the the given session token.")
	@GET
	@Path("/logout")
	@Produces("application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
	public Response logout(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String token) {

		try (AutoCloseable ignored = perform("logout()")) { // NOSONAR

			return externalInterfaceMapper.authLogout(token);

		} catch (Exception e) {
			return externalInterfaceMapper.responseFromException(e);
		}
	}

}
