/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.controller.planning.PlannedDataController;
import org.eclipse.openk.sp.controller.reports.ReportController;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySelectionDto;
import org.eclipse.openk.sp.dto.StandbyScheduleDto;
import org.eclipse.openk.sp.dto.planning.PlanRowsDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.eclipse.openk.sp.dto.report.ReportDto;
import org.eclipse.openk.sp.dto.report.ReportGroupDto;
import org.eclipse.openk.sp.dto.report.ReportInputDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportGroupNameDtoConverter {

	public static final int MAX_ROWS = 60;

	@Autowired
	StandbyGroupRepository standbyGroupRepository;

	@Autowired
	StandbyListRepository standbyListRepository;

	@Autowired
	private PlannedDataController plannedDataController;

	@Autowired
	private EntityConverter entityConverter;

	protected static final Logger LOGGER = Logger.getLogger(ReportGroupNameDtoConverter.class);

	public List<ReportInputDto> get60Rows(ReportDto reportDto) throws SpException {

		List<ReportInputDto> inputDto = new ArrayList<>();

		StandbyScheduleFilterDto standbyScheduleFilterDto = new StandbyScheduleFilterDto();
		standbyScheduleFilterDto.setStandbyListId(reportDto.getStandByListId());
		standbyScheduleFilterDto.setValidFrom(reportDto.getValidFromDate());
		standbyScheduleFilterDto.setValidTo(reportDto.getValidToDate());
		boolean allDaySearch = true;

		Long statusId = reportDto.getStatusId();

		// get plan
		StandbyScheduleDto plan = plannedDataController.getFilteredPlanByStatus(standbyScheduleFilterDto, statusId,
				allDaySearch);

		// get first 10 groups with some data
		// empty groups are not represented
		Long[] groupIdArray = new Long[MAX_ROWS];
		int index = 0;

		List<StandbyGroupSelectionDto> groupList = plan.getPlanHeader().getListGroups();
		for (StandbyGroupSelectionDto standbyGroupSelectionDto : groupList) {
			Long groupId = standbyGroupSelectionDto.getId();
			// BP-832, empty groups are not filtered
			if (index < MAX_ROWS) {
				groupIdArray[index++] = groupId;
			}
		}

		Map<Long, StandbyScheduleBodySelectionDto> lastBodyMap = new HashMap<>();

		// convert to rows
		Date dateIndex = DateHelper.getStartOfDay(reportDto.getValidFromDate());

		while (DateHelper.isDateBefore(dateIndex, reportDto.getValidToDate())) {

			// fill groups from 1 to MAX_ROWS
			// only groups that are not empty in the interval of time
			addReportRow(reportDto, inputDto, plan, dateIndex, groupIdArray, lastBodyMap);

			dateIndex = DateHelper.addDaysToDate(dateIndex, 1);

		}

		return inputDto;
	}

	public ReportGroupDto addReportRow(ReportDto reportDto, List<ReportInputDto> inputDto, StandbyScheduleDto plan,
			Date dateIndex, Long[] groupIdArray, Map<Long, StandbyScheduleBodySelectionDto> lastBodyMap) {

		PlanRowsDto row = plan.getRow(dateIndex);

		if (row == null) {
			LOGGER.debug("no plan row for day found: " + dateIndex);
			return null;
		}

		ReportInputDto reportInputRow = new ReportInputDto();
		reportInputRow.setReportDto(reportDto);

		ReportGroupDto reportGroupDto = new ReportGroupDto();
		reportInputRow.setReportGroupDto(reportGroupDto);

		reportGroupDto.setFromDate(dateIndex);
		inputDto.add(reportInputRow);

		for (int i = 0; i < groupIdArray.length; i++) {
			Long groupId = groupIdArray[i];

			if (groupId != null) {

				List<StandbyScheduleBodySelectionDto> dayGroupList = row.getGroupList(groupId);

				if (dayGroupList != null) {

					try {
						if (checkEmpty(dayGroupList, groupId)) {

							StandbyScheduleBodySelectionDto tmpDto = dayGroupList.get(0);
							// empty entry

							reportGroupDto.setUserX(i + 1, "");
							reportGroupDto.setGroupX(i + 1, tmpDto.getStandbyGroup());

						} else {

							addOneUserString(i + 1, dayGroupList, reportGroupDto, reportDto, lastBodyMap);

						}
					} catch (Exception e) {
						LOGGER.info("Can't set Body in Group " + groupId + ". Error on User or Group!");
					}

				}
			}

		}

		return reportGroupDto;
	}

	public void addOneUserString(int i, List<StandbyScheduleBodySelectionDto> dayGroupList,
			ReportGroupDto reportGroupDto, ReportDto reportDto,
			Map<Long, StandbyScheduleBodySelectionDto> lastBodyMap) {

		StandbyScheduleBodySelectionDto lastBodyToday = null;
		boolean oneBodyPrinted = false;

		for (StandbyScheduleBodySelectionDto standbyScheduleBodySelectionDto : dayGroupList) {

			try {

				StandbyScheduleBodySelectionDto lastBody = lastBodyMap
						.get(standbyScheduleBodySelectionDto.getStandbyGroup().getId());

				Date endIndex = standbyScheduleBodySelectionDto.getValidTo();
				Date endDay = DateHelper.getStartOfDay(endIndex);

				// get first 00:00:00 ending body
				if (lastBodyToday == null && endIndex.equals(endDay)) {
					lastBodyToday = standbyScheduleBodySelectionDto;
				}

				Date startIndex = standbyScheduleBodySelectionDto.getValidFrom();
				Date startDay = DateHelper.getStartOfDay(startIndex);

				if (!oneBodyPrinted) {

					// case 00:00:00 check duration-user
					if (startIndex.equals(startDay)) {

						if (lastBody != null && lastBody.getUser().getId()
								.longValue() == standbyScheduleBodySelectionDto.getUser().getId().longValue()
								&& dayGroupList.size() > 1) {
							// nothing to do
						} else {

							// set first entry on this day
							oneBodyPrinted = addUserString(i, standbyScheduleBodySelectionDto, reportGroupDto,
									reportDto, lastBodyMap, dayGroupList);
						}

					} else {

						// case later then 00:00:00
						oneBodyPrinted = addUserString(i, standbyScheduleBodySelectionDto, reportGroupDto, reportDto,
								lastBodyMap, dayGroupList);

					}

				}

			} catch (Exception e) {
				LOGGER.info("Can't set ScheduleBody " + standbyScheduleBodySelectionDto.getId()
						+ ". Error on User or Group!");
			}

		}

		// remember first 00:00:00 ending body
		lastBodyMap.put(lastBodyToday.getStandbyGroup().getId(), lastBodyToday);

	}

	public boolean checkEmpty(List<StandbyScheduleBodySelectionDto> dayGroupList, Long groupId) {
		boolean isEmpty = false;

		if (dayGroupList.isEmpty()) {
			// add empty placeholder
			StandbyScheduleBodySelectionDto placeHoder = new StandbyScheduleBodySelectionDto();
			dayGroupList.add(placeHoder);
			StandbyGroup standbyGroup = standbyGroupRepository.findOne(groupId);

			StandbyGroupSelectionDto dto = (StandbyGroupSelectionDto) entityConverter.convertEntityToDto(standbyGroup,
					new StandbyGroupSelectionDto());
			placeHoder.setStandbyGroup(dto);
			isEmpty = true;
		}
		return isEmpty;
	}

	public boolean addUserString(int i, StandbyScheduleBodySelectionDto standbyScheduleBodySelectionDto,
			ReportGroupDto reportGroupDto, ReportDto reportDto, Map<Long, StandbyScheduleBodySelectionDto> lastBodyMap,
			List<StandbyScheduleBodySelectionDto> dayGroupList) throws SpException {

		StandbyScheduleBodySelectionDto lastBody = lastBodyMap
				.get(standbyScheduleBodySelectionDto.getStandbyGroup().getId());

		String userString = userString(lastBody, standbyScheduleBodySelectionDto, reportDto, dayGroupList.size());

		String str = reportGroupDto.getUserX(i);
		str = ((str == null || str.equals("")) ? "" : (str + "\n")) + userString;
		str = str.trim();

		reportGroupDto.setUserX(i, str);
		reportGroupDto.setGroupX(i, standbyScheduleBodySelectionDto.getStandbyGroup());

		return true;
	}

	public String userString(StandbyScheduleBodySelectionDto lastBody, StandbyScheduleBodySelectionDto body,
			ReportDto reportDto, int numberOfEntries) {

		String userString = "";

		if (body.getUser() != null && reportDto != null) {

			Long lastBodyId = (lastBody != null) ? lastBody.getUser().getId().longValue() : null;
			long bodyId = body.getUser().getId().longValue();

			String reportName = reportDto.getReportName();
			userString = body.getUser().getFirstname() + " " + body.getUser().getLastname();

			if (reportName.equals(ReportController.REPORT_MAX10_GROUPS)) {

				userString += "\n" + body.getUser().getPrivateContactData().getPhone() + "\n"
						+ body.getUser().getBusinessContactData().getCellphone() + "\n";

				// print every body on this report
				lastBodyId = null;
			}

			Date dayStart = DateHelper.getStartOfDay(body.getValidFrom());

			if ((lastBodyId != null) && (lastBodyId.longValue() == bodyId)
					&& (body.getValidFrom().getTime() == dayStart.getTime() && (numberOfEntries > 1))) {
				userString = "";
			}

		}

		return userString;
	}

}
