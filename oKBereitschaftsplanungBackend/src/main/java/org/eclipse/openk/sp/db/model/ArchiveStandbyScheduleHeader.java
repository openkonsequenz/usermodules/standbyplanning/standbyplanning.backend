/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.persistence.annotations.ConversionValue;
import org.eclipse.persistence.annotations.ObjectTypeConverter;

/**
 * The persistent class for the "STANDBY_SCHEDULE_HEADER" database table.
 */
@Entity
@Table(name = "ARCHIVE_HEADER")
@ObjectTypeConverter(name = "booleanConverter", dataType = java.lang.String.class, objectType = java.lang.Boolean.class, conversionValues = {
		@ConversionValue(dataValue = "Y", objectValue = "true"),
		@ConversionValue(dataValue = "N", objectValue = "false") })
public class ArchiveStandbyScheduleHeader extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ARCHIVE_HEADER_ID_SEQ")
	@SequenceGenerator(name = "ARCHIVE_HEADER_ID_SEQ", sequenceName = "ARCHIVE_HEADER_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "title", length = 2000, nullable = false)
	private String title;

	@Lob
	@Column(name = "annotation", nullable = true)
	private String comment;

	@Lob
	@Column(name = "json_plan", nullable = false)
	private String jsonPlan;

	@Column(name = "modification_date", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date modificationDate;

	@Column(name = "modification_by", nullable = false)
	private String modifiedBy;

	@Lob
	@Column(name = "modification_cause", nullable = false)
	private String modifiedCause;

	@Column(name = "valid_from", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date validFrom;

	@Column(name = "valid_to", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date validTo;

	@Column(name = "statusId", nullable = false)
	private Long statusId;

	@Column(name = "statusName", nullable = false)
	private String statusName;

	public ArchiveStandbyScheduleHeader() {
		/** default constructor. */
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the modifiedBy
	 */
	public String getModifiedBy() {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy
	 *            the modifiedBy to set
	 */
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the modifiedCause
	 */
	public String getModifiedCause() {
		return modifiedCause;
	}

	/**
	 * @param modifiedCause
	 *            the modifiedCause to set
	 */
	public void setModifiedCause(String modifiedCause) {
		this.modifiedCause = modifiedCause;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate
	 *            the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the jsonPlan
	 */
	public String getJsonPlan() {
		return jsonPlan;
	}

	/**
	 * @param jsonPlan
	 *            the jsonPlan to set
	 */
	public void setJsonPlan(String jsonPlan) {
		this.jsonPlan = jsonPlan;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom
	 *            the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo
	 *            the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the statusId
	 */
	public Long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId
	 *            the statusId to set
	 */
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the statusName
	 */
	public String getStatusName() {
		return statusName;
	}

	/**
	 * @param statusName
	 *            the statusName to set
	 */
	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}

}
