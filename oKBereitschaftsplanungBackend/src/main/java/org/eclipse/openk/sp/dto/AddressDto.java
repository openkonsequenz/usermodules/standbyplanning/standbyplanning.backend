/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "Address" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "AddressDto")
@JsonInclude(Include.NON_NULL)
public class AddressDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	@NotNull(message = "Postleitzahl ist nicht gesetzt")
	@Size(max = 8, message = "Postleitzahl mit max. 8 Zeichen")
	private String postcode;
	@NotNull(message = "Ort ist nicht gesetzt")
	@Size(max = 256, message = "Ort mit max. 256 Zeichen")
	private String community;
	@Size(max = 256, message = "Zusatz mit max. 256 Zeichen")
	private String communitySuffix;
	@NotNull(message = "Strasse ist nicht gesetzt")
	@Size(max = 256, message = "Strasse mit max. 256 Zeichen")
	private String street;
	@NotNull(message = "Hausnummer ist nicht gesetzt")
	@Size(max = 32, message = "Hausnummer mit max. 32 Zeichen")
	private String housenumber;
	@Size(max = 256, message = "Zone mit max. 256 Zeichen")
	private String wgs84zone;
	@Size(max = 32, message = "Koordinate(Breite) mit max. 32 Zeichen")
	private String latitude;
	@Size(max = 32, message = "Koordinate(Länge) mit max. 32 Zeichen")
	private String longitude;
	@Size(max = 256, message = "URL mit max. 256 Zeichen")
	private String urlMap;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the postcode
	 */
	public String getPostcode() {
		return postcode;
	}

	/**
	 * @param postcode the postcode to set
	 */
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	/**
	 * @return the community
	 */
	public String getCommunity() {
		return community;
	}

	/**
	 * @param community the community to set
	 */
	public void setCommunity(String community) {
		this.community = community;
	}

	/**
	 * @return the communitySuffix
	 */
	public String getCommunitySuffix() {
		return communitySuffix;
	}

	/**
	 * @param communitySuffix the communitySuffix to set
	 */
	public void setCommunitySuffix(String communitySuffix) {
		this.communitySuffix = communitySuffix;
	}

	/**
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * @param street the street to set
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * @return the housenumber
	 */
	public String getHousenumber() {
		return housenumber;
	}

	/**
	 * @param housenumber the housenumber to set
	 */
	public void setHousenumber(String housenumber) {
		this.housenumber = housenumber;
	}

	/**
	 * @return the wgs84zone
	 */
	public String getWgs84zone() {
		return wgs84zone;
	}

	/**
	 * @param wgs84zone the wgs84zone to set
	 */
	public void setWgs84zone(String wgs84zone) {
		this.wgs84zone = wgs84zone;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the urlMap
	 */
	public String getUrlMap() {
		return urlMap;
	}

	/**
	 * @param urlMap the urlMap to set
	 */
	public void setUrlMap(String urlMap) {
		this.urlMap = urlMap;
	}
}
