/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.util.Date;

import org.junit.Test;

public class ArchiveStandbyScheduleBodyTest {

	@Test
	public void ArchiveStandbyScheduleBodyGetterSetterTest() {

		Long id = new Long(3);
		Date d = new Date();
		String text = "test";
		User user = new User();
		StandbyStatus status = new StandbyStatus();
		StandbyGroup standbyGroup = new StandbyGroup();
		ArchiveStandbyScheduleHeader header = new ArchiveStandbyScheduleHeader();
		header.setId(id);

		StandbyScheduleBody body = new StandbyScheduleBody();

		ArchiveStandbyScheduleBody arcBody = new ArchiveStandbyScheduleBody();

		arcBody.setId(id);
		assertEquals(id, arcBody.getId());

		arcBody.setStandbyGroup(standbyGroup);
		assertEquals(standbyGroup, arcBody.getStandbyGroup());

		arcBody.setStatus(status);
		assertEquals(status, arcBody.getStatus());

		arcBody.setUser(user);
		assertEquals(user, arcBody.getUser());

		arcBody.setValidFrom(d);
		assertEquals(d, arcBody.getValidFrom());

		arcBody.setValidTo(d);
		assertEquals(d, arcBody.getValidTo());

		arcBody.setModificationDate(d);
		assertEquals(d, arcBody.getModificationDate());

		arcBody.setModifiedBy(text);
		assertEquals(text, arcBody.getModifiedBy());

		arcBody.setModifiedCause(text);
		assertEquals(text, arcBody.getModifiedCause());

		arcBody.setStandbyHeader(header);
		assertEquals(header, arcBody.getStandbyHeader());

		ArchiveStandbyScheduleBody arcBody2 = new ArchiveStandbyScheduleBody(header, body);
		assertEquals(header, arcBody2.getStandbyHeader());
	}

	@Test
	public void testCompare() throws MalformedURLException {
		ArchiveStandbyScheduleBody arcBody = new ArchiveStandbyScheduleBody();
		arcBody.setId(5L);

		ArchiveStandbyScheduleBody arcBody2 = arcBody;
		assertEquals(1, arcBody.compareTo(arcBody2));

		ArchiveStandbyScheduleBody arcBody3 = new ArchiveStandbyScheduleBody();
		arcBody3.setId(6L);
		assertEquals(0, arcBody.compareTo(arcBody3));

	}
}
