package org.eclipse.openk.sp.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "SpResponseDto" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "StandbyDurationResponseDto")
@JsonInclude(Include.NON_NULL)
public class StandbyDurationResponseDto extends AbstractDto {
	/***/
	private static final long serialVersionUID = 1L;
	private List<PlanningMsgDto> lsMsg = new ArrayList<>();
	private List<StandbyDurationDto> data = new ArrayList<>();

	/**
	 * @return the data
	 */
	public List<StandbyDurationDto> getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(List<StandbyDurationDto> data) {
		this.data = data;
	}

	/**
	 * @return the lsMsg
	 */
	public List<PlanningMsgDto> getLsMsg() {
		return lsMsg;
	}

	/**
	 * @param lsMsg
	 *            the lsMsg to set
	 */
	public void setLsMsg(List<PlanningMsgDto> lsMsg) {
		this.lsMsg = lsMsg;
	}
}
