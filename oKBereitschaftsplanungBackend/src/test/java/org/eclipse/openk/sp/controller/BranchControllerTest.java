/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.BranchRepository;
import org.eclipse.openk.sp.db.model.Branch;
import org.eclipse.openk.sp.dto.BranchDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/** Class to handle location operations. */
@RunWith(MockitoJUnitRunner.class)
public class BranchControllerTest {
	protected static final Logger logger = Logger.getLogger(BranchControllerTest.class);

	@Mock
	BranchRepository branchRepository;

	@Mock
	EntityConverter entityConverter;

	@InjectMocks
	BranchController branchController = new BranchController();

	@Test
	public void getLocationSelectionTest() {
		List<Branch> lsBranchMock = new ArrayList<>();
		lsBranchMock.add(new Branch(1l));
		lsBranchMock.add(new Branch(2l));

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) new BranchDto());
		when(branchRepository.findAllByOrderByTitleAsc()).thenReturn(lsBranchMock);

		// run test
		List<BranchDto> lsOutputBranchDto = null;
		try {
			lsOutputBranchDto = branchController.getBranchSelection();
		} catch (Exception e) {
			assertNull(e);
		}

		assertNotNull(lsOutputBranchDto);
		assertTrue(lsOutputBranchDto.size() > 0);
		assertTrue(lsOutputBranchDto.get(0) instanceof BranchDto);

		// run negative test
		when(branchRepository.findAllByOrderByTitleAsc()).thenReturn(null);
		try {
			lsOutputBranchDto = branchController.getBranchSelection();
		} catch (Exception e) {
			assertNotNull(e);
		}
	}
}
