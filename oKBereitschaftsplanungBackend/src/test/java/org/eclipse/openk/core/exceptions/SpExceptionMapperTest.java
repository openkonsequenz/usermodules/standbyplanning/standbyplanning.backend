/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.core.exceptions;



import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import org.apache.http.HttpStatus;
import org.eclipse.openk.common.JsonGeneratorBase;
import org.eclipse.openk.core.viewmodel.ErrorReturn;
import org.eclipse.openk.sp.exceptions.SpExceptionMapper;
import org.junit.Test;

public class SpExceptionMapperTest {
    @Test
    public void testToJson() {
        String json = SpExceptionMapper.toJson(new HttpStatusException(HttpStatus.SC_NOT_FOUND, "lalilu"));

        ErrorReturn er = JsonGeneratorBase.getGson().fromJson(json, ErrorReturn.class);
        assertEquals(404, er.getErrorCode());
        assertTrue(er.getErrorText().equals("lalilu"));
    }

    @Test
    public void testUnknownErrorToJson() {
        String json = SpExceptionMapper.unknownErrorToJson();

        ErrorReturn er = JsonGeneratorBase.getGson().fromJson(json, ErrorReturn.class);
        assertEquals(500, er.getErrorCode());
    }

    @Test
    public void testGeneralOKJson() {
        String ok = SpExceptionMapper.getGeneralOKJson();
        assertTrue("{\"ret\":\"OK\"}".equals(ok));
    }

    @Test
    public void testGeneralErrorJson() {
        String nok = SpExceptionMapper.getGeneralErrorJson();
        assertTrue("{\"ret\":\"NOK\"}".equals(nok));
    }
    
	
	@Test
	public void testConstructorIsPrivate() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
	  Constructor<SpExceptionMapper> constructor = SpExceptionMapper.class.getDeclaredConstructor();
	  
	  assertTrue(Modifier.isPrivate(constructor.getModifiers()));
	  
	  constructor.setAccessible(true);
	  constructor.newInstance();
	}



}
