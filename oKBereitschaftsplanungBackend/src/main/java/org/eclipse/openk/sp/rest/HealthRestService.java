/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.JsonGeneratorBase;
import org.eclipse.openk.core.controller.ResponseBuilderWrapper;
import org.eclipse.openk.resources.BaseResource;
import org.eclipse.openk.sp.controller.HealthController;
import org.eclipse.openk.sp.exceptions.SpExceptionMapper;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("health")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class HealthRestService extends BaseResource {

	@Autowired
	HealthController healthController;

	public HealthRestService() {
		super(Logger.getLogger(HealthRestService.class.getName()), new FileHelper());
	}

	@ApiOperation(value = "Version", notes = "This services gives the version of the application.")
	@GET
	@Path("/applicationversion")
	@Produces("application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
	public Response getVersion() {

		try { // NOSONAR

			String o = healthController.getVersion();
			String jsonStr = JsonGeneratorBase.getGson().toJson(o);

			return ResponseBuilderWrapper.INSTANCE.buildOKResponse(jsonStr);
		} catch (Exception e) {
			return healthController.responseFromException(e);
		}
	}

	@ApiOperation(value = "AppStatus", notes = "This services gives a 200 if the application runs.")
	@GET
	@Path("/status")
	@Produces("application/json")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "OK") })
	public Response getAppStatus() {

		try {

			healthController.checkDB();

			return ResponseBuilderWrapper.INSTANCE.buildOKResponse(SpExceptionMapper.getGeneralOKJson());
		} catch (Exception e) {
			return healthController.responseFromException(e);
		}
	}

}
