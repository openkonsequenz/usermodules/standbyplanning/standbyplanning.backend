/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.rest;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.resources.BaseResource;
import org.eclipse.openk.sp.controller.RegionController;
import org.eclipse.openk.sp.dto.LocationSelectionDto;
import org.eclipse.openk.sp.dto.RegionDto;
import org.eclipse.openk.sp.dto.RegionSelectionDto;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;

import io.swagger.annotations.ApiParam;

@Path("region")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class RegionRestService extends BaseResource {

	@Autowired
	private RegionController rCo;

	public RegionRestService() {
		super(Logger.getLogger(RegionRestService.class.getName()), new FileHelper());
	}

	@GET
	@Path("/list")
	public Response getRegions(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<RegionDto>> invokable = modusr -> rCo.getRegions();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Path("/selectionlist")
	public Response getRegionsSelection(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<List<RegionSelectionDto>> invokable = modusr -> rCo.getRegionsSelection();

		String[] securityRoles = Globals.getAllRolls();

		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@GET
	@Path("/{id}")
	public Response getRegion(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt) {

		ModifyingInvokable<RegionDto> invokable = modusr -> rCo.getRegion(id);

		String[] securityRoles = Globals.getAllRolls();

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);

	}

	@PUT
	@Path("/{id}/location/save/list")
	public Response saveLocationsForRegion(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid List<LocationSelectionDto> lsLocationDto) {

		ModifyingInvokable<List<LocationSelectionDto>> invokable = modusr -> rCo.saveLocationsForRegion(id,
				lsLocationDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/{id}/location/delete/list")
	public Response deleteLocationsForRegion(@PathParam("id") Long id,
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			List<LocationSelectionDto> lsLocationDto) {

		ModifyingInvokable<List<LocationSelectionDto>> invokable = modusr -> rCo.deleteLocationsForRegion(id,
				lsLocationDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

	@PUT
	@Path("/save")
	public Response saveRegionDto(
			@ApiParam(name = "Authorization", value = "JWT Token", required = true) @HeaderParam(value = Globals.KEYCLOAK_AUTH_TAG) String jwt,
			@Valid RegionSelectionDto regionSelectionDto) {

		RegionDto regionDto = new RegionDto(regionSelectionDto);

		ModifyingInvokable<RegionDto> invokable = modusr -> rCo.saveRegion(regionDto);

		String[] securityRoles = { Globals.KEYCLOAK_ROLE_BP_SACHBEARBEITER, Globals.KEYCLOAK_ROLE_BP_ADMIN };

		// return response
		return invokeRunnable(jwt, securityRoles, invokable);
	}

}
