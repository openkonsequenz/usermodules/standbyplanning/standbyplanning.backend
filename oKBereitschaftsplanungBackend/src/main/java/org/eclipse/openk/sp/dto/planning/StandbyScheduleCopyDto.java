/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "STANDBY_SCHEDULE_COPY" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "StandbyScheduleCopyDto")
@JsonInclude(Include.NON_NULL)
public class StandbyScheduleCopyDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Date validFrom;
	private Date validTo;
	private Long statusId;
	private List<TransferGroupDto> lsTransferGroup = new ArrayList<>();
	private Boolean overwrite;
	private Long standbyListId;

	public StandbyScheduleCopyDto() {
		/** default constructor. */
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the statusId
	 */
	public Long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the overwrite
	 */
	public Boolean getOverwrite() {
		return overwrite;
	}

	/**
	 * @param overwrite the overwrite to set
	 */
	public void setOverwrite(Boolean overwrite) {
		this.overwrite = overwrite;
	}

	/**
	 * @return the lsTransferGroup
	 */
	public List<TransferGroupDto> getLsTransferGroup() {
		return lsTransferGroup;
	}

	/**
	 * @param lsTransferGroup the lsTransferGroup to set
	 */
	public void setLsTransferGroup(List<TransferGroupDto> lsTransferGroup) {
		this.lsTransferGroup = lsTransferGroup;
	}

	public Long getStandbyListId() {
		return standbyListId;
	}

	public void setStandbyListId(Long standbyListId) {
		this.standbyListId = standbyListId;
	}

}
