/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.external;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.viewmodel.ErrorReturn;
import org.eclipse.openk.sp.controller.external.dto.DistanceServiceDto;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.LocationRepository;
import org.eclipse.openk.sp.db.model.Address;
import org.eclipse.openk.sp.db.model.Location;
import org.eclipse.openk.sp.db.model.Organisation;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.dto.AddressDto;
import org.eclipse.openk.sp.dto.LocationDto;
import org.eclipse.openk.sp.dto.OrganisationDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleSearchDto;
import org.eclipse.openk.sp.exceptions.SpNestedException;
import org.eclipse.openk.sp.util.FileHelper;
import org.eclipse.openk.sp.util.GsonTypeHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle requests to external interfaces operations. */
public class DistanceController extends RestService {

	protected static final Logger LOGGER = Logger.getLogger(DistanceController.class);

	@Autowired
	LocationRepository locationRepository;

	@Autowired
	EntityConverter entityConverter;

	@Autowired
	FileHelper fileHelper;

	public List<StandbyScheduleBody> calcDistance(List<StandbyScheduleBody> listBodies,
			StandbyScheduleSearchDto standbyScheduleSearchDto, String token, boolean onlyLogExceptions)
			throws SpNestedException {

		super.setPrintExceptionsOnlyToLog(onlyLogExceptions);

		// no distance when no location

		Long locationId = standbyScheduleSearchDto.getLocationId();
		if (standbyScheduleSearchDto == null || locationId == null || listBodies == null || listBodies.isEmpty()) {
			return listBodies;
		}

		// mapp to list of DistanceServiceDto
		String body = getInputBody(listBodies, standbyScheduleSearchDto);

		// get parameter

		boolean useHttps = true;
		String baseURL = "";
		String restFunctionWithParams = "?format=json";

		try {
			String use = fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_DISTANCE_USE_HTTPS);
			useHttps = Boolean.parseBoolean(use);
			baseURL = fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_DISTANCE_URL);

		} catch (Exception e) {
			LOGGER.info("Distance service is disabled!", e);

			if (!super.isPrintExceptionsOnlyToLog()) {
				ErrorReturn er = new ErrorReturn(503,
						"Configuration for interface 'Distance Service' is wrong or missing!");
				throw new SpNestedException(er);
			}
		}

		// the call
		try {

			HttpResponse response = performPutRequest(useHttps, baseURL, restFunctionWithParams, token, body);

			// 200?
			if (validateResponse(response)) {

				// conjunction of the data
				mergeResponse(response, listBodies);
			}

		} catch (SpNestedException e) {
			LOGGER.info(e.getMessage(), e);
			if (!super.isPrintExceptionsOnlyToLog()) {
				ErrorReturn er = new ErrorReturn(502, "'Distance Service' is not available!");
				throw new SpNestedException(er);
			}
		}

		return listBodies;
	}

	public boolean mergeResponse(HttpResponse response, List<StandbyScheduleBody> listBodies) throws SpNestedException {

		String jsonList = responseToJson(response, StandardCharsets.UTF_8);

		if (jsonList == null) {
			return false;
		}

		List<DistanceServiceDto> lsDistanceServiceDtoResponse = (List<DistanceServiceDto>) new Gson().fromJson(jsonList,
				GsonTypeHelper.JSON_TYPE_DISTANCESERVICE_DTO_ARRAY_LIST);

		for (StandbyScheduleBody sbBody : listBodies) {
			for (DistanceServiceDto distanceServiceDto : lsDistanceServiceDtoResponse) {
				if (distanceServiceDto.getBodyId().longValue() == sbBody.getId()) {
					sbBody.setOrganisationDistance(distanceServiceDto.getOrganisationDistance());
					sbBody.setPrivateDistance(distanceServiceDto.getPrivateDisance());
					break;
				}
			}
		}

		return true;
	}

	private String getInputBody(List<StandbyScheduleBody> listBodies,
			StandbyScheduleSearchDto standbyScheduleSearchDto) {

		// mapp to DistanceServiceDto

		Location location = locationRepository.findOne(standbyScheduleSearchDto.getLocationId());
		LocationDto lDto = (LocationDto) entityConverter.convertEntityToDto(location, new LocationDto());

		ArrayList<DistanceServiceDto> lsDistanceServiceDto = new ArrayList<>();

		for (StandbyScheduleBody body : listBodies) {
			Long userId = body.getUser().getId();
			Organisation orga = body.getUser().getOrganisation();
			Address address = body.getUser().getPrivateAddress();

			OrganisationDto oDto = (OrganisationDto) entityConverter.convertEntityToDto(orga, new OrganisationDto());
			AddressDto aDto = (AddressDto) entityConverter.convertEntityToDto(address, new AddressDto());

			DistanceServiceDto dsDto = new DistanceServiceDto();
			dsDto.setBodyId(body.getId());
			dsDto.setLocation(lDto);
			dsDto.setOrganisation(oDto);
			dsDto.setPrivateAddress(aDto);
			dsDto.setUserId(userId);

			lsDistanceServiceDto.add(dsDto);
		}

		// convert list to string
		String body = new Gson().toJson(lsDistanceServiceDto, GsonTypeHelper.JSON_TYPE_DISTANCESERVICE_DTO_ARRAY_LIST);

		return body;
	}

}
