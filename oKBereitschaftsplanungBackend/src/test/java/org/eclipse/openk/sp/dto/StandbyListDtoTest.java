/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;
import java.util.List;

import org.junit.Test;

public class StandbyListDtoTest {

	@Test
	public void testGettersAndSetters() {
		String text = "test";
		Date d = new Date();
		StandbyListDto sl = new StandbyListDto();
		sl.setId(1l);
		assertEquals(1l, sl.getId().longValue());

		sl.setModificationDate(d);
		assertEquals(d, sl.getModificationDate());

		sl.setTitle(text);
		assertEquals(text, sl.getTitle());

		StandbyListHasStandbyGroupDto standbyListHasStandbyGroup = new StandbyListHasStandbyGroupDto();
		standbyListHasStandbyGroup.setStandbyGroupId(1L);
		standbyListHasStandbyGroup.setStandbyGroupName(text);
		standbyListHasStandbyGroup.setPosition(1);
		standbyListHasStandbyGroup.setStandbyListId(sl.getId());

		sl.getLsStandbyListHasStandbyGroup().add(standbyListHasStandbyGroup);
		List<StandbyListHasStandbyGroupDto> list = sl.getLsStandbyListHasStandbyGroup();
		sl.setLsStandbyListHasStandbyGroup(list);
		assertNotNull(sl.getLsStandbyListHasStandbyGroup());
		assertEquals(standbyListHasStandbyGroup, sl.getLsStandbyListHasStandbyGroup().get(0));

	}

}
