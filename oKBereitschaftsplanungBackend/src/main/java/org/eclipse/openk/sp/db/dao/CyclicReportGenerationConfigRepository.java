/* *******************************************************************************
 * Copyright (c) 2020 Basys GmbH
 * 
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 * *******************************************************************************/
package org.eclipse.openk.sp.db.dao;

import java.util.List;
import java.util.Optional;

import org.eclipse.openk.sp.db.model.ReportGenerationConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CyclicReportGenerationConfigRepository extends JpaRepository<ReportGenerationConfig, Long> {

	Optional<ReportGenerationConfig> findById(Long configId);

	@Query(value = "SELECT rgc FROM ReportGenerationConfig rgc WHERE rgc.triggerWeekDay = :weekDay and rgc.triggerHour = :hour and rgc.triggerMinute >= :minuteGE and rgc.triggerMinute <= :minuteLE")
	List<ReportGenerationConfig> findConfigs(@Param("weekDay") int weekDay, @Param("hour")int hour, @Param("minuteGE")int minuteGE, @Param("minuteLE")int minuteLE);

}