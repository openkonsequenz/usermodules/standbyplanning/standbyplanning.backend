/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;

public class StandbyListTest {

	@Test
	public void testGettersAndSetters() {
		Date d = new Date();
		StandbyList sl = new StandbyList();
		sl.setId(1l);
		assertEquals(1l, sl.getId().longValue());

		StandbyList standbyList = new StandbyList(2L);
		assertEquals(2L, standbyList.getId().longValue());

		sl.setModificationDate(d);
		assertEquals(d, sl.getModificationDate());

		sl.setTitle("Title");
		assertEquals("Title", sl.getTitle());

		List<StandbyScheduleHeader> lsScheduleHeader = new ArrayList<>();
		sl.setLsScheduleHeader(lsScheduleHeader);
		assertEquals(lsScheduleHeader, sl.getLsScheduleHeader());

//		ArrayList<StandbyGroup> lsStandbyGroups = new ArrayList<>();
//		StandbyGroup group = new StandbyGroup();
//		lsStandbyGroups.add(group);
//		sl.setLsStandbyGroups(lsStandbyGroups);
//		assertNotNull(sl.getLsStandbyGroups());
//		assertEquals(group, sl.getLsStandbyGroups().get(0));
	}

	@Test
	public void testCompare() throws MalformedURLException {
		StandbyList obj1 = new StandbyList();
		obj1.setId(5L);

		StandbyList obj2 = obj1;
		assertEquals(1, obj1.compareTo(obj2));

		StandbyList obj3 = new StandbyList();
		obj3.setId(6L);
		assertEquals(0, obj1.compareTo(obj3));

	}

}
