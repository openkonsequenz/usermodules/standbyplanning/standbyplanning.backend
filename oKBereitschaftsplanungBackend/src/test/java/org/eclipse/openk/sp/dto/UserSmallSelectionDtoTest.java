/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Test;

public class UserSmallSelectionDtoTest {

	@Test
	public void testGettersAndSetters() {
		String test = "test";
		Date d = new Date();
		UserSmallSelectionDto u = new UserSmallSelectionDto();

		u.setId(1l);
		assertEquals(1l, u.getId().longValue());

		u.setFirstname("FirstName");
		assertEquals("FirstName", u.getFirstname());

		u.setLastname("LastName");
		assertEquals("LastName", u.getLastname());

		ContactDataDto contactData = new ContactDataDto();
		u.setBusinessContactData(contactData);
		assertEquals(contactData, u.getBusinessContactData());

		u.setPrivateContactData(contactData);
		assertEquals(contactData, u.getPrivateContactData());

	}
}
