/********************************************************************************
 * Copyright (c) 2019 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.eclipse.openk.sp.util.FileHelper;
import org.eclipse.openk.sp.util.SpMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
/** Class to handle document operations. */
public class DocumentController extends AbstractController {
	protected static final Logger LOGGER = Logger.getLogger(DocumentController.class);

	@Autowired
	private FileHelper fileHelper;

	private static final String USER_DOCUMENTATION_PATH = "documents";
	private static final String USER_DOCUMENTATION_FILE = "oK_Bereitschaftsplanung.pdf";

	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	public File getUserDocumentation() throws SpException {
		try {
			return fileHelper.loadFileFromFileSystemOrResource(USER_DOCUMENTATION_PATH, USER_DOCUMENTATION_FILE, false);
		} catch (Exception e) {
			SpException spE = new SpException(SpExceptionEnum.LOADING_LIST_EXCEPTION, SpMsg.TXT_BRANCHES);
			LOGGER.error(spE, spE);
			throw spE;
		}
	}
}
