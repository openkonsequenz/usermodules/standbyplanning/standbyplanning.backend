/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Date;

import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.junit.Test;

public class StandbyDurationDtoTest {

	@Test
	public void testGettersAndSetters() {
		Date d = new Date();
		Boolean b = new Boolean(true);
		TimeDto td = new TimeDto();
		
		StandbyDurationDto dur = new StandbyDurationDto();
		dur.setStandbyDurationId(1l);
		assertEquals(1l, dur.getStandbyDurationId().longValue());
		
		dur.setModificationDate(d);
		assertEquals(d, dur.getModificationDate());
		
		dur.setValidDayFrom(1);
		assertEquals(1, dur.getValidDayFrom().intValue());
		
		dur.setValidDayTo(1);
		assertEquals(1, dur.getValidDayTo().intValue());
		
		dur.setValidFrom(td);
		assertEquals(td, dur.getValidFrom());
		
		dur.setValidTo(td);
		assertEquals(td, dur.getValidTo());
		
		dur.setNextUserInNextDuration(b);
		assertEquals(b, dur.getNextUserInNextDuration());
		
		dur.setStandbyGroupId(1L);
		assertEquals(1L, dur.getStandbyGroupId().longValue());
		

		
	}
	
	

	

}
