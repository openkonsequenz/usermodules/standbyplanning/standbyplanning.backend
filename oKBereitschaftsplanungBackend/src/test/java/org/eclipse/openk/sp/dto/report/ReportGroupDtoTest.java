/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.report;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Date;

import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.junit.Test;

public class ReportGroupDtoTest {

	@Test
	public void testReportGroupGetterSetter() {

		ReportGroupDto dto = new ReportGroupDto();
		assertNotNull(dto);

		dto.setFromDate(new Date());
		assertNotNull(dto.getFromDate());

		dto.setGroup1(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup1());

		dto.setGroup2(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup2());

		dto.setGroup3(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup3());

		dto.setGroup4(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup4());

		dto.setGroup5(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup5());

		dto.setGroup6(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup6());

		dto.setGroup7(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup7());

		dto.setGroup8(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup8());

		dto.setGroup9(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup9());

		dto.setGroup10(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup10());

		dto.setGroup11(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup11());

		dto.setGroup12(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup12());

		dto.setGroup13(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup13());

		dto.setGroup14(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup14());

		dto.setGroup15(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup15());

		dto.setGroup16(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup16());

		dto.setGroup17(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup17());

		dto.setGroup18(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup18());

		dto.setGroup19(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup19());

		dto.setGroup20(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup20());

		dto.setGroup21(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup21());

		dto.setGroup22(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup22());

		dto.setGroup23(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup23());

		dto.setGroup24(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup24());

		dto.setGroup25(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup25());

		dto.setGroup26(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup26());

		dto.setGroup27(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup27());

		dto.setGroup28(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup28());

		dto.setGroup29(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup29());

		dto.setGroup30(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup30());

		dto.setGroup31(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup31());

		dto.setGroup32(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup32());

		dto.setGroup33(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup33());

		dto.setGroup34(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup34());

		dto.setGroup35(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup35());

		dto.setGroup36(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup36());

		dto.setGroup37(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup37());

		dto.setGroup38(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup38());

		dto.setGroup39(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup39());

		dto.setGroup40(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup40());

		dto.setGroup41(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup41());

		dto.setGroup42(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup42());

		dto.setGroup43(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup43());

		dto.setGroup44(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup44());

		dto.setGroup45(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup45());

		dto.setGroup46(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup46());

		dto.setGroup47(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup47());

		dto.setGroup48(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup48());

		dto.setGroup49(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup49());

		dto.setGroup50(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup50());

		dto.setGroup51(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup51());

		dto.setGroup52(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup52());

		dto.setGroup53(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup53());

		dto.setGroup54(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup54());

		dto.setGroup55(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup55());

		dto.setGroup56(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup56());

		dto.setGroup57(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup57());

		dto.setGroup58(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup58());

		dto.setGroup59(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup59());

		dto.setGroup60(new StandbyGroupSelectionDto());
		assertNotNull(dto.getGroup60());

		dto.setUser1("a");
		assertEquals("a", dto.getUser1());

		dto.setUser2("a");
		assertEquals("a", dto.getUser2());

		dto.setUser3("a");
		assertEquals("a", dto.getUser3());

		dto.setUser4("a");
		assertEquals("a", dto.getUser4());

		dto.setUser5("a");
		assertEquals("a", dto.getUser5());

		dto.setUser6("a");
		assertEquals("a", dto.getUser6());

		dto.setUser7("a");
		assertEquals("a", dto.getUser7());

		dto.setUser8("a");
		assertEquals("a", dto.getUser8());

		dto.setUser9("a");
		assertEquals("a", dto.getUser9());

		dto.setUser10("a");
		assertEquals("a", dto.getUser10());

	}

	@Test(expected = SpException.class)
	public void testsetUserX() throws SpException {
		String text = "test";
		ReportGroupDto dto = new ReportGroupDto();
		assertNotNull(dto);

		dto.setUserX(63, text);

	}

	@Test(expected = SpException.class)
	public void testsetGroupX() throws SpException {
		String text = "test";
		ReportGroupDto dto = new ReportGroupDto();
		assertNotNull(dto);

		dto.setGroupX(63, new StandbyGroupSelectionDto());

	}

}
