/*Standby List History*/
CREATE TABLE IF NOT EXISTS STANDBY_LIST_HIST (HIST_ID serial, operation char(1), stamp timestamp, LIKE STANDBY_LIST EXCLUDING ALL);
--
-- Create a row in {0}.standby_list_hist_audit to reflect the operation performed on standby_list,
-- making use of the special variable TG_OP to work out the operation.
--
CREATE OR REPLACE FUNCTION process_standby_list_audit() RETURNS TRIGGER AS $object_audit$ BEGIN IF (TG_OP = 'DELETE') THEN INSERT INTO {0}.standby_list_hist SELECT nextval('standby_list_hist_hist_id_seq'), 'D', now(), OLD.*; ELSIF (TG_OP = 'UPDATE') THEN INSERT INTO {0}.standby_list_hist SELECT nextval('standby_list_hist_hist_id_seq'), 'U', now(), NEW.*; ELSIF (TG_OP = 'INSERT') THEN INSERT INTO {0}.standby_list_hist SELECT nextval('standby_list_hist_hist_id_seq'), 'I', now(), NEW.*; END IF; RETURN NULL; END; $object_audit$ LANGUAGE plpgsql;
DROP TRIGGER IF EXISTS standby_list_audit on {0}.standby_list;
CREATE TRIGGER standby_list_audit AFTER INSERT OR UPDATE OR DELETE ON {0}.standby_list FOR EACH ROW EXECUTE PROCEDURE process_standby_list_audit();