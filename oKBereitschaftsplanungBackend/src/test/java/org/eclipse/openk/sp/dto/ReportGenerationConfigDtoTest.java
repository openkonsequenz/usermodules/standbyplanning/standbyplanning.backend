package org.eclipse.openk.sp.dto;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

public class ReportGenerationConfigDtoTest {

	@Test
	public void testGetterAndSetter() {

		ReportGenerationConfigDto config = new ReportGenerationConfigDto();
		
		Integer triggerHour = 1;
		Integer triggerMinute = 1;
		Integer triggerWeekDay = 1;
		Integer validFromDayOffset = -1;
		Integer validFromHour = 2;
		Integer validFromMinute = 30;
		Integer validToDayOffset = 1;
		Integer validToHour = 3;
		Integer validToMinute = 35;
		Long standByListId = 23L;
		Long statusId = 4L;
		String fileNamePattern = "pattern";
		String name = "name";
		String printFormat = "pdf";
		String reportName = "REPORT_NAME";
		String subject = "subject";
		Set<String> to  = new HashSet<>();
		
		
		config.setFileNamePattern(fileNamePattern);
		assertEquals(fileNamePattern, config.getFileNamePattern());
		
		config.setName(name);
		assertEquals(name, config.getName());
		
		config.setPrintFormat(printFormat);
		assertEquals(printFormat, config.getPrintFormat());
		
		config.setReportName(reportName);
		assertEquals(reportName, config.getReportName());
		
		config.setStandByListId(standByListId);
		assertEquals(standByListId, config.getStandByListId());

		config.setStatusId(statusId);
		assertEquals(statusId, config.getStatusId());
		
		config.setSubject(subject);
		assertEquals(subject, config.getSubject());
		
		config.setTo(to);
		assertEquals(to, config.getTo());

		config.setTriggerHour(triggerHour);
		assertEquals(triggerHour, config.getTriggerHour());
		
		config.setTriggerMinute(triggerMinute);
		assertEquals(triggerMinute, config.getTriggerMinute());

		config.setTriggerWeekDay(triggerWeekDay);
		assertEquals(triggerWeekDay, config.getTriggerWeekDay());

		config.setValidFromDayOffset(validFromDayOffset);
		assertEquals(validFromDayOffset, config.getValidFromDayOffset());
		
		config.setValidFromHour(validFromHour);
		assertEquals(validFromHour, config.getValidFromHour());
		
		config.setValidFromMinute(validFromMinute);
		assertEquals(validFromMinute, config.getValidFromMinute());
		
		config.setValidToDayOffset(validToDayOffset);
		assertEquals(validToDayOffset, config.getValidToDayOffset());
		
		config.setValidToHour(validToHour);
		assertEquals(validToHour, config.getValidToHour());
		
		config.setValidToMinute(validToMinute);
		assertEquals(validToMinute, config.getValidToMinute());
	}

}
