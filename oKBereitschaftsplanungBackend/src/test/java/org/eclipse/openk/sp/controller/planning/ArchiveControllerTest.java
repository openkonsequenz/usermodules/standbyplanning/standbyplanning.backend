/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.planning;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.controller.AbstractController;
import org.eclipse.openk.sp.controller.StandbyScheduleController;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.ArchiveRepository;
import org.eclipse.openk.sp.db.dao.StandbyGroupRepository;
import org.eclipse.openk.sp.db.dao.StandbyListRepository;
import org.eclipse.openk.sp.db.dao.StandbyScheduleBodyRepository;
import org.eclipse.openk.sp.db.dao.StandbyStatusRepository;
import org.eclipse.openk.sp.db.dao.UserRepository;
import org.eclipse.openk.sp.db.model.ArchiveStandbyScheduleHeader;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.db.model.StandbyStatus;
import org.eclipse.openk.sp.db.model.User;
import org.eclipse.openk.sp.dto.ArchiveStandbyScheduleHeaderDto;
import org.eclipse.openk.sp.dto.ArchiveStandbyScheduleHeaderSelectionDto;
import org.eclipse.openk.sp.dto.StandbyGroupSelectionDto;
import org.eclipse.openk.sp.dto.StandbyScheduleArchiveDto;
import org.eclipse.openk.sp.dto.StandbyScheduleBodySelectionDto;
import org.eclipse.openk.sp.dto.UserSmallSelectionDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgDto;
import org.eclipse.openk.sp.dto.planning.PlanningMsgResponseDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleActionDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleFilterDto;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleSearchDto;
import org.eclipse.openk.sp.dto.planning.TransferGroupDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.mail.MailRequest;
import org.eclipse.openk.sp.util.DateHelper;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

@RunWith(MockitoJUnitRunner.class)
public class ArchiveControllerTest extends AbstractController {

	protected static final Logger LOGGER = Logger.getLogger(ArchiveControllerTest.class);

	@Mock
	private StandbyGroupRepository standbyGroupRepository;

	@Mock
	private StandbyListRepository standbyListRepository;

	@Mock
	private StandbyStatusRepository standbyStatusRepository;

	@Mock
	private ArchiveRepository archiveRepository;

	@Mock
	private PlannedDataController plannedDataController;

	@Mock
	private EntityConverter entityConverter;

	@Mock
	private StandbyScheduleController standbyScheduleController;

	@Mock
	private BodyDataCopyController bodyDataCopyController;

	@Mock
	private StandbyScheduleBodyRepository standbyScheduleBodyRepository;

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	ArchiveController archiveController;

	@Mock
	private MailRequest mailRequest;

	@Test
	public void createHeaderTest() throws SpException {
		Long id = new Long(1);
		String text = "";
		ArchiveStandbyScheduleHeader result;
		StandbyScheduleArchiveDto plan = new StandbyScheduleArchiveDto();
		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		plan.setFilter(filter);

		filter.setStandbyListId(id);
		filter.setValidFrom(new Date());
		filter.setValidTo(new Date());

		StandbyList standbyList = new StandbyList();
		standbyList.setTitle("title");

		StandbyStatus status = new StandbyStatus();
		status.setTitle("status");

		Mockito.when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(standbyList);
		Mockito.when(standbyStatusRepository.findOne(Mockito.anyLong())).thenReturn(status);

		result = archiveController.createHeader(plan, id, text, text, text, text);
		assertNotNull(result);

	}

	@Test
	public void createByStandbyListTest() throws SpException {
		ArchiveStandbyScheduleHeaderDto header;

		Long id = new Long(2);
		String userName = "mx";

		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		filter.setStandbyListId(id);
		filter.setValidFrom(new Date());
		filter.setValidTo(new Date());

		StandbyList standbyList = new StandbyList();
		standbyList.setTitle("title");

		StandbyStatus status = new StandbyStatus();
		status.setTitle("status");
		status.setId(id);

		StandbyScheduleArchiveDto plan = new StandbyScheduleArchiveDto();
		plan.setFilter(filter);
		ArchiveStandbyScheduleHeaderDto headerDto = new ArchiveStandbyScheduleHeaderDto();

		Mockito.when(plannedDataController.getFilteredPlanByStatusForArchive(Mockito.any(), Mockito.anyLong()))
				.thenReturn(plan);
		Mockito.when(entityConverter.convertEntityToDto(Mockito.any(), Mockito.any())).thenReturn(headerDto);
		Mockito.when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(standbyList);
		Mockito.when(standbyStatusRepository.findOne(Mockito.anyLong())).thenReturn(status);

		header = archiveController.manualCreateByStandbyList(filter, id, userName);
		assertNotNull(header);

	}

	@Test
	public void createByStandbyGroup() throws SpException {
		ArchiveStandbyScheduleHeaderDto header;

		Long id = new Long(2);
		String userName = "mx";

		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		filter.setStandbyListId(id);
		filter.setValidFrom(new Date());
		filter.setValidTo(new Date());

		StandbyList standbyList = new StandbyList();
		standbyList.setTitle("title");

		StandbyStatus status = new StandbyStatus();
		status.setTitle("status");
		status.setId(id);

		StandbyScheduleArchiveDto plan = new StandbyScheduleArchiveDto();
		plan.setFilter(filter);
		ArchiveStandbyScheduleHeaderDto headerDto = new ArchiveStandbyScheduleHeaderDto();

		Mockito.when(plannedDataController.getFilteredPlanByStatusByGroupsForArchive(Mockito.any(), Mockito.anyLong()))
				.thenReturn(plan);
		Mockito.when(entityConverter.convertEntityToDto(Mockito.any(), Mockito.any())).thenReturn(headerDto);
		Mockito.when(standbyListRepository.findOne(Mockito.anyLong())).thenReturn(standbyList);
		Mockito.when(standbyStatusRepository.findOne(Mockito.anyLong())).thenReturn(status);

		ArchiveStandbyScheduleHeaderDto result = archiveController.createByStandbyGroup(filter, id, userName);

		assertNotNull(result);
	}

	@Test
	public void getOneArchiveHeaderTest() {
		Long id = new Long(2);
		ArchiveStandbyScheduleHeaderDto result = new ArchiveStandbyScheduleHeaderDto();

		ArchiveStandbyScheduleHeader header = new ArchiveStandbyScheduleHeader();
		Mockito.when(archiveRepository.findOne(Mockito.anyLong())).thenReturn(header);

		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) result);

		result = archiveController.getOneArchiveHeader(id);

		assertNotNull(result);

	}

	@Test
	public void getAllTest() {

		List<ArchiveStandbyScheduleHeader> listHeader = new ArrayList<>();
		List<ArchiveStandbyScheduleHeaderSelectionDto> result = new ArrayList<>();

		Mockito.when(archiveRepository.findAll()).thenReturn(listHeader);
		when(entityConverter.convertEntityToDtoList((List<AbstractEntity>) any(), (List<AbstractDto>) any(), any()))
				.thenReturn(listHeader);

		result = archiveController.getAll();

		assertNotNull(result);

	}

	@Test(expected = SpException.class)
	public void importPlan1Test() throws SpException {
		Long id = null;
		String modUserName = "test";

		PlanningMsgResponseDto responseDto;

		Mockito.when(archiveRepository.exists(Mockito.anyLong())).thenReturn(false);

		responseDto = archiveController.importPlan(id, modUserName);

		assertNotNull(responseDto);

	}

	@Test(expected = SpException.class)
	public void importPlan2Test() throws SpException {
		Long id = new Long(2);
		String modUserName = "test";

		PlanningMsgResponseDto responseDto;

		Mockito.when(archiveRepository.exists(Mockito.anyLong())).thenReturn(false);

		responseDto = archiveController.importPlan(id, modUserName);

		assertNotNull(responseDto);

	}

	@Test
	public void importPlan3Test() throws SpException {
		Long id = new Long(2);
		String modUserName = "test";
		String jsonPlan = "{\"filter\":{\"validFrom\":\"Oct 14, 2018 2:00:00 AM\",\"validTo\":\"Oct 16, 2018 2:00:00 AM\",\"standbyListId\":1},\"planHeader\":{\"label\":\"Datum/Gruppe\",\"listGroups\":[{\"id\":1,\"title\":\"group1\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\"},{\"id\":2,\"title\":\"group2\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\"},{\"id\":3,\"title\":\"Sondergruppe\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\"}]},\"listPlanRows\":[{\"label\":\"14.10.2018, Sonntag\",\"listGroupBodies\":[[{\"id\":27,\"validFrom\":\"Oct 14, 2018 12:00:00 AM\",\"validTo\":\"Oct 14, 2018 8:00:00 AM\",\"user\":{\"id\":1,\"firstname\":\"Max\",\"lastname\":\"Mustermann\",\"isCompany\":false,\"notes\":\"Test-Notiz\",\"validFrom\":\"Jan 1, 2018 2:00:00 AM\",\"validTo\":\"Jan 1, 2022 2:00:00 AM\",\"modificationDate\":\"Aug 2, 2018 4:57:07 PM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":3,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Private Str. 1a\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":3,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[{\"id\":1,\"regionId\":1,\"regionName\":\"Darmstadt\",\"userId\":1}],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":1,\"title\":\"group1\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":1,\"functionName\":\"Meister Betrieb Strom\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":28,\"validFrom\":\"Oct 14, 2018 8:00:00 AM\",\"validTo\":\"Oct 14, 2018 11:59:59 PM\",\"user\":{\"id\":2,\"firstname\":\"Erika\",\"lastname\":\"Musterfrau\",\"isCompany\":false,\"notes\":\"Test-Notiz\",\"validFrom\":\"Jan 1, 2018 2:00:00 AM\",\"validTo\":\"Jan 1, 2022 2:00:00 AM\",\"modificationDate\":\"Aug 2, 2018 4:57:07 PM\",\"organisation\":{\"id\":2,\"orgaName\":\"Mustermann GmbH\",\"address\":{\"id\":2,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 2\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":4,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Am Feldweg 5\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":2,\"isPrivate\":false,\"phone\":\"0123 111111\",\"cellphone\":\"0123 987654\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 987654\"},\"privateContactData\":{\"id\":4,\"isPrivate\":false,\"phone\":\"0123 111111\",\"cellphone\":\"0123 987654\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 987654\"},\"lsUserInRegions\":[{\"id\":2,\"regionId\":1,\"regionName\":\"Darmstadt\",\"userId\":2},{\"id\":3,\"regionId\":2,\"regionName\":\"Erbach\",\"userId\":2}],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":1,\"title\":\"group1\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":1,\"functionName\":\"Meister Betrieb Strom\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}],[{\"id\":87,\"validFrom\":\"Oct 14, 2018 12:00:00 AM\",\"validTo\":\"Oct 14, 2018 8:00:00 AM\",\"user\":{\"id\":3,\"firstname\":\"N\",\"lastname\":\"N 1\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":2,\"title\":\"group2\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":3,\"functionName\":\"Ingenieure Strom/Gas/Wasser\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":88,\"validFrom\":\"Oct 14, 2018 8:00:00 AM\",\"validTo\":\"Oct 14, 2018 11:59:59 PM\",\"user\":{\"id\":3,\"firstname\":\"N\",\"lastname\":\"N 1\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":2,\"title\":\"group2\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":3,\"functionName\":\"Ingenieure Strom/Gas/Wasser\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}],[{\"id\":147,\"validFrom\":\"Oct 14, 2018 12:00:00 AM\",\"validTo\":\"Oct 14, 2018 8:00:00 AM\",\"user\":{\"id\":7,\"firstname\":\"N\",\"lastname\":\"N 5\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":3,\"title\":\"Sondergruppe\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":true,\"lsUserFunction\":[{\"functionId\":4,\"functionName\":\"Meister Betrieb Gas\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":148,\"validFrom\":\"Oct 14, 2018 8:00:00 AM\",\"validTo\":\"Oct 14, 2018 11:59:59 PM\",\"user\":{\"id\":8,\"firstname\":\"N\",\"lastname\":\"N 6\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":3,\"title\":\"Sondergruppe\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":true,\"lsUserFunction\":[{\"functionId\":4,\"functionName\":\"Meister Betrieb Gas\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}]]},{\"label\":\"15.10.2018, Montag\",\"listGroupBodies\":[[{\"id\":29,\"validFrom\":\"Oct 15, 2018 12:00:00 AM\",\"validTo\":\"Oct 15, 2018 8:00:00 AM\",\"user\":{\"id\":2,\"firstname\":\"Erika\",\"lastname\":\"Musterfrau\",\"isCompany\":false,\"notes\":\"Test-Notiz\",\"validFrom\":\"Jan 1, 2018 2:00:00 AM\",\"validTo\":\"Jan 1, 2022 2:00:00 AM\",\"modificationDate\":\"Aug 2, 2018 4:57:07 PM\",\"organisation\":{\"id\":2,\"orgaName\":\"Mustermann GmbH\",\"address\":{\"id\":2,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 2\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":4,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Am Feldweg 5\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":2,\"isPrivate\":false,\"phone\":\"0123 111111\",\"cellphone\":\"0123 987654\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 987654\"},\"privateContactData\":{\"id\":4,\"isPrivate\":false,\"phone\":\"0123 111111\",\"cellphone\":\"0123 987654\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 987654\"},\"lsUserInRegions\":[{\"id\":2,\"regionId\":1,\"regionName\":\"Darmstadt\",\"userId\":2},{\"id\":3,\"regionId\":2,\"regionName\":\"Erbach\",\"userId\":2}],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":1,\"title\":\"group1\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":1,\"functionName\":\"Meister Betrieb Strom\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":30,\"validFrom\":\"Oct 15, 2018 4:00:00 PM\",\"validTo\":\"Oct 15, 2018 11:59:59 PM\",\"user\":{\"id\":3,\"firstname\":\"N\",\"lastname\":\"N 1\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":1,\"title\":\"group1\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":1,\"functionName\":\"Meister Betrieb Strom\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}],[{\"id\":89,\"validFrom\":\"Oct 15, 2018 12:00:00 AM\",\"validTo\":\"Oct 15, 2018 8:00:00 AM\",\"user\":{\"id\":3,\"firstname\":\"N\",\"lastname\":\"N 1\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":2,\"title\":\"group2\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":3,\"functionName\":\"Ingenieure Strom/Gas/Wasser\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":90,\"validFrom\":\"Oct 15, 2018 4:00:00 PM\",\"validTo\":\"Oct 15, 2018 11:59:59 PM\",\"user\":{\"id\":3,\"firstname\":\"N\",\"lastname\":\"N 1\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":2,\"title\":\"group2\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":3,\"functionName\":\"Ingenieure Strom/Gas/Wasser\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}],[{\"id\":149,\"validFrom\":\"Oct 15, 2018 12:00:00 AM\",\"validTo\":\"Oct 15, 2018 8:00:00 AM\",\"user\":{\"id\":8,\"firstname\":\"N\",\"lastname\":\"N 6\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":3,\"title\":\"Sondergruppe\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":true,\"lsUserFunction\":[{\"functionId\":4,\"functionName\":\"Meister Betrieb Gas\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":150,\"validFrom\":\"Oct 15, 2018 4:00:00 PM\",\"validTo\":\"Oct 15, 2018 11:59:59 PM\",\"user\":{\"id\":9,\"firstname\":\"N\",\"lastname\":\"N 7\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":3,\"title\":\"Sondergruppe\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":true,\"lsUserFunction\":[{\"functionId\":4,\"functionName\":\"Meister Betrieb Gas\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}]]},{\"label\":\"16.10.2018, Dienstag\",\"listGroupBodies\":[[{\"id\":31,\"validFrom\":\"Oct 16, 2018 12:00:00 AM\",\"validTo\":\"Oct 16, 2018 8:00:00 AM\",\"user\":{\"id\":3,\"firstname\":\"N\",\"lastname\":\"N 1\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":1,\"title\":\"group1\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":1,\"functionName\":\"Meister Betrieb Strom\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":32,\"validFrom\":\"Oct 16, 2018 4:00:00 PM\",\"validTo\":\"Oct 16, 2018 11:59:59 PM\",\"user\":{\"id\":4,\"firstname\":\"N\",\"lastname\":\"N 2\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":1,\"title\":\"group1\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":1,\"functionName\":\"Meister Betrieb Strom\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}],[{\"id\":91,\"validFrom\":\"Oct 16, 2018 12:00:00 AM\",\"validTo\":\"Oct 16, 2018 8:00:00 AM\",\"user\":{\"id\":3,\"firstname\":\"N\",\"lastname\":\"N 1\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":2,\"title\":\"group2\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":3,\"functionName\":\"Ingenieure Strom/Gas/Wasser\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":92,\"validFrom\":\"Oct 16, 2018 4:00:00 PM\",\"validTo\":\"Oct 16, 2018 11:59:59 PM\",\"user\":{\"id\":1,\"firstname\":\"Max\",\"lastname\":\"Mustermann\",\"isCompany\":false,\"notes\":\"Test-Notiz\",\"validFrom\":\"Jan 1, 2018 2:00:00 AM\",\"validTo\":\"Jan 1, 2022 2:00:00 AM\",\"modificationDate\":\"Aug 2, 2018 4:57:07 PM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":3,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Private Str. 1a\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":3,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[{\"id\":1,\"regionId\":1,\"regionName\":\"Darmstadt\",\"userId\":1}],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":2,\"title\":\"group2\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":3,\"functionName\":\"Ingenieure Strom/Gas/Wasser\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}],[{\"id\":151,\"validFrom\":\"Oct 16, 2018 12:00:00 AM\",\"validTo\":\"Oct 16, 2018 8:00:00 AM\",\"user\":{\"id\":9,\"firstname\":\"N\",\"lastname\":\"N 7\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":3,\"title\":\"Sondergruppe\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":true,\"lsUserFunction\":[{\"functionId\":4,\"functionName\":\"Meister Betrieb Gas\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":152,\"validFrom\":\"Oct 16, 2018 4:00:00 PM\",\"validTo\":\"Oct 16, 2018 11:59:59 PM\",\"user\":{\"id\":9,\"firstname\":\"N\",\"lastname\":\"N 7\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":3,\"title\":\"Sondergruppe\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":true,\"lsUserFunction\":[{\"functionId\":4,\"functionName\":\"Meister Betrieb Gas\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}]]}]}";

		StandbyStatus status = new StandbyStatus();
		ArchiveStandbyScheduleHeader header = new ArchiveStandbyScheduleHeader();

		header.setJsonPlan(jsonPlan);
		PlanningMsgResponseDto responseDto;

		Mockito.when(archiveRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(standbyStatusRepository.findOne(Mockito.anyLong())).thenReturn(status);
		Mockito.when(archiveRepository.findOne(Mockito.anyLong())).thenReturn(header);

		responseDto = archiveController.importPlan(id, modUserName);

		assertNotNull(responseDto);

	}

	@Test
	public void importPlan4Test() throws SpException {
		Long id = new Long(2);
		String modUserName = "test";
		String jsonPlan = "{\"filter\":{\"validFrom\":\"Oct 14, 2018 2:00:00 AM\",\"validTo\":\"Oct 16, 2018 2:00:00 AM\",\"standbyListId\":1},\"planHeader\":{\"label\":\"Datum/Gruppe\",\"listGroups\":[{\"id\":1,\"title\":\"group1\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\"},{\"id\":2,\"title\":\"group2\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\"},{\"id\":3,\"title\":\"Sondergruppe\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\"}]},\"listPlanRows\":[{\"label\":\"14.10.2018, Sonntag\",\"listGroupBodies\":[[{\"id\":27,\"validFrom\":\"Oct 14, 2018 12:00:00 AM\",\"validTo\":\"Oct 14, 2018 8:00:00 AM\",\"user\":{\"id\":1,\"firstname\":\"Max\",\"lastname\":\"Mustermann\",\"isCompany\":false,\"notes\":\"Test-Notiz\",\"validFrom\":\"Jan 1, 2018 2:00:00 AM\",\"validTo\":\"Jan 1, 2022 2:00:00 AM\",\"modificationDate\":\"Aug 2, 2018 4:57:07 PM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":3,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Private Str. 1a\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":3,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[{\"id\":1,\"regionId\":1,\"regionName\":\"Darmstadt\",\"userId\":1}],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":1,\"title\":\"group1\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":1,\"functionName\":\"Meister Betrieb Strom\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":28,\"validFrom\":\"Oct 14, 2018 8:00:00 AM\",\"validTo\":\"Oct 14, 2018 11:59:59 PM\",\"user\":{\"id\":2,\"firstname\":\"Erika\",\"lastname\":\"Musterfrau\",\"isCompany\":false,\"notes\":\"Test-Notiz\",\"validFrom\":\"Jan 1, 2018 2:00:00 AM\",\"validTo\":\"Jan 1, 2022 2:00:00 AM\",\"modificationDate\":\"Aug 2, 2018 4:57:07 PM\",\"organisation\":{\"id\":2,\"orgaName\":\"Mustermann GmbH\",\"address\":{\"id\":2,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 2\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":4,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Am Feldweg 5\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":2,\"isPrivate\":false,\"phone\":\"0123 111111\",\"cellphone\":\"0123 987654\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 987654\"},\"privateContactData\":{\"id\":4,\"isPrivate\":false,\"phone\":\"0123 111111\",\"cellphone\":\"0123 987654\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 987654\"},\"lsUserInRegions\":[{\"id\":2,\"regionId\":1,\"regionName\":\"Darmstadt\",\"userId\":2},{\"id\":3,\"regionId\":2,\"regionName\":\"Erbach\",\"userId\":2}],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":1,\"title\":\"group1\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":1,\"functionName\":\"Meister Betrieb Strom\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}],[{\"id\":87,\"validFrom\":\"Oct 14, 2018 12:00:00 AM\",\"validTo\":\"Oct 14, 2018 8:00:00 AM\",\"user\":{\"id\":3,\"firstname\":\"N\",\"lastname\":\"N 1\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":2,\"title\":\"group2\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":3,\"functionName\":\"Ingenieure Strom/Gas/Wasser\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":88,\"validFrom\":\"Oct 14, 2018 8:00:00 AM\",\"validTo\":\"Oct 14, 2018 11:59:59 PM\",\"user\":{\"id\":3,\"firstname\":\"N\",\"lastname\":\"N 1\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":2,\"title\":\"group2\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":3,\"functionName\":\"Ingenieure Strom/Gas/Wasser\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}],[{\"id\":147,\"validFrom\":\"Oct 14, 2018 12:00:00 AM\",\"validTo\":\"Oct 14, 2018 8:00:00 AM\",\"user\":{\"id\":7,\"firstname\":\"N\",\"lastname\":\"N 5\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":3,\"title\":\"Sondergruppe\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":true,\"lsUserFunction\":[{\"functionId\":4,\"functionName\":\"Meister Betrieb Gas\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":148,\"validFrom\":\"Oct 14, 2018 8:00:00 AM\",\"validTo\":\"Oct 14, 2018 11:59:59 PM\",\"user\":{\"id\":8,\"firstname\":\"N\",\"lastname\":\"N 6\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":3,\"title\":\"Sondergruppe\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":true,\"lsUserFunction\":[{\"functionId\":4,\"functionName\":\"Meister Betrieb Gas\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}]]},{\"label\":\"15.10.2018 Montag\",\"listGroupBodies\":[[{\"id\":29,\"validFrom\":\"Oct 15, 2018 12:00:00 AM\",\"validTo\":\"Oct 15, 2018 8:00:00 AM\",\"user\":{\"id\":2,\"firstname\":\"Erika\",\"lastname\":\"Musterfrau\",\"isCompany\":false,\"notes\":\"Test-Notiz\",\"validFrom\":\"Jan 1, 2018 2:00:00 AM\",\"validTo\":\"Jan 1, 2022 2:00:00 AM\",\"modificationDate\":\"Aug 2, 2018 4:57:07 PM\",\"organisation\":{\"id\":2,\"orgaName\":\"Mustermann GmbH\",\"address\":{\"id\":2,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 2\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":4,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Am Feldweg 5\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":2,\"isPrivate\":false,\"phone\":\"0123 111111\",\"cellphone\":\"0123 987654\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 987654\"},\"privateContactData\":{\"id\":4,\"isPrivate\":false,\"phone\":\"0123 111111\",\"cellphone\":\"0123 987654\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 987654\"},\"lsUserInRegions\":[{\"id\":2,\"regionId\":1,\"regionName\":\"Darmstadt\",\"userId\":2},{\"id\":3,\"regionId\":2,\"regionName\":\"Erbach\",\"userId\":2}],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":1,\"title\":\"group1\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":1,\"functionName\":\"Meister Betrieb Strom\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":30,\"validFrom\":\"Oct 15, 2018 4:00:00 PM\",\"validTo\":\"Oct 15, 2018 11:59:59 PM\",\"user\":{\"id\":3,\"firstname\":\"N\",\"lastname\":\"N 1\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":1,\"title\":\"group1\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":1,\"functionName\":\"Meister Betrieb Strom\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}],[{\"id\":89,\"validFrom\":\"Oct 15, 2018 12:00:00 AM\",\"validTo\":\"Oct 15, 2018 8:00:00 AM\",\"user\":{\"id\":3,\"firstname\":\"N\",\"lastname\":\"N 1\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":2,\"title\":\"group2\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":3,\"functionName\":\"Ingenieure Strom/Gas/Wasser\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":90,\"validFrom\":\"Oct 15, 2018 4:00:00 PM\",\"validTo\":\"Oct 15, 2018 11:59:59 PM\",\"user\":{\"id\":3,\"firstname\":\"N\",\"lastname\":\"N 1\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":2,\"title\":\"group2\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":3,\"functionName\":\"Ingenieure Strom/Gas/Wasser\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}],[{\"id\":149,\"validFrom\":\"Oct 15, 2018 12:00:00 AM\",\"validTo\":\"Oct 15, 2018 8:00:00 AM\",\"user\":{\"id\":8,\"firstname\":\"N\",\"lastname\":\"N 6\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":3,\"title\":\"Sondergruppe\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":true,\"lsUserFunction\":[{\"functionId\":4,\"functionName\":\"Meister Betrieb Gas\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":150,\"validFrom\":\"Oct 15, 2018 4:00:00 PM\",\"validTo\":\"Oct 15, 2018 11:59:59 PM\",\"user\":{\"id\":9,\"firstname\":\"N\",\"lastname\":\"N 7\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":3,\"title\":\"Sondergruppe\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":true,\"lsUserFunction\":[{\"functionId\":4,\"functionName\":\"Meister Betrieb Gas\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}]]},{\"label\":\"16.10.2018, Dienstag\",\"listGroupBodies\":[[{\"id\":31,\"validFrom\":\"Oct 16, 2018 12:00:00 AM\",\"validTo\":\"Oct 16, 2018 8:00:00 AM\",\"user\":{\"id\":3,\"firstname\":\"N\",\"lastname\":\"N 1\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":1,\"title\":\"group1\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":1,\"functionName\":\"Meister Betrieb Strom\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":32,\"validFrom\":\"Oct 16, 2018 4:00:00 PM\",\"validTo\":\"Oct 16, 2018 11:59:59 PM\",\"user\":{\"id\":4,\"firstname\":\"N\",\"lastname\":\"N 2\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":1,\"title\":\"group1\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":1,\"functionName\":\"Meister Betrieb Strom\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}],[{\"id\":91,\"validFrom\":\"Oct 16, 2018 12:00:00 AM\",\"validTo\":\"Oct 16, 2018 8:00:00 AM\",\"user\":{\"id\":3,\"firstname\":\"N\",\"lastname\":\"N 1\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":2,\"title\":\"group2\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":3,\"functionName\":\"Ingenieure Strom/Gas/Wasser\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":92,\"validFrom\":\"Oct 16, 2018 4:00:00 PM\",\"validTo\":\"Oct 16, 2018 11:59:59 PM\",\"user\":{\"id\":1,\"firstname\":\"Max\",\"lastname\":\"Mustermann\",\"isCompany\":false,\"notes\":\"Test-Notiz\",\"validFrom\":\"Jan 1, 2018 2:00:00 AM\",\"validTo\":\"Jan 1, 2022 2:00:00 AM\",\"modificationDate\":\"Aug 2, 2018 4:57:07 PM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":3,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Private Str. 1a\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":3,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[{\"id\":1,\"regionId\":1,\"regionName\":\"Darmstadt\",\"userId\":1}],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":2,\"title\":\"group2\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":false,\"lsUserFunction\":[{\"functionId\":3,\"functionName\":\"Ingenieure Strom/Gas/Wasser\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}],[{\"id\":151,\"validFrom\":\"Oct 16, 2018 12:00:00 AM\",\"validTo\":\"Oct 16, 2018 8:00:00 AM\",\"user\":{\"id\":9,\"firstname\":\"N\",\"lastname\":\"N 7\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":3,\"title\":\"Sondergruppe\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":true,\"lsUserFunction\":[{\"functionId\":4,\"functionName\":\"Meister Betrieb Gas\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}},{\"id\":152,\"validFrom\":\"Oct 16, 2018 4:00:00 PM\",\"validTo\":\"Oct 16, 2018 11:59:59 PM\",\"user\":{\"id\":9,\"firstname\":\"N\",\"lastname\":\"N 7\",\"isCompany\":false,\"notes\":\"noch nicht belegter Anwender\",\"validFrom\":\"Jan 1, 2018 12:00:00 AM\",\"validTo\":\"Jan 1, 2033 12:00:00 AM\",\"modificationDate\":\"Sep 26, 2018 6:57:07 AM\",\"organisation\":{\"id\":1,\"orgaName\":\"TestOrga\",\"address\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"}},\"privateAddress\":{\"id\":1,\"postcode\":\"33100\",\"community\":\"community-sued\",\"communitySuffix\":\"_sued\",\"street\":\"Teststraße 1\",\"housenumber\":\"1\",\"wgs84zone\":\"EPSG-xy\",\"latitude\":\"1.0000\",\"longitude\":\"2.0000\",\"urlMap\":\"www.url.de\"},\"businessContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"privateContactData\":{\"id\":1,\"isPrivate\":true,\"phone\":\"0123 456789\",\"cellphone\":\"0123 456789\",\"radiocomm\":\"01234656789\",\"email\":\"test@test.de\",\"pager\":\"0123 456789\"},\"lsUserInRegions\":[],\"lsUserFunctions\":[]},\"status\":{\"id\":1,\"title\":\"Plan-Plan\"},\"standbyGroup\":{\"id\":3,\"title\":\"Sondergruppe\",\"note\":\"Test-Notiz\",\"modificationDate\":\"Aug 6, 2018 2:00:00 AM\",\"nextUserInNextCycle\":true,\"lsUserFunction\":[{\"functionId\":4,\"functionName\":\"Meister Betrieb Gas\"}],\"lsRegions\":[],\"lsIgnoredCalendarDays\":[]}}]]}]}";

		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);
		standbyGroup.setId(1l);

		User user = new User();
		user.setId(new Long(1));
		user.setLastname("test");
		user.setFirstname("test");

		StandbyStatus status = new StandbyStatus();
		ArchiveStandbyScheduleHeader header = new ArchiveStandbyScheduleHeader();

		header.setJsonPlan(jsonPlan);
		PlanningMsgResponseDto responseDto;

		Mockito.when(archiveRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(standbyStatusRepository.findOne(Mockito.anyLong())).thenReturn(status);
		Mockito.when(archiveRepository.findOne(Mockito.anyLong())).thenReturn(header);
		Mockito.when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);
		Mockito.when(userRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(userRepository.findOne(Mockito.anyLong())).thenReturn(user);

		responseDto = archiveController.importPlan(id, modUserName);

		assertNotNull(responseDto);

	}

//	@Test
//	public void deleteExistingBodysOnOneDayTest() throws SpException {
//
//		boolean doOnce = false;
//		Long groupId = new Long(1);
//		Date dateIndex = new Date();
//		StandbyStatus status = new StandbyStatus();
//		List<PlanningMsgDto> lsMsg = new ArrayList<>();
//		List<StandbyScheduleBody> listBodyTarget = new ArrayList<>();
//		StandbyScheduleBody standbyScheduleBody = new StandbyScheduleBody();
//		listBodyTarget.add(standbyScheduleBody);
//
//		Mockito.when(archiveRepository.exists(Mockito.anyLong())).thenReturn(true);
//		Mockito.when(standbyStatusRepository.findOne(Mockito.anyLong())).thenReturn(status);
//
//		Mockito.when(plannedDataController.getPlannedBodysForGroupAndDayAndStatus(Mockito.anyLong(), Mockito.any(),
//				Mockito.any())).thenReturn(listBodyTarget);
//
//		boolean response = archiveController.deleteExistingBodysOnOneDay(doOnce, groupId, dateIndex, status, lsMsg);
//		assertNotNull(response);
//
//	}

//	@Test
//	public void createNewBodyFromArchiveBodyTest() throws SpException {
//
//		StandbyScheduleBodySelectionDto body = new StandbyScheduleBodySelectionDto();
//		StandbyGroupSelectionDto sbga = new StandbyGroupSelectionDto();
//		sbga.setTitle("title");
//		sbga.setId(1L);
//		body.setStandbyGroup(sbga);
//
//		UserSmallSelectionDto userDto = new UserSmallSelectionDto();
//		userDto.setId(1L);
//		userDto.setFirstname("firstname");
//		userDto.setLastname("lastname");
//		body.setValidFrom(DateHelper.getDate(2017, 12, 31));
//		body.setValidTo(DateHelper.getDate(2018, 1, 3));
//		body.setUser(userDto);
//
//		User user = new User();
//		user.setId(1L);
//
//		StandbyGroup sbg = new StandbyGroup();
//		sbg.setId(1L);
//
//		List<PlanningMsgDto> lsMsg = new ArrayList<>();
//
//		ArchiveStandbyScheduleHeader header = new ArchiveStandbyScheduleHeader();
//
//		Long id = new Long(2);
//		String userName = "mx";
//
//		StandbyStatus status = new StandbyStatus();
//		status.setTitle("status");
//		status.setId(id);
//
//		Mockito.when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
//		Mockito.when(userRepository.exists(Mockito.anyLong())).thenReturn(true);
//
//		Mockito.when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(sbg);
//		Mockito.when(userRepository.findOne(Mockito.anyLong())).thenReturn(user);
//
//		List<PlanningMsgDto> result = archiveController.createNewBodyFromArchiveBody(body, status, header, userName,
//				lsMsg);
//		assertNotNull(result);
//
//		Mockito.when(userRepository.exists(Mockito.anyLong())).thenReturn(false);
//		result = archiveController.createNewBodyFromArchiveBody(body, status, header, userName, lsMsg);
//		assertNotNull(result);
//
//	}

	@Test
	public void createArchiveForIntervallAndGroupsTest() throws SpException {
		List<TransferGroupDto> lsTransferGroup = new ArrayList<>();
		Date validFrom = new Date();
		Date validTo = new Date();
		Long statusId = new Long(1);
		String username = "test";
		String action = "test";
		String comment = "";

		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		filter.setStandbyListId(new Long(1));
		filter.setValidFrom(new Date());
		filter.setValidTo(new Date());

		StandbyScheduleArchiveDto plan = new StandbyScheduleArchiveDto();
		plan.setFilter(filter);

		StandbyStatus status = new StandbyStatus();
		status.setId(statusId);
		status.setTitle("test");

		Mockito.when(standbyStatusRepository.findOne(statusId)).thenReturn(status);

		Mockito.when(plannedDataController.getFilteredPlanByStatusByGroupsForArchive(Mockito.any(), Mockito.anyLong()))
				.thenReturn(plan);

		boolean result = archiveController.createArchiveForIntervallAndGroups(lsTransferGroup, validFrom, validTo,
				statusId, username, action, comment);
		assertNotNull(result);

		Mockito.when(plannedDataController.getFilteredPlanByStatusByGroupsForArchive(Mockito.any(), Mockito.anyLong()))
				.thenThrow(new SpException(121, "Error Code Message", new Exception()));
		result = archiveController.createArchiveForIntervallAndGroups(lsTransferGroup, validFrom, validTo, statusId,
				username, action, comment);
		assertNotNull(result);

	}

	@Test
	public void searchTest() {

		StandbyScheduleSearchDto searchDto = new StandbyScheduleSearchDto();

		List<ArchiveStandbyScheduleHeaderSelectionDto> result = archiveController.search(searchDto);
		assertNotNull(result);

	}

	@Test
	public void createArchiveOnMoveTest() throws SpException {

		StandbyScheduleActionDto actionDto = new StandbyScheduleActionDto();
		String username = "test";

		StandbyScheduleBody body = new StandbyScheduleBody();
		StandbyGroup sbg = new StandbyGroup();
		sbg.setTitle("title");
		sbg.setId(new Long(1));
		body.setStandbyGroup(sbg);

		StandbyStatus status = new StandbyStatus();
		status.setId(new Long(2));
		status.setTitle("test");

		body.setStatus(status);
		User user = new User();
		user.setId(new Long(1));
		user.setLastname("test");
		body.setUser(user);

		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		filter.setStandbyListId(new Long(1));
		filter.setValidFrom(new Date());
		filter.setValidTo(new Date());

		StandbyScheduleArchiveDto plan = new StandbyScheduleArchiveDto();
		plan.setFilter(filter);

		Mockito.when(standbyStatusRepository.findOne(Mockito.anyLong())).thenReturn(status);
		Mockito.when(standbyScheduleBodyRepository.findOne(actionDto.getScheduleBodyId())).thenReturn(body);
		Mockito.when(standbyGroupRepository.findOne(actionDto.getStandbyGroupId())).thenReturn(sbg);
		Mockito.when(plannedDataController.getFilteredPlanByStatusByGroupsForArchive(Mockito.any(), Mockito.anyLong()))
				.thenReturn(plan);
//		when(mailRequest.sendMail(Mockito.any(), Mockito.any(), Mockito.any(), Mockito.any())).thenReturn(null);

		boolean result = archiveController.createArchiveOnMove(actionDto, username);

		assertNotNull(result);

	}

	@Test
	public void createArchiveOnReplaceInOneGroupTest() throws SpException {

		StandbyScheduleActionDto actionDto = new StandbyScheduleActionDto();
		actionDto.setCurrentUserId(new Long(1));
		actionDto.setNewUserId(new Long(2));
		actionDto.setStatusId(new Long(2));
		String username = "test";

		StandbyScheduleBody body = new StandbyScheduleBody();
		StandbyGroup sbg = new StandbyGroup();
		sbg.setTitle("title");
		sbg.setId(new Long(1));
		body.setStandbyGroup(sbg);

		StandbyStatus status = new StandbyStatus();
		status.setId(new Long(2));
		status.setTitle("test");

		body.setStatus(status);
		User user = new User();
		user.setId(new Long(1));
		user.setLastname("test");
		user.setFirstname("test");
		body.setUser(user);

		User user2 = new User();
		user2.setId(new Long(2));
		user2.setLastname("test2");
		user2.setFirstname("test2");

		StandbyScheduleFilterDto filter = new StandbyScheduleFilterDto();
		filter.setStandbyListId(new Long(1));
		filter.setValidFrom(new Date());
		filter.setValidTo(new Date());

		StandbyScheduleArchiveDto plan = new StandbyScheduleArchiveDto();
		plan.setFilter(filter);

		Mockito.when(userRepository.findOne(actionDto.getCurrentUserId())).thenReturn(user);
		Mockito.when(userRepository.findOne(actionDto.getNewUserId())).thenReturn(user2);
		Mockito.when(standbyStatusRepository.findOne(Mockito.anyLong())).thenReturn(status);
		Mockito.when(standbyScheduleBodyRepository.findOne(actionDto.getScheduleBodyId())).thenReturn(body);
		Mockito.when(standbyGroupRepository.findOne(actionDto.getStandbyGroupId())).thenReturn(sbg);
		Mockito.when(plannedDataController.getFilteredPlanByStatusByGroupsForArchive(Mockito.any(), Mockito.anyLong()))
				.thenReturn(plan);

		boolean result = archiveController.createArchiveOnReplaceInOneGroup(actionDto, username);

		assertNotNull(result);

	}

	@Test
	public void testInitUser() {

		Map<Long, User> mapUser = new HashMap<Long, User>();
		Long userId = 1L;
		User user = new User();
		user.setId(new Long(1));
		user.setLastname("test");
		user.setFirstname("test");

		Mockito.when(userRepository.exists(Mockito.anyLong())).thenReturn(false);

		Map<Long, User> result = archiveController.initUser(mapUser, userId);
		assertEquals(mapUser, result);
		assertEquals(mapUser.size(), result.size());

		Mockito.when(userRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(userRepository.findOne(Mockito.anyLong())).thenReturn(user);

		result = archiveController.initUser(mapUser, userId);
		assertEquals(mapUser, result);
		assertEquals(1, result.size());

		result = archiveController.initUser(mapUser, userId);
		assertEquals(mapUser, result);
		assertEquals(1, result.size());

	}

	@Test
	public void testMsgWarn() {
		StandbyScheduleBodySelectionDto stbyBody = new StandbyScheduleBodySelectionDto();
		StandbyGroupSelectionDto sbg = new StandbyGroupSelectionDto();
		sbg.setTitle("title");
		sbg.setId(1L);
		stbyBody.setStandbyGroup(sbg);

		UserSmallSelectionDto user = new UserSmallSelectionDto();
		user.setId(1L);
		user.setFirstname("firstname");
		user.setLastname("lastname");
		stbyBody.setValidFrom(DateHelper.getDate(2017, 12, 31));
		stbyBody.setValidTo(DateHelper.getDate(2018, 1, 3));
		stbyBody.setUser(user);

		boolean printWarningDetails = true;
		int warnCount = 0;
		List<PlanningMsgDto> msgWarning = new ArrayList<>();

		int result = archiveController.msgWarn(printWarningDetails, stbyBody, warnCount, msgWarning);
		assertNotEquals(warnCount, result);

		warnCount = 50;
		result = archiveController.msgWarn(printWarningDetails, stbyBody, warnCount, msgWarning);
		assertNotEquals(warnCount, result);

		printWarningDetails = false;
		result = archiveController.msgWarn(printWarningDetails, stbyBody, warnCount, msgWarning);
		assertNotEquals(warnCount, result);

	}

	@Test
	public void testInitGroup() {

		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testStandbyGroup.json");
		StandbyGroupSelectionDto standbyGroupDto = (StandbyGroupSelectionDto) new Gson().fromJson(json,
				StandbyGroupSelectionDto.class);
		standbyGroupDto.setId(1l);
		StandbyGroup standbyGroup = (StandbyGroup) new Gson().fromJson(json, StandbyGroup.class);
		standbyGroup.setId(1l);

		ArrayList<StandbyGroupSelectionDto> listOfGroups = new ArrayList<>();
		listOfGroups.add(standbyGroupDto);

		Map<Long, StandbyGroup> mapGroups = new HashMap<>();
		ArrayList<Long> lsGroupIds = new ArrayList<>();
		List<PlanningMsgDto> msgWarning = new ArrayList<>();
		int warnCount = 0;

		Mockito.when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(false);

		int result = archiveController.initGroups(mapGroups, listOfGroups, lsGroupIds, msgWarning, warnCount);
		assertEquals(1, result);

		Mockito.when(standbyGroupRepository.exists(Mockito.anyLong())).thenReturn(true);
		Mockito.when(standbyGroupRepository.findOne(Mockito.anyLong())).thenReturn(standbyGroup);

		result = archiveController.initGroups(mapGroups, listOfGroups, lsGroupIds, msgWarning, warnCount);
		assertEquals(0, result);

		listOfGroups.clear();
		result = archiveController.initGroups(mapGroups, listOfGroups, lsGroupIds, msgWarning, warnCount);
		assertEquals(0, result);

	}

}
