/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "STANDBY_SCHEDULE_BODY" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "StandbyScheduleBodyDto")
@JsonInclude(Include.NON_NULL)
public class StandbyScheduleBodySelectionDto extends AbstractDto {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	private Long id;
	private Date validFrom;
	private Date validTo;
	private UserSmallSelectionDto user;
	private StandbyStatusDto status;
	private StandbyGroupSelectionDto standbyGroup;

	public StandbyScheduleBodySelectionDto() {
		/** default constructor. */
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the validFrom
	 */
	public Date getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public Date getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo the validTo to set
	 */
	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	/**
	 * @return the status
	 */
	public StandbyStatusDto getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(StandbyStatusDto status) {
		this.status = status;
	}

	/**
	 * @return the user
	 */
	public UserSmallSelectionDto getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(UserSmallSelectionDto user) {
		this.user = user;
	}

	/**
	 * @return the standbyGroup
	 */
	public StandbyGroupSelectionDto getStandbyGroup() {
		return standbyGroup;
	}

	/**
	 * @param standbyGroup the standbyGroup to set
	 */
	public void setStandbyGroup(StandbyGroupSelectionDto standbyGroup) {
		this.standbyGroup = standbyGroup;
	}

}
