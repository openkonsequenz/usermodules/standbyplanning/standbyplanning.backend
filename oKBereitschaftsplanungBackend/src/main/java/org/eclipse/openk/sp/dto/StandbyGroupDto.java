/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "STANDBYGROUP" Data Transfer Object (DTO)
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@XmlRootElement(name = "StandbyGroupDto")
@JsonInclude(Include.NON_NULL)
public class StandbyGroupDto extends AbstractDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * default serial id.
	 */

	private Long id;
	@NotNull(message = "Titel ist nicht gesetzt")
	@Size(max = 256, message = "Titel mit max. 256 Zeichen")
	private String title;
	@Size(max = 2048, message = "Kommentar mit max. 2048 Zeichen")
	private String note;
	private Date modificationDate;
	@NotNull(message = "Zykluns ist nicht gesetzt")
	private Boolean nextUserInNextCycle;
	@NotNull(message = "Verlängerung ist nicht gesetzt")
	private Boolean extendStandbyTime;
	private List<UserInStandbyGroupDto> lsUserInStandbyGroups;
	private List<StandbyDurationDto> lsStandbyDurations;
	private List<BranchDto> lsBranches;
	private List<UserFunctionDto> lsUserFunction = new ArrayList<>();
	private List<RegionSelectionDto> lsRegions;
	private List<CalendarDayDto> lsIgnoredCalendarDays;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the modificationDate
	 */
	public Date getModificationDate() {
		return modificationDate;
	}

	/**
	 * @param modificationDate the modificationDate to set
	 */
	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	/**
	 * @return the lsUserInStandbyGroups
	 */
	public List<UserInStandbyGroupDto> getLsUserInStandbyGroups() {
		return lsUserInStandbyGroups;
	}

	/**
	 * @param lsUserInStandbyGroups the lsUserInStandbyGroups to set
	 */
	public void setLsUserInStandbyGroups(List<UserInStandbyGroupDto> lsUserInStandbyGroups) {
		this.lsUserInStandbyGroups = lsUserInStandbyGroups;
	}

	/**
	 * @return the lsStandbyDurations
	 */
	public List<StandbyDurationDto> getLsStandbyDurations() {
		return lsStandbyDurations;
	}

	/**
	 * @param lsStandbyDurations the lsStandbyDurations to set
	 */
	public void setLsStandbyDurations(List<StandbyDurationDto> lsStandbyDurations) {
		this.lsStandbyDurations = lsStandbyDurations;
	}

	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * @param note the note to set
	 */
	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the lsBranches
	 */
	public List<BranchDto> getLsBranches() {
		return lsBranches;
	}

	/**
	 * @param lsBranches the lsBranches to set
	 */
	public void setLsBranches(List<BranchDto> lsBranches) {
		this.lsBranches = lsBranches;
	}

	/**
	 * @return the nextUserInNextCycle
	 */
	public Boolean getNextUserInNextCycle() {
		return nextUserInNextCycle;
	}

	/**
	 * @param nextUserInNextCycle the nextUserInNextCycle to set
	 */
	public void setNextUserInNextCycle(Boolean nextUserInNextCycle) {
		this.nextUserInNextCycle = nextUserInNextCycle;
	}

	/**
	 * @return the extendDuty
	 */
	public Boolean getExtendStandbyTime() {
		return extendStandbyTime;
	}

	/**
	 * @param extendStandbyTime the extendDuty to set
	 */
	public void setExtendStandbyTime(Boolean extendStandbyTime) {
		this.extendStandbyTime = extendStandbyTime;
	}

	/**
	 * @return the lsUserFunction
	 */
	public List<UserFunctionDto> getLsUserFunction() {
		return lsUserFunction;
	}

	/**
	 * @param lsUserFunction the lsUserFunction to set
	 */
	public void setLsUserFunction(List<UserFunctionDto> lsUserFunction) {
		this.lsUserFunction = lsUserFunction;
	}

	/**
	 * @return the lsRegions
	 */
	public List<RegionSelectionDto> getLsRegions() {
		return lsRegions;
	}

	/**
	 * @param lsRegions the lsRegions to set
	 */
	public void setLsRegions(List<RegionSelectionDto> lsRegions) {
		this.lsRegions = lsRegions;
	}

	/**
	 * @return the lsIgnoredCalendarDays
	 */
	public List<CalendarDayDto> getLsIgnoredCalendarDays() {
		return lsIgnoredCalendarDays;
	}

	/**
	 * @param lsIgnoredCalendarDays the lsIgnoredCalendarDays to set
	 */
	public void setLsIgnoredCalendarDays(List<CalendarDayDto> lsIgnoredCalendarDays) {
		this.lsIgnoredCalendarDays = lsIgnoredCalendarDays;
	}
}