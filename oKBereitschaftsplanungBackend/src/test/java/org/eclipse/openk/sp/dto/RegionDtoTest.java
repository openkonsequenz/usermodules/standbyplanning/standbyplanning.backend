/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

public class RegionDtoTest {

	@Test
	public void testGettersAndSetters() {
		RegionDto r = new RegionDto();

		r.setId(1l);
		assertEquals(1l, r.getId().longValue());

		r.setRegionName("RegionDto");
		assertEquals("RegionDto", r.getRegionName());

		LocationSelectionDto d = new LocationSelectionDto();
		LocationSelectionDto d2 = new LocationSelectionDto();
		ArrayList<LocationSelectionDto> lsLocationDto = new ArrayList<>();
		lsLocationDto.add(d);
		lsLocationDto.add(d2);
		r.setLsLocations(lsLocationDto);
		assertNotNull(lsLocationDto);
		assertTrue(lsLocationDto.size() == 2);
		assertNotNull(r.getLsLocations());
		assertTrue(r.getLsLocations().size() == 2);
		assertEquals(d, r.getLsLocations().get(0));
		assertEquals(d2, r.getLsLocations().get(1));
		
		RegionHasFunctionDto d1 = new RegionHasFunctionDto();
		RegionHasFunctionDto d21 = new RegionHasFunctionDto();
		ArrayList<RegionHasFunctionDto> lsRegionHasFunctionsDto = new ArrayList<>();
		lsRegionHasFunctionsDto.add(d1);
		lsRegionHasFunctionsDto.add(d21);
		r.setLsRegionHasFunctions(lsRegionHasFunctionsDto);
		assertNotNull(lsRegionHasFunctionsDto);
		assertTrue(lsRegionHasFunctionsDto.size() == 2);
		assertNotNull(r.getLsRegionHasFunctions());
		assertTrue(r.getLsRegionHasFunctions().size() == 2);
		assertEquals(d1, r.getLsRegionHasFunctions().get(0));
		assertEquals(d21, r.getLsRegionHasFunctions().get(1));

	}

}
