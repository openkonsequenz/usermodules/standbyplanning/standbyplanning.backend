/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.auth2.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class JwtTokenTest {

    @Test
    public void testGettersAndSetters(){

        JwtToken jwtToken = new JwtToken();

        jwtToken.setAccessToken("AccessToken");
        assertEquals("AccessToken", jwtToken.getAccessToken());

        jwtToken.setRefreshToken("RefreshToken");
        assertEquals("RefreshToken", jwtToken.getRefreshToken());

        jwtToken.setTokenType("TokenType");
        assertEquals("TokenType", jwtToken.getTokenType());

        jwtToken.setSessionState("SessionState");
        assertEquals("SessionState", jwtToken.getSessionState());

        jwtToken.setExpiresIn(13);
        assertEquals((Integer)13, jwtToken.getExpiresIn());

        jwtToken.setRefreshExpiresIn(14);
        assertEquals((Integer)14, jwtToken.getRefreshExpiresIn());

        jwtToken.setNotBeforePolicy(15);
        assertEquals((Integer)15, jwtToken.getNotBeforePolicy());
    }
}
