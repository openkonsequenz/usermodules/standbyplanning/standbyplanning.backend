/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.CalendarRepository;
import org.eclipse.openk.sp.db.model.CalendarDay;
import org.eclipse.openk.sp.dto.CalendarDayDto;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.eclipse.openk.sp.util.GsonTypeHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

/** Class to handle user operations. */
@RunWith(MockitoJUnitRunner.class)
public class CalendarControllerTest {
	protected static final Logger logger = Logger.getLogger(UserController.class);

	private static String currentUser = "test";

	@Mock
	CalendarRepository calendarRepository;
	@Mock
	EntityConverter entityConverter;

	@InjectMocks
	CalendarController calendarController;

	@Test
	public void saveErrorTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testCalendarDay.json");
		CalendarDayDto calendarDayDto = (CalendarDayDto) new Gson().fromJson(json, CalendarDayDto.class);

		CalendarDay calendarDay = (CalendarDay) new Gson().fromJson(json, CalendarDay.class);

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) calendarDay);
		when(calendarRepository.save(calendarDay)).thenThrow(new IllegalArgumentException());

		// run test
		CalendarDayDto calendarDayResult = null;

		try {
			calendarDayResult = calendarController.saveCalendarDay(calendarDayDto);
		} catch (Exception e) {
			assertNotNull(e);
		}
		// assertEquals(1l, calendarDayResult.getId().longValue());
	}

	@Test
	public void saveTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testCalendarDay.json");
		CalendarDayDto calendarDayDto = (CalendarDayDto) new Gson().fromJson(json, CalendarDayDto.class);

		CalendarDay calendarDay = (CalendarDay) new Gson().fromJson(json, CalendarDay.class);

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) calendarDay);

		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) calendarDayDto);
		when(calendarRepository.save(calendarDay)).thenReturn(calendarDay);

		// run test
		CalendarDayDto calendarDayResult = null;

		calendarDayResult = calendarController.saveCalendarDay(calendarDayDto);
		assertNotNull(calendarDayResult);
		assertEquals(30l, calendarDayResult.getId().longValue());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAllTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testCalendarDayList.json");
		List<CalendarDayDto> lsCalendarDayDtoMock = (List<CalendarDayDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_CALENDARDAY_DTO_ARRAY_LIST);
		List<CalendarDay> lsCalendarDayMock = (List<CalendarDay>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_CALENDARDAY_ARRAY_LIST);

		CalendarDayDto calendarDayDto = lsCalendarDayDtoMock.get(1);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) calendarDayDto);
		when(calendarRepository.findAllByOrderByDateIndexDesc()).thenReturn(lsCalendarDayMock);

		// run test
		List<CalendarDayDto> lsCalendarDayDto = null;
		try {
			lsCalendarDayDto = calendarController.getCalendarDays();
		} catch (Exception e) {
			assertNull(e);
		}

		assertNotNull(lsCalendarDayDto);
		assertTrue(lsCalendarDayDto.size() > 0);
		assertTrue(lsCalendarDayDto.get(0) instanceof CalendarDayDto);

		// run negativ test
		when(calendarRepository.findAllByOrderByDateIndexDesc()).thenThrow(new IllegalArgumentException());
		try {
			lsCalendarDayDto = calendarController.getCalendarDays();
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getAllSelectionTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testCalendarDayList.json");
		List<CalendarDayDto> lsCalendarDayDtoMock = (List<CalendarDayDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_CALENDARDAY_DTO_ARRAY_LIST);
		List<CalendarDay> lsCalendarDayMock = (List<CalendarDay>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_CALENDARDAY_ARRAY_LIST);

		CalendarDayDto calendarDayDto = lsCalendarDayDtoMock.get(1);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) calendarDayDto);
		when(calendarRepository.findAllByOrderByDateIndexDesc()).thenReturn(lsCalendarDayMock);

		// run test
		List<CalendarDayDto> lsCalendarDayDto = null;
		try {
			lsCalendarDayDto = calendarController.getCalendarDaysSelection();
		} catch (Exception e) {
			assertNull(e);
		}

		assertNotNull(lsCalendarDayDto);
		assertTrue(lsCalendarDayDto.size() > 0);
		assertTrue(lsCalendarDayDto.get(0) instanceof CalendarDayDto);

		// run negativ test
		when(calendarRepository.findAllByOrderByDateIndexDesc()).thenReturn(null);
		try {
			lsCalendarDayDto = calendarController.getCalendarDays();
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	@Test
	public void getCalendarDay() {
		Long id = 30L;
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testCalendarDay.json");
		CalendarDayDto calendarDayDto = (CalendarDayDto) new Gson().fromJson(json, CalendarDayDto.class);

		CalendarDay calendarDay = (CalendarDay) new Gson().fromJson(json, CalendarDay.class);

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) calendarDay);

		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) calendarDayDto);
		when(calendarRepository.findOne(any())).thenReturn(calendarDay);

		// run test
		CalendarDayDto calendarDayResult = null;

		try {
			calendarDayResult = calendarController.getCalendarDay(id);
		} catch (Exception e) {
			assertNull(e);
		}
		assertNotNull(calendarDayResult);
		assertEquals(id, calendarDayResult.getId());

		// run negative test
		when(calendarRepository.findOne(any())).thenThrow(new IllegalArgumentException());
		try {
			calendarDayResult = calendarController.getCalendarDay(id);
		} catch (Exception e) {
			assertNotNull(e);
		}
	}

	@Test
	public void deleteTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testCalendarDay.json");
		CalendarDayDto calendarDayDto = (CalendarDayDto) new Gson().fromJson(json, CalendarDayDto.class);

		CalendarDay calendarDay = (CalendarDay) new Gson().fromJson(json, CalendarDay.class);

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) calendarDay);

		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) calendarDayDto);
		when(calendarRepository.save(calendarDay)).thenReturn(calendarDay);

		// run test
		CalendarDayDto calendarDayResult = null;

		try {
			calendarDayResult = calendarController.deleteCalendarDay(calendarDayDto);
			assertNull(calendarDayResult);
		} catch (Exception e) {
			assertNull(e);
		}

		// run negative test
		when(calendarRepository.save(calendarDay)).thenThrow(new IllegalArgumentException());

		try {
			calendarDayResult = calendarController.deleteCalendarDay(calendarDayDto);
		} catch (Exception e) {
			assertNotNull(e);
		}

	}

	@Test(expected = SpException.class)
	public void deleteTestException() throws SpException {

		calendarController.deleteCalendarDay(null);

	}

	@Test
	public void containsDateTest() throws SpException {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testCalendarDayList.json");
		List<CalendarDayDto> lsCalendarDayDtoMock = (List<CalendarDayDto>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_CALENDARDAY_DTO_ARRAY_LIST);
		List<CalendarDay> lsCalendarDayMock = (List<CalendarDay>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_CALENDARDAY_ARRAY_LIST);

		CalendarDayDto calendarDayDto = lsCalendarDayDtoMock.get(1);
		Date testDate = calendarDayDto.getDateIndex();

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) calendarDayDto);
		when(calendarRepository.findAllByOrderByDateIndexDesc()).thenReturn(lsCalendarDayMock);

		boolean result = calendarController.containsDate(testDate);
		assertEquals(true, result);

		result = calendarController.containsDate(new Date());
		assertEquals(false, result);
	}

}
