/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DocumentDtoTest {

	@Test
	public void getterSetterTest() {
		String testString = "TEST";
		byte[] data = testString.getBytes();

		DocumentDto dto = new DocumentDto();
		dto.setDocumentName(testString);
		assertEquals(testString, dto.getDocumentName());

		dto.setData(data);
		assertEquals(data, dto.getData());

	}

}
