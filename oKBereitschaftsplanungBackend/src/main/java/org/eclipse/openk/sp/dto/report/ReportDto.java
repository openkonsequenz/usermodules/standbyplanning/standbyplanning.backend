/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.report;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.db.model.StandbyGroup;
import org.eclipse.openk.sp.db.model.StandbyList;
import org.eclipse.openk.sp.exceptions.SpErrorEntry;
import org.eclipse.openk.sp.exceptions.SpException;
import org.eclipse.openk.sp.exceptions.SpExceptionEnum;
import org.eclipse.openk.sp.util.DateHelper;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "Report" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "ReportDto")
@JsonInclude(Include.NON_NULL)
public class ReportDto extends AbstractDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@NotNull(message = "Planart muss angegeben werden")
	private String reportName;
	@NotNull(message = "Format muss angegeben werden")
	private String printFormat;
	private Long groupId;
	private StandbyGroup standByGroup;
	private Long standByListId;
	private StandbyList standByList;
	private Long userId;
	private Long statusId;
	private String validFrom;
	private String validTo;
	private Date validFromDate;
	private Date validToDate;
	private Integer defaultDayRange;
	@NotNull(message = "Ebene muss angegeben werden")
	private String reportLevel;
	private int groupPosition;
	private String currentDate;

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public String getPrintFormat() {
		return printFormat;
	}

	public void setPrintFormat(String format) {
		if (format != null) {
			this.printFormat = format.toLowerCase();
		}
	}

	public Date getValidFromDate() {
		return validFromDate;
	}

	public void setValidFromDate(Date from) {
		this.validFromDate = from;
	}

	public Date getValidToDate() {
		return validToDate;
	}

	public void setValidToDate(Date to) {
		this.validToDate = to;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public StandbyList getStandByList() {
		return standByList;
	}

	public void setStandByList(StandbyList standByList) {
		this.standByList = standByList;
	}

	public StandbyGroup getStandByGroup() {
		return standByGroup;
	}

	public void setStandByGroup(StandbyGroup standByGroup) {
		this.standByGroup = standByGroup;
	}

	public Integer getDefaultDayRange() {
		return defaultDayRange;
	}

	public void setDefaultDayRange(Integer defaultDayRange) {
		this.defaultDayRange = defaultDayRange;
	}

	public String getReportLevel() {
		return reportLevel;
	}

	public void setReportLevel(String reportLevel) {
		this.reportLevel = reportLevel;
	}

	public int getGroupPosition() {
		return groupPosition;
	}

	public void setGroupPosition(int groupPosition) {
		this.groupPosition = groupPosition;
	}

	/**
	 * @return the groupId
	 */
	public Long getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the listId
	 */
	public Long getStandByListId() {
		return standByListId;
	}

	/**
	 * @param listId the listId to set
	 */
	public void setStandByListId(Long listId) {
		this.standByListId = listId;
	}

	/**
	 * @return the statusId
	 */
	public Long getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId the statusId to set
	 */
	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	/**
	 * @return the currentDate
	 */
	public String getCurrentDate() {
		return currentDate;
	}

	/**
	 * @param currentDate the currentDate to set
	 */
	public void setCurrentDate(String currentDate) {
		this.currentDate = currentDate;
	}

	/**
	 * @return the validFrom
	 */
	public String getValidFrom() {
		return validFrom;
	}

	/**
	 * @param validFrom the validFrom to set
	 */
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}

	/**
	 * @return the validTo
	 */
	public String getValidTo() {
		return validTo;
	}

	/**
	 * @param validTo the validTo to set
	 */
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}
	
	public void createDatesFromStrings() throws SpException {
		try {
			if (this.getValidFrom() != null) {
				this.setValidFromDate(DateHelper.getDateFromString(this.getValidFrom(), Globals.DATE_TIME_FORMAT));
			}
			if (this.getValidTo() != null) {
				this.setValidToDate(DateHelper.getDateFromString(this.getValidTo(), Globals.DATE_TIME_FORMAT));
			}
		} catch (Exception e) {
			Logger.getLogger(ReportDto.class).error(e, e);
			SpErrorEntry ee = SpExceptionEnum.DEFAULT_EXCEPTION.getEntry();
			ee.setE(e);
			throw new SpException(ee);
		}
	}
}
