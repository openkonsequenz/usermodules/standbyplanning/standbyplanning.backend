/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.List;

import org.apache.log4j.Logger;
import org.eclipse.openk.sp.abstracts.AbstractDto;
import org.eclipse.openk.sp.abstracts.AbstractEntity;
import org.eclipse.openk.sp.db.converter.EntityConverter;
import org.eclipse.openk.sp.db.dao.OrganisationRepository;
import org.eclipse.openk.sp.db.model.Organisation;
import org.eclipse.openk.sp.dto.OrganisationDto;
import org.eclipse.openk.sp.dto.OrganisationSelectionDto;
import org.eclipse.openk.sp.util.FileHelperTest;
import org.eclipse.openk.sp.util.GsonTypeHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.Gson;

/** Class to handle organisation operations. */
@RunWith(MockitoJUnitRunner.class)
public class OrganisationControllerTest {
	protected static final Logger logger = Logger.getLogger(OrganisationController.class);

	@Mock
	OrganisationRepository organisationRepository;
	@Mock
	EntityConverter entityConverter;

	@InjectMocks
	OrganisationController organisationController;

	@Test
	public void saveOrganisationErrorTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testOrganisation.json");
		OrganisationDto organisationDto = (OrganisationDto) new Gson().fromJson(json, OrganisationDto.class);
		organisationDto.setId(1l);
		Organisation organisation = (Organisation) new Gson().fromJson(json, Organisation.class);
		organisation.setId(1l);

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) organisation);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) organisationDto);

		// run test
		OrganisationDto organisationResult = null;

		organisationResult = organisationController.saveOrganisation(organisationDto);
		assertNotNull(organisationResult);
		assertEquals(1l, organisationResult.getId().longValue());
	}

	@Test
	public void saveOrganisationTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testOrganisation.json");
		OrganisationDto organisationDto = (OrganisationDto) new Gson().fromJson(json, OrganisationDto.class);
		organisationDto.setId(1l);
		Organisation organisation = (Organisation) new Gson().fromJson(json, Organisation.class);
		organisation.setId(1l);

		// create when
		when(entityConverter.convertDtoToEntity((AbstractDto) any(), (AbstractEntity) any()))
				.thenReturn((AbstractEntity) organisation);
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) organisationDto);
		when(organisationRepository.save(organisation)).thenReturn(organisation);

		// run test
		OrganisationDto organisationResult = null;

		organisationResult = organisationController.saveOrganisation(organisationDto);

		assertNotNull(organisationResult);
		assertEquals(1l, organisationResult.getId().longValue());
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getOrganisationsTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testOrganisationList.json");
		List<Organisation> lsOrganisationMock = (List<Organisation>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_ORGANISATION_ARRAY_LIST);

		json = fh.loadStringFromResource("testOrganisation.json");
		OrganisationDto organisationDtoMock = (OrganisationDto) new Gson().fromJson(json, OrganisationDto.class);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) organisationDtoMock);
		when(organisationRepository.findAllByOrderByOrgaNameAsc()).thenReturn(lsOrganisationMock);

		// run test
		List<OrganisationDto> lsOutputOrganisationDto = organisationController.getOrganisations();

		assertNotNull(lsOutputOrganisationDto);
		assertTrue(lsOutputOrganisationDto.size() > 0);
		assertTrue(lsOutputOrganisationDto.get(0) instanceof OrganisationDto);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void getOrganisationsSelectionTest() {
		FileHelperTest fh = new FileHelperTest();
		// create test data
		String json = fh.loadStringFromResource("testOrganisationList.json");
		List<Organisation> lsOrganisationMock = (List<Organisation>) new Gson().fromJson(json,
				GsonTypeHelper.JSON_TYPE_ORGANISATION_ARRAY_LIST);

		json = fh.loadStringFromResource("testOrganisation.json");
		OrganisationSelectionDto organisationDtoMock = (OrganisationSelectionDto) new Gson().fromJson(json,
				OrganisationSelectionDto.class);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) organisationDtoMock);
		when(organisationRepository.findAllByOrderByOrgaNameAsc()).thenReturn(lsOrganisationMock);

		// run test
		List<OrganisationSelectionDto> lsOutputOrganisationDto = organisationController.getOrganisationsSelection();

		assertNotNull(lsOutputOrganisationDto);
		assertTrue(lsOutputOrganisationDto.size() > 0);
		assertTrue(lsOutputOrganisationDto.get(0) instanceof OrganisationSelectionDto);
	}

	@Test
	public void getOrganisationTest() {
		// create test data
		long id = 5;
		FileHelperTest fh = new FileHelperTest();
		String json = fh.loadStringFromResource("testOrganisation.json");
		Organisation organisationMock = (Organisation) new Gson().fromJson(json, Organisation.class);
		organisationMock.setId(id);
		OrganisationDto organisationDtoMock = (OrganisationDto) new Gson().fromJson(json, OrganisationDto.class);
		organisationDtoMock.setId(id);

		// create when
		when(entityConverter.convertEntityToDto((AbstractEntity) any(), (AbstractDto) any()))
				.thenReturn((AbstractDto) organisationDtoMock);
		when(organisationRepository.findOne(id)).thenReturn(organisationMock);

		// run test
		OrganisationDto organisationDto = organisationController.getOrganisation(id);
		assertNotNull(organisationDto);
		assertTrue(organisationDto.getId() == id);
	}

}
