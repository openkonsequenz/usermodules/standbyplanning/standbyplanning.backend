/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.db.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.eclipse.openk.sp.abstracts.AbstractEntity;

/**
 * The persistent class for the "REGION" database table.
 */
@Entity
@Table(name = "REGION")
public class Region extends AbstractEntity {

	/**
	 * default serial id.
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "REGION_ID_SEQ")
	@SequenceGenerator(name = "REGION_ID_SEQ", sequenceName = "REGION_ID_SEQ", allocationSize = 1)
	@Column(name = "id", updatable = false)
	private Long id;

	@Column(name = "region_name", length = 256, nullable = false)
	private String regionName;

	@ManyToMany(mappedBy = "lsRegions", fetch = FetchType.EAGER)
	private List<Location> lsLocations = new ArrayList<>();

	@ManyToMany(mappedBy = "lsRegions", fetch = FetchType.EAGER)
	private List<StandbyGroup> lsStandbyGroups = new ArrayList<>();

	public Region() {
		/** default constructor. */
	}

	public Region(Long id) {
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the regionName
	 */
	public String getRegionName() {
		return regionName;
	}

	/**
	 * @param regionName
	 *            the regionName to set
	 */
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	/**
	 * @return the lsLocations
	 */
	public List<Location> getLsLocations() {
		return lsLocations;
	}

	/**
	 * @param lsLocations
	 *            the lsLocations to set
	 */
	public void setLsLocations(List<Location> lsLocations) {
		this.lsLocations = lsLocations;
	}

	/**
	 * @return the lsStandbyGroups
	 */
	public List<StandbyGroup> getLsStandbyGroups() {
		return lsStandbyGroups;
	}

	/**
	 * @param lsStandbyGroups
	 *            the lsStandbyGroups to set
	 */
	public void setLsStandbyGroups(List<StandbyGroup> lsStandbyGroups) {
		this.lsStandbyGroups = lsStandbyGroups;
	}

	@Override
	public int compareTo(AbstractEntity abstractEntity) {
		return (getId() == abstractEntity.getId()) ? 1 : 0;
	}

	@Override
	public Region copy() {
		Region newRegion = new Region();
		newRegion.setId(null);
		newRegion.setLsLocations(this.getLsLocations());
		newRegion.setLsStandbyGroups(this.getLsStandbyGroups());
		newRegion.setRegionName(this.getRegionName());

		return newRegion;

	}
}
