/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.controller.external;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;
import org.eclipse.openk.common.Globals;
import org.eclipse.openk.core.exceptions.HttpStatusException;
import org.eclipse.openk.sp.db.model.StandbyScheduleBody;
import org.eclipse.openk.sp.dto.planning.StandbyScheduleSearchDto;
import org.eclipse.openk.sp.exceptions.SpExceptionMapper;
import org.eclipse.openk.sp.exceptions.SpNestedException;
import org.eclipse.openk.sp.util.FileHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
/** Class to handle requests to external interfaces operations. */
public class ExternalInterfaceMapper {

	protected static final Logger LOGGER = Logger.getLogger(ExternalInterfaceMapper.class);

	@Autowired
	DistanceController distanceService;

	@Autowired
	AuthController authController;

	@Autowired
	FileHelper fileHelper;

	public List<StandbyScheduleBody> calcDistance(List<StandbyScheduleBody> listBodies,
			StandbyScheduleSearchDto standbyScheduleSearchDto, String token) {

		boolean skipDistanceService = true;

		try {
			String skip = fileHelper.getProperty(Globals.APP_PROP_FILE_NAME, Globals.PROP_DISTANCE_SKIP);
			skipDistanceService = Boolean.parseBoolean(skip);

		} catch (Exception e) {
			LOGGER.info("Distance service is disabled!", e);
			skipDistanceService = true;
		}

		try {

			if (!skipDistanceService) {
				listBodies = distanceService.calcDistance(listBodies, standbyScheduleSearchDto, token, false);
			}

		} catch (Exception e) {
			LOGGER.info("Distance service runs into an error!", e);
		}

		return listBodies;
	}

	public Response authLogout(String token) throws IOException, HttpStatusException {

		return authController.logout(token);

	}

	public Response authLogin(String token) throws IOException, HttpStatusException {

		return authController.login(token);

	}

	public Response responseFromException(Exception e) {
		int errcode;
		String retJson;
		if (e instanceof SpNestedException) {
			SpNestedException spException = (SpNestedException) e;
			LOGGER.info(spException.getHttpStatus(), spException);
			return Response.status(spException.getHttpStatus()).entity(spException).build();

		} else if (e instanceof HttpStatusException) {
			LOGGER.info("Login Failure");
			LOGGER.debug(e);
			errcode = ((HttpStatusException) e).getHttpStatus();
			retJson = SpExceptionMapper.toJson((HttpStatusException) e);
			return Response.status(errcode).entity(retJson).build();

		} else {
			LOGGER.info("Unexpected exception");
			LOGGER.debug(e);
			return Response.status(HttpStatus.SC_INTERNAL_SERVER_ERROR).entity(SpExceptionMapper.getGeneralErrorJson())
					.build();
		}
	}

}
