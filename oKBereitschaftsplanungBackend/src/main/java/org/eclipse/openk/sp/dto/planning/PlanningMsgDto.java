/********************************************************************************
 * Copyright (c) 2018 Mettenmeier GmbH 
 *
 * See the NOTICE file(s) distributed with this work for additional 
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the 
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0 
 ********************************************************************************/
package org.eclipse.openk.sp.dto.planning;

import javax.xml.bind.annotation.XmlRootElement;

import org.eclipse.openk.sp.abstracts.AbstractDto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The "Address" Data Transfer Object (DTO)
 */
@XmlRootElement(name = "PlanningMsgDto")
@JsonInclude(Include.NON_NULL)
public class PlanningMsgDto extends AbstractDto {

	/**	*/
	private static final long serialVersionUID = 1L;
	public static final String WARN = "WARNUNG";
	public static final String INFO = "INFO";

	public static final String CSS_WARN = "warning";
	public static final String CSS_INFO = "info";
	public static final String CSS_INFO_ML1 = "info ml-1";
	public static final String CSS_INFO_ML2 = "info ml-2";
	public static final String CSS_INFO_ML3 = "info ml-3";
	public static final String CSS_INFO_ML4 = "info ml-4";
	public static final String CSS_INFO_ML5 = "info ml-5";
	public static final String CSS_DANGER = "danger";

	private String msg;
	private String type;
	private String cssType;

	public PlanningMsgDto(String msg, String type, String cssType) {
		super();
		this.msg = msg;
		this.type = type;
		this.cssType = cssType;
	}

	/**
	 * @return the msg
	 */
	public String getMsg() {
		return msg;
	}

	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the cssType
	 */
	public String getCssType() {
		return cssType;
	}

	/**
	 * @param cssType the cssType to set
	 */
	public void setCssType(String cssType) {
		this.cssType = cssType;
	}
}
